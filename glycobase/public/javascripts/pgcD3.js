function truncate(str, maxLength, suffix) {
	if(str.length > maxLength) {
		str = str.substring(0, maxLength + 1);
		str = str.substring(0, Math.min(str.length, str.lastIndexOf(" ")));
		str = str + suffix;
	}
	return str;
}

if(filter.length > 0) {
	var rgxp = "^" + filter.toUpperCase() + "$";
	var rgxp2 = "^" + filter2.toUpperCase() + "$";
	//var rgxp = new RegExp(filter.toUpperCase(), "i");
}
if(maxTime.length <= 0) { var maxTime = 70}
if(minTime.length <= 0) { var minTime = 20}
if(massLow.length <= 0) { var massLow = 0}
if(massHigh.length <= 0) { var massHigh = 20000}

console.log(massLow);

var margin = {top: 20, right: 200, bottom: 0, left: 20},
	width = 700,
	height = 650;

	var tooltip = d3.select("body").append("div")
	.attr("class", "tooltip")
	.style("opacity", 0);

	// add the tooltip area to the webpage
var tooltip = d3.select("body").append("div")
    .attr("class", "tooltip")
    .style("opacity", 0);

	var start_year = 26,
		end_year = 62;


var c = d3.scale.category20c();

var x = d3.scale.linear()
	.range([0, width]);

var xAxis = d3.svg.axis()
	.scale(x)
	.orient("top");



var formatYears = d3.format("0000");
xAxis.tickFormat(formatYears);

var svg = d3.select("#d3").append("svg")
	.attr("width", width + margin.left + margin.right)
	.attr("height", height + margin.top + margin.bottom)
	.style("margin-left", margin.left + "px")
	.append("g")
	.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

// var dataset = [[ [2002, 8], [2003, 1], [2004, 1], [2005, 1], [2006, 3], [2007, 3], [2009, 3], [2013, 3]], [ [2004, 5], [2005, 1], [2006, 2], [2010, 20], [2011, 3] ] ,[ [2001, 5], [2005, 15], [2006, 2], [2010, 20], [2012, 25] ]];
// var dataset = [ [2001, 5], [2005, 15], [2006, 2], [2010, 20], [2012, 25] ];

d3.json("/assets/javascripts/pgc_rt.json", function(data) {
	x.domain([start_year, end_year]);
	var xScale = d3.scale.linear()
		.domain([start_year, end_year])
		.range([0, width]);

	//console.log(data);

	svg.append("g")
		.attr("class", "x axis")
		.attr("transform", "translate(0," + 0 + ")")
		.call(xAxis);

	for (var j = 0; j < data.length; j++) {

		var g = svg.append("g").attr("class","journal");

		var circles = g.selectAll("circle")
			.data(data[j]['retention'])
			.enter()
			.append("circle")
			.filter( function (d) {  return ( (d[1].match(rgxp) || d[1].match(rgxp2)) && (d[0] > minTime && d[0] < maxTime)  && ( d[2] >= massLow && d[2] <= massHigh  ) )} );

		var text = g.selectAll("text")
			.data(data[j]['retention'])
			.enter()
			.append("text");

		var rScale = d3.time.scale()
			.domain([0, d3.max(data[j]['retention'], function(d) { return d[1]; })])
			.range([2, 9]);

			var radius = 5;

		circles
			.attr("cx", function(d, i) { return xScale(d[0]); })
			.attr("cy", j*20+20)
			.attr("r", function(d) { return radius; } ) //rScale(d[1]); })
			.style("fill", function(d) { return c(j); })
			.on("mouseover", function(d) {
          tooltip.transition()
               .duration(200)
               .style("opacity", .9);
          tooltip.html( d[1])
               .style("left", (d3.event.pageX + 5) + "px")
               .style("top", (d3.event.pageY - 28) + "px");
      })
      .on("mouseout", function(d) {
          tooltip.transition()
               .duration(500)
               .style("opacity", 0);
      });

		text
			.attr("y", j*20+25)
			.attr("x",function(d, i) { return xScale(d[0])-5; })
			.attr("class","value")
			.text(function(d){ return d[1]; })
			.style("fill", function(d) { return c(j); })
			.style("display","none");
			//.attr('transform', 'rotate(45 -10 10)');

		g.append("text")
			.attr("y", j*20+25)
			.attr("x",width+20)
			.attr("class","label")
			.text(truncate(data[j]['name'],30,"..."))
			.style("fill", function(d) { return c(j); })
			.on("mouseover", mouseover)
			.on("mouseout", mouseout);
	};

	function mouseover(p) {
		var g = d3.select(this).node().parentNode;
		//d3.select(g).selectAll("circle").style("display","none");
		//d3.select(g).selectAll("text.value").style("display","block");
	}

	function mouseout(p) {
		var g = d3.select(this).node().parentNode;
		//d3.select(g).selectAll("circle").style("display","block");
		//d3.select(g).selectAll("text.value").style("display","none");
	}
});
