name := """glycobase"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.11.6"

libraryDependencies ++= Seq(
  javaJdbc,
  cache,
  javaWs,
  "org.postgresql" % "postgresql" % "9.4-1201-jdbc41",
  "commons-io" % "commons-io" % "2.4",
  "org.apache.commons" % "commons-lang3" % "3.5",
  //"com.fasterxml.jackson.core" % "jackson-core" % "2.6.1",
  "com.googlecode.json-simple" % "json-simple" % "1.1.1",
  "org.apache.jena" % "jena-core" % "3.2.0", "org.apache.jena" % "jena-base" % "3.2.0", "org.apache.jena" % "jena-arq"  % "3.2.0",
  "log4j" % "log4j" % "1.2.17",
  "org.apache.jclouds" % "jclouds-all" % "2.0.1",
  "org.javaswift" % "joss" % "0.9.14",
  "org.json" % "json" % "20180130"

)

libraryDependencies += guice
libraryDependencies += "com.typesafe.play" %% "play-json" % "2.6.0"


// Play provides two styles of routers, one expects its actions to be injected, the
// other, legacy style, accesses its actions statically.
routesGenerator := InjectedRoutesGenerator

//unmanagedJars in Compile += file("lib/rdf-core.jar")
//unmanagedJars in Compile += file("lib/glycostore_rdf.jar")

