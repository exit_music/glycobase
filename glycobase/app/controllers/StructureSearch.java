package controllers;

import com.google.inject.Inject;
import org.apache.jena.query.*;
import org.expasy.glycanrdf.io.glycoCT.GlycoCTReader;
import org.expasy.glycanrdf.query.QueryGenerator;
import org.expasy.glycanrdf.rdf.query.VirtuosoQueryGeneratorExtend;
import play.Logger;
import play.data.DynamicForm;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.*;
import views.html.search.builder;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletionStage;


/**
 * Created by matthew on 9/09/16.
 */

class StructureSearchLoggingAction extends Action.Simple {
    private Logger.ALogger structureSearchLogger = Logger.of("structureSearch");

    public CompletionStage<Result> call(Http.Context ctx) {
        final Http.Request request = ctx.request();
        structureSearchLogger.info("method={} uri={} remote-address={}", request.method(), request.uri(), request.remoteAddress(), request.queryString(), request.host());
        return delegate.call(ctx);
    }
}
public class StructureSearch extends Controller {



    @With(StructureSearchLoggingAction.class)
    public Result builder() {
        return ok(builder.render());
    }

    @With(StructureSearchLoggingAction.class)
    public Result searchGlycoct() {
        return ok(views.html.search.searchGlycoct.render());
    }

    /*
    Items to check include the URI of structure library
    Logger statements important for testing, remove after
    Strutcure library to follow
     */


    @Inject private FormFactory formFactory;
    @With(StructureSearchLoggingAction.class)
    public Result SubstructureSPARQLQueryResult() {
        List<String> rdfResults = new ArrayList<>();

        DynamicForm dynamicForm = formFactory.form().bindFromRequest();

        //remove Logger statements once tested
        Logger.info("form posted " + dynamicForm.data().values());
        Logger.info("check structure string " + dynamicForm.data().values().toString().replaceAll("\\[", "").replaceAll("\\]", ""));
        String queryRDF = prepareQuery(dynamicForm.data().values().toString().replaceAll("\\[", "").replaceAll("\\]", ""));

        Query query = QueryFactory.create(queryRDF.replaceAll("mzjava.expasy.org", "structure.glycostore.org"));
        ARQ.getContext().setTrue(ARQ.useSAX);
        QueryExecution qexec = QueryExecutionFactory.sparqlService("http://137.92.56.159:40935/glycostoreStructuresRDF/query", query); //check IP

        //Retrieving the SPARQL Query results
        org.apache.jena.query.ResultSet results = qexec.execSelect();

        while (results.hasNext()) {
            QuerySolution soln = results.nextSolution();
            rdfResults.add( soln.get("structureConnection").toString() );
        }
        qexec.close();

        return ok(views.html.search.searchStructureResult.render(rdfResults));
    }

    public static String prepareQuery(String glycoCT) {

       /*String glycoCT =    "RES\n" +
               "1b:a-dgal-HEX-1:5\n" +
               "2s:n-acetyl\n" +
               "3b:b-dgal-HEX-1:5\n" +
               "4b:a-dgal-HEX-1:5\n" +
               "5b:a-dgal-HEX-1:5\n" +
               "6b:a-lgal-HEX-1:5|6:d\n" +
               "LIN\n" +
               "1:1d(2+1)2n\n" +
               "2:1o(3+1)3d\n" +
               "3:3o(4+1)4d\n" +
               "4:4o(3+1)5d\n" +
               "5:5o(2+1)6d";*/

        GlycoCTReader reader = new GlycoCTReader();
        org.expasy.glycanrdf.datastructure.Glycan glycan = reader.read(glycoCT, "1");
        QueryGenerator queryGenerator = new VirtuosoQueryGeneratorExtend();
        String query = queryGenerator.generateQueryString(glycan);
        return query;
    }

    @With(StructureSearchLoggingAction.class)
    public Result glys3(){
        return ok(views.html.search.searchGlyS3.render());
    }

}


