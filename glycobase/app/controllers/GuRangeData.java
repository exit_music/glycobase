package controllers;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import static org.apache.commons.io.FileUtils.getFile;

/**
 * Created by s2980369 on 14/8/17.
 */
public class GuRangeData {

    private String minValue;
    private String maxValue;
    private String expType;

    public GuRangeData() {
    }

    public String getMinValue() {
        return minValue;
    }

    public void setMinValue(String minValue) {
        this.minValue = minValue;
    }

    public String getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(String maxValue) {
        this.maxValue = maxValue;
    }

    public String getExpType() {
        return expType;
    }

    public void setExpType(String expType) {
        this.expType = expType;
    }

    public static double getM5PgcTime() throws IOException {
        File file = getFile("/tmp/pgc_rt.json");
        BufferedReader b = new BufferedReader(new FileReader(file));
        String readLine = "";
        double M5value = 0;
        while ((readLine = b.readLine()) != null) {
            if (readLine.contains("\"M5\"")) {
                String pattern = "^.*\\[ \\[(.*)\\,\"M5\".*$";
                M5value = Double.parseDouble(readLine.replaceAll(pattern, "$1"));
            }
        }
        return M5value;
    }
}
