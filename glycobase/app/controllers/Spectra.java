package controllers;

import java.util.HashMap;

/**
 * Created by matthew on 15/07/16.
 */
public class Spectra {

    /*
    Temp. class to list spectra to viewed in glycan page
    Depends on connection with unicarb-db
    values of hashmap linked with json files
     */
    public HashMap spectraMap() {
        final HashMap<String, Integer> hmap = new HashMap<String, Integer>();
            hmap.put("M8", 10);
            hmap.put("M9", 11);
            hmap.put("A2BG(4)2S(6)1", 12);
            hmap.put("M6", 13);
            hmap.put("F(6)A2BG(4)1", 14);
            hmap.put("A2G(4)2F2", 15);
            hmap.put("F(6)A2BG(4)2", 16);
            hmap.put("F(6)A2BG(4)1S(6)1", 17);
            hmap.put("M4A1G(4)1S1", 18);
            hmap.put("A1G(4)1S1", 19);
            hmap.put("A2B", 2);
            hmap.put("F(6)A2BG(4)2S(6)1", 20);
            hmap.put("A2G(4)1S1", 21);
            hmap.put("A2F(3)G(4)2S(6)1", 22);
            hmap.put("M5A1G(4)1S1", 23);
            hmap.put("F(6)A2BG(4)2S(6)2", 24);
            hmap.put("F(6)A2", 25);
            hmap.put("F(6)A2F(3)2G(4)2", 26);
            hmap.put("M5", 27);
            hmap.put("A2G(4)2S(6)1", 28);
            hmap.put("F(6)M4A1G(4)1S1", 29);
            hmap.put("M5A1B", 3);
            hmap.put("F(6)A2G(4)2F", 30);
            hmap.put("A2G(4)2S2", 31);
            hmap.put("F(6)A1G(4)1S1", 32);
            hmap.put("F(6)A2G(4)1S(6)1", 33);
            hmap.put("F(6)A2F(3)G(4)2S1", 34);
            hmap.put("F(6)A2G(4)2S1", 35);
            hmap.put("F(6)A2F(3)G(4)2S1", 36);
            hmap.put("F(6)A2F(3)G(4)2S1", 37);
            hmap.put("A2F2G(4)2S1", 38);
            hmap.put("A2BG(4)1", 4);
            hmap.put("M4BA1G(4)1", 5);
            hmap.put("A2BG(4)2", 6);
            hmap.put("A2BG(4)1S(6)1", 7);
            hmap.put("F(6)A2B",8);
            hmap.put("M7", 9);

        return hmap;
    }

    public int searchSpectraMap(String uoxf) throws SpectraNotFoundException {
        int spectraId = 0;
        if(!spectraMap().containsKey(uoxf)) {
            spectraId = -1;
        } else {
            spectraId = (int) spectraMap().get(uoxf);
        }
        return spectraId;
    }

    public class SpectraNotFoundException extends Throwable {
        public SpectraNotFoundException(String uoxf) {
        }
    }
}
