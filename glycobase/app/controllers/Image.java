package controllers;

import org.apache.commons.io.IOUtils;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.eurocarbdb.application.glycanbuilder.*;
import org.jclouds.*;
import play.mvc.Controller;
import play.mvc.Result;
import sparql.SparqlEntity;
import sparql.SparqlException;
import sparql.glycostore.StoreSequenceById;
import sparql.glycobase.*;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by matthew on 9/12/15.
 */
public class Image extends Controller {

    public Result showImagePgcObjectStore(String glycostoreId, String notation) throws IOException {

        if (notation.matches("cfgl")) {
            notation = "cfg-uoxf";
        }

        String url = "https://swift.rc.nectar.org.au:8888/v1/AUTH_1507c81fb388480f88312a2d2f9189cd/lg-" + notation + "/tmp/lg-" + glycostoreId + "-" + notation + ".png";

        BufferedImage img = ImageIO.read(new URL(url));
        byte[] byteArray;
        byteArray = IOUtils.toByteArray(String.valueOf(img));
        ByteArrayInputStream input = new ByteArrayInputStream(byteArray);

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

            ImageIO.write(img, "png", byteArrayOutputStream);

        return ok(byteArrayOutputStream.toByteArray()).as("image/png");


    }

    /*
    this will need to removed once complete pgc testing
     */
    public Result showImagePgc(String glycostoreId, String notation, String extended) throws Exception {

        StoreSequenceById sequenceById = new StoreSequenceById();
        SparqlEntity sparqlentity = new SparqlEntity();
        sparqlentity.setValue(GlycanGlycobase.GlycoStoreId, glycostoreId);


        sequenceById.setSparqlEntity(sparqlentity);
        List<SparqlEntity> glycoCTList = Application.queryPgc(sequenceById.getSparql());
        String structure = glycoCTList.get(0).getValue("Sequence");

        BuilderWorkspace workspace;
        workspace = new BuilderWorkspace("/tmp/config", true, new GlycanRendererAWT());

        workspace.setNotation("cfg"); //cfgbw | uoxf | uoxfcol | text

        //GlycoCTCondensedParser parser = new GlycoCTCondensedParser(false);
        GlycoCTCondensedParser parser = new GlycoCTCondensedParser(true);


        String style = extended;
        String format = "png";


        if (notation.matches("cfgl")) {
            notation = "cfg";
        }
        if (notation.matches("uoxf")) {
            notation = "uoxf";
        }
        if (notation.matches("gs")) {
            notation = "iupac";
        }

        // parse sequence and create Sugar object


        List<Glycan> glycanList = new ArrayList<Glycan>();

        String op = MassOptions.NO_DERIVATIZATION;

        String cloud = "";


        MassOptions masso = new MassOptions();
        IonCloud ic = new IonCloud(cloud);
        //ic.add("-", 1);

        masso.ION_CLOUD.clear();
        masso.setDerivatization(op);

        String redEnd = "redEnd";
        masso.setReducingEndTypeString(redEnd);


        //Parse in a GlycoCT condensed string from text and pass in empty MassOptions
        Glycan glycan = parser.fromGlycoCTCondensed(structure.trim(), masso);
        //org.eurocarbdb.application.glycanbuilder.Glycan glycan = org.eurocarbdb.application.glycanbuilder.Glycan.fromString(structure);
        // glycan.setReducingEndType()
        //parser.readGlycan(structure, masso);


        //t_glycan = GlycoCTParser.fromSugar(t_sugar, monosaccharideConverter, t_visFromGlycoCT, new MassOptions(), true);

        // configure the image settings
        if (notation == null || notation.equalsIgnoreCase("cfg"))
            workspace.setNotation(GraphicOptions.NOTATION_CFG);
        else if (notation.equalsIgnoreCase("cfgbw"))
            workspace.setNotation(GraphicOptions.NOTATION_CFGBW);
        else if (notation.equalsIgnoreCase("cfg-uoxf"))
            workspace.setNotation(GraphicOptions.NOTATION_CFGLINK);
        else if (notation.equalsIgnoreCase("uoxf"))
            workspace.setNotation(GraphicOptions.NOTATION_UOXF);
        else if (notation.equalsIgnoreCase("uoxf-color"))
            workspace.setNotation(GraphicOptions.NOTATION_UOXFCOL);
        else if (notation.equalsIgnoreCase("iupac"))
            workspace.setNotation(GraphicOptions.NOTATION_TEXT);
        else
            throw new IllegalArgumentException("Notation " + notation + " is not supported");

        if (style == null || style.equalsIgnoreCase("compact"))
            workspace.setDisplay(GraphicOptions.DISPLAY_COMPACT);
        else if (style.equalsIgnoreCase("normal"))
            workspace.setDisplay(GraphicOptions.DISPLAY_NORMAL);
        else if (style.equalsIgnoreCase("extended"))
            workspace.setDisplay(GraphicOptions.DISPLAY_NORMALINFO);
        else
            throw new IllegalArgumentException("Notation style " + style + " is not supported");

        if (format == null)
            format = "png";
        ByteArrayOutputStream byteArrayOutputStream;
        if (format.equalsIgnoreCase("png") || format.equalsIgnoreCase("jpg") || format.equalsIgnoreCase("jpeg")) {
            // create a buffered image of scale 1
            BufferedImage img = workspace.getGlycanRenderer().getImage(glycan, true, false, true, 0.5);

            byteArrayOutputStream = new ByteArrayOutputStream();
            if (format.equalsIgnoreCase("png")) {
                createImageFile(glycostoreId, notation, img, byteArrayOutputStream);
                ImageIO.write(img, "png", byteArrayOutputStream);
            }else {
                createImageFile(glycostoreId, notation, img, byteArrayOutputStream);
                ImageIO.write(img, "jpg", byteArrayOutputStream);
            }
            return ok(byteArrayOutputStream.toByteArray()).as("image/png");
            //return byteArrayOutputStream.toByteArray();
        }
            /*else if (format.equalsIgnoreCase("svg")) {
                // create the SVG
                String t_svg = SVGUtils.getVectorGraphics((GlycanRendererAWT)glycanWorkspace.getGlycanRenderer(),new Union<Glycan>(t_glycan));
                return t_svg.getBytes();
            }*/


        else {
            throw new IllegalArgumentException("Image format " + format + " is not supported");
        }



    }


    public void createImageFile(String glycostoreId, String notation, BufferedImage img, ByteArrayOutputStream byteArrayOutputStream ){
        try {
            File outputfile = new File("/tmp/lg-" + glycostoreId + "-" + notation + ".png");
            ImageIO.write(img, "png", byteArrayOutputStream);
            ImageIO.write(img, "png", outputfile);
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    public Result showImageReaction(String glycobaseId, String notation, String extended) throws Exception {

        SequenceByID sequence = new SequenceByID();
        SparqlEntity sparqlentity = new SparqlEntity();
        sparqlentity.setValue(GlycanGlycobase.GlycoBaseId, URLDecoder.decode(glycobaseId, "UTF-8"));
        sequence.setSparqlEntity(sparqlentity);
        List<SparqlEntity> glycoCTList = Application.query(sequence.getSparql());
        String structure = glycoCTList.get(0).getValue("Sequence");

        BuilderWorkspace workspace;
        workspace = new BuilderWorkspace("/tmp/config", true, new GlycanRendererAWT());


        workspace.setNotation("cfg"); //cfgbw | uoxf | uoxfcol | text

        GlycoCTCondensedParser parser = new GlycoCTCondensedParser(false);


        String style = extended;
        String format = "png";


        if (notation.matches("cfgl")) {
            notation = "cfg";
        }
        if (notation.matches("uoxf")) {
            notation = "uoxf";
        }
        if (notation.matches("gs")) {
            notation = "iupac";
        }

        // parse sequence and create Sugar object


        List<Glycan> glycanList = new ArrayList<Glycan>();

        String op = MassOptions.NO_DERIVATIZATION;

        String cloud = "";


        MassOptions masso = new MassOptions();
        IonCloud ic = new IonCloud(cloud);
        //ic.add("-", 1);

        masso.ION_CLOUD.clear();
        masso.setDerivatization(op);

        String redEnd = "freeEnd";
        masso.setReducingEndTypeString(redEnd);


        //Parse in a GlycoCT condensed string from text and pass in empty MassOptions
        Glycan glycan = parser.fromGlycoCTCondensed(structure.trim(), masso);
        //org.eurocarbdb.application.glycanbuilder.Glycan glycan = org.eurocarbdb.application.glycanbuilder.Glycan.fromString(structure);
        // glycan.setReducingEndType()
        //parser.readGlycan(structure, masso);


        //t_glycan = GlycoCTParser.fromSugar(t_sugar, monosaccharideConverter, t_visFromGlycoCT, new MassOptions(), true);

        // configure the image settings
        if (notation == null || notation.equalsIgnoreCase("cfg"))
            workspace.setNotation(GraphicOptions.NOTATION_CFG);
        else if (notation.equalsIgnoreCase("cfgbw"))
            workspace.setNotation(GraphicOptions.NOTATION_CFGBW);
        else if (notation.equalsIgnoreCase("cfg-uoxf"))
            workspace.setNotation(GraphicOptions.NOTATION_CFGLINK);
        else if (notation.equalsIgnoreCase("uoxf"))
            workspace.setNotation(GraphicOptions.NOTATION_UOXF);
        else if (notation.equalsIgnoreCase("uoxf-color"))
            workspace.setNotation(GraphicOptions.NOTATION_UOXFCOL);
        else if (notation.equalsIgnoreCase("iupac"))
            workspace.setNotation(GraphicOptions.NOTATION_TEXT);
        else
            throw new IllegalArgumentException("Notation " + notation + " is not supported");

        if (style == null || style.equalsIgnoreCase("compact"))
            workspace.setDisplay(GraphicOptions.DISPLAY_COMPACT);
        else if (style.equalsIgnoreCase("normal"))
            workspace.setDisplay(GraphicOptions.DISPLAY_NORMAL);
        else if (style.equalsIgnoreCase("extended"))
            workspace.setDisplay(GraphicOptions.DISPLAY_NORMALINFO);
        else
            throw new IllegalArgumentException("Notation style " + style + " is not supported");

        if (format == null)
            format = "png";
        ByteArrayOutputStream byteArrayOutputStream;
        if (format.equalsIgnoreCase("png") || format.equalsIgnoreCase("jpg") || format.equalsIgnoreCase("jpeg")) {
            // create a buffered image of scale 1
            BufferedImage img = workspace.getGlycanRenderer().getImage(glycan, true, false, true, 0.5);


            byteArrayOutputStream = new ByteArrayOutputStream();
            if (format.equalsIgnoreCase("png"))
                ImageIO.write(img, "png", byteArrayOutputStream);
            else
                ImageIO.write(img, "jpg", byteArrayOutputStream);
            return ok(byteArrayOutputStream.toByteArray()).as("image/png");
            //return byteArrayOutputStream.toByteArray();
        }
            /*else if (format.equalsIgnoreCase("svg")) {
                // create the SVG
                String t_svg = SVGUtils.getVectorGraphics((GlycanRendererAWT)glycanWorkspace.getGlycanRenderer(),new Union<Glycan>(t_glycan));
                return t_svg.getBytes();
            }*/
        else {
            throw new IllegalArgumentException("Image format " + format + " is not supported");
        }
    }


    public Result showImage(String structure, String notation, String extended) throws Exception {


        BuilderWorkspace workspace;
        workspace = new BuilderWorkspace("/tmp/config", true, new GlycanRendererAWT());


        workspace.setNotation("cfg"); //cfgbw | uoxf | uoxfcol | text

        GlycoCTCondensedParser parser = new GlycoCTCondensedParser(false);


        String style = extended;
        String format = "png";


        if (notation.matches("cfgl")) {
            notation = "cfg";
        }
        if (notation.matches("uoxf")) {
            notation = "uoxf";
        }
        if (notation.matches("gs")) {
            notation = "iupac";
        }

        // parse sequence and create Sugar object


        List<Glycan> glycanList = new ArrayList<Glycan>();

        String op = MassOptions.NO_DERIVATIZATION;

        String cloud = "";


        MassOptions masso = new MassOptions();
        IonCloud ic = new IonCloud(cloud);
        //ic.add("-", 1);

        masso.ION_CLOUD.clear();
        masso.setDerivatization(op);

        String redEnd = "freeEnd";
        masso.setReducingEndTypeString(redEnd);


        //Parse in a GlycoCT condensed string from text and pass in empty MassOptions
        Glycan glycan = parser.fromGlycoCTCondensed(structure.trim(), masso);
        //org.eurocarbdb.application.glycanbuilder.Glycan glycan = org.eurocarbdb.application.glycanbuilder.Glycan.fromString(structure);
        // glycan.setReducingEndType()
        //parser.readGlycan(structure, masso);


        //t_glycan = GlycoCTParser.fromSugar(t_sugar, monosaccharideConverter, t_visFromGlycoCT, new MassOptions(), true);

        // configure the image settings
        if (notation == null || notation.equalsIgnoreCase("cfg"))
            workspace.setNotation(GraphicOptions.NOTATION_CFG);
        else if (notation.equalsIgnoreCase("cfgbw"))
            workspace.setNotation(GraphicOptions.NOTATION_CFGBW);
        else if (notation.equalsIgnoreCase("cfg-uoxf"))
            workspace.setNotation(GraphicOptions.NOTATION_CFGLINK);
        else if (notation.equalsIgnoreCase("uoxf"))
            workspace.setNotation(GraphicOptions.NOTATION_UOXF);
        else if (notation.equalsIgnoreCase("uoxf-color"))
            workspace.setNotation(GraphicOptions.NOTATION_UOXFCOL);
        else if (notation.equalsIgnoreCase("iupac"))
            workspace.setNotation(GraphicOptions.NOTATION_TEXT);
        else
            throw new IllegalArgumentException("Notation " + notation + " is not supported");

        if (style == null || style.equalsIgnoreCase("compact"))
            workspace.setDisplay(GraphicOptions.DISPLAY_COMPACT);
        else if (style.equalsIgnoreCase("normal"))
            workspace.setDisplay(GraphicOptions.DISPLAY_NORMAL);
        else if (style.equalsIgnoreCase("extended"))
            workspace.setDisplay(GraphicOptions.DISPLAY_NORMALINFO);
        else
            throw new IllegalArgumentException("Notation style " + style + " is not supported");

        if (format == null)
            format = "png";
        ByteArrayOutputStream byteArrayOutputStream;
        if (format.equalsIgnoreCase("png") || format.equalsIgnoreCase("jpg") || format.equalsIgnoreCase("jpeg")) {
            // create a buffered image of scale 1
            BufferedImage img = workspace.getGlycanRenderer().getImage(glycan, true, false, true, 2);


            byteArrayOutputStream = new ByteArrayOutputStream();
            if (format.equalsIgnoreCase("png"))
                ImageIO.write(img, "png", byteArrayOutputStream);
            else
                ImageIO.write(img, "jpg", byteArrayOutputStream);
            return ok(byteArrayOutputStream.toByteArray()).as("image/png");
            //return byteArrayOutputStream.toByteArray();
        }
            /*else if (format.equalsIgnoreCase("svg")) {
                // create the SVG
                String t_svg = SVGUtils.getVectorGraphics((GlycanRendererAWT)glycanWorkspace.getGlycanRenderer(),new Union<Glycan>(t_glycan));
                return t_svg.getBytes();
            }*/
        else {
            throw new IllegalArgumentException("Image format " + format + " is not supported");
        }

        //return byteArrayOutputStream.toByteArray();
    }
}