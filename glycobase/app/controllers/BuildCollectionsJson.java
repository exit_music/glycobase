package controllers;


import com.google.common.collect.Ordering;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.inject.Inject;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import play.data.DynamicForm;

import play.data.FormFactory;
import play.mvc.Result;
import sparql.SparqlEntity;
import sparql.SparqlException;
import sparql.glycobase.GlycanGlycobase;
import sparql.glycobase.ListReport;
import sparql.glycobase.StructureByReport;
import sparql.glycostore.Comments;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.ExecutionException;

import static controllers.Application.query;
import static play.mvc.Results.ok;

/**
 * Created by s2980369 on 4/7/17.
 */
public class BuildCollectionsJson {

    @Inject
    private FormFactory formFactory;

    public static ArrayList<String> displayTopRefs(String collectionName) {
        System.out.println("Collection Name: " + collectionName);
        ArrayList<String> displayRef = new ArrayList<String>();
        JSONParser parser = new JSONParser();
        JSONArray collection = null;
        try {
            Object object = parser
                    .parse(new FileReader("/tmp/collections.json"));

            //convert Object to JSONObject
            JSONObject jsonObject = (JSONObject) object;

            //Reading the String

            //Reading the array
            collection = (JSONArray) jsonObject.get(collectionName);

            for (Object a : collection) {
                JSONObject jsonLineItem = (JSONObject) a;
                // JSONObject O = (JSONObject) collection.get(0);
                System.out.println("\t" + jsonLineItem.get("lead"));
                //String display = jsonLineItem.get("lead") + ", " + jsonLineItem.get("title") + ", " + jsonLineItem.get("year");


                String display = "<a href=\"/reference/" + jsonLineItem.get("title") + "\"><span class=\"badge\">" + jsonLineItem.get("lead") + ", " + jsonLineItem.get("title") + ", " + jsonLineItem.get("year") + "</span></a>";

                displayRef.add(display);
            }


        } catch (FileNotFoundException fe) {
            fe.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return displayRef;
    }

    //some of the collections are large
    //slow sparql queries, requires some work

    public Result buildCollectionsJson(String reportName) throws InterruptedException, SparqlException, ExecutionException, IOException {

        System.out.println("REPORT NAME " + reportName);

        String reportNameJson = reportName.replaceAll(" ", "_").replaceAll("\\(", "").replaceAll("\\)", "");

        //System.out.println(reportNameJson);

//        String contents = new String(Files.readAllBytes(Paths.get("/tmp/" + reportNameJson + ".json")));
        String contents = new String(Files.readAllBytes(Paths.get("public", "reports/" + reportNameJson + ".json")));
        String[] i = contents.split("\\\",");

        Map<String, String> m = new HashMap<String, String>();

        for(String ii : i){
            String [] x = ii.split("\\\":");
            //System.out.println("i " + x[0] );

            if (!x[0].contains("\\}") && !x[0].contains("}\"")) {

                x[0] = x[0].replaceAll("\\\\", "").replaceAll("\"", "").replace("{","");
                x[1] = x[1].replaceAll("\"", "").replaceAll("\\\\$", "").replaceAll("^\\\\", "").replaceAll("\\\\}$", "");
                x[1] = x[1].replaceAll("\\\\t", "\t").replaceAll("\\\\", "");
                m.put(x[0], x[1]);

                System.out.println("i " + x[0] );
            }
        }

        DynamicForm dynamicForm = formFactory.form().bindFromRequest();
        m = Application.filterStructuresMotif(dynamicForm, m);

        int count = m.size();

        ListReport allReports = new ListReport();
        SparqlEntity repSpqlEntity = new SparqlEntity();
        allReports.setSparqlEntity(repSpqlEntity);
        List<SparqlEntity> allReportList = query(allReports.getSparql());

        Set<String> uniqueReports = new HashSet<String>();
        for (SparqlEntity s : allReportList) {
            uniqueReports.add(s.getValue("ReportName"));
        }

        List<String> uniqueReportsList = new ArrayList<String>(uniqueReports);
        List<String> uniqueReportsListOrdered = Ordering.from(String.CASE_INSENSITIVE_ORDER).sortedCopy(uniqueReportsList);

        ArrayList<String> displayTopRefs = displayTopRefs(reportNameJson);
        for(String s : displayTopRefs){
            System.out.println("list check " + s);
        }


        //get comments if any on data report
        Comments comments = new Comments();
        SparqlEntity sparqlentity = new SparqlEntity();
        String reportNameModified = reportName;
        sparqlentity.setValue(GlycanGlycobase.ReportName, reportName);
        comments.setSparqlEntity(sparqlentity);
        List<SparqlEntity> reportComment = query(comments.getSparql());

        return ok(views.html.test.testSearchReport.render(count, reportName, m, uniqueReportsListOrdered, displayTopRefs, reportComment));



    }


    private static String getReport(String reportName) throws InterruptedException, SparqlException, ExecutionException, IOException {
        StructureByReport report = new StructureByReport();
        SparqlEntity sparqlentity = new SparqlEntity();
        sparqlentity.setValue(GlycanGlycobase.ReportName, reportName);
        report.setSparqlEntity(sparqlentity);

        List<SparqlEntity> reportSpqList = query(report.getSparql());

        Map<String, String> avgMap = AvgTest.testAvg(reportSpqList);


        Gson gson = new Gson();
        String json = gson.toJson(avgMap);


        try (Writer writer = new FileWriter("/tmp/Output.json")) {
            gson = new GsonBuilder().create();
            gson.toJson(json, writer);
        }
        return json;
    }

    private static String lookupCollectionsFile(String reportName){

        //Represents reports, stored in public folder
        String [] jsonFiles = {"BTINglycans.json", "BTIgsl.json", "Ludger.json", "Milk.json", "NibrtPublic.json", "Oglycans.json", "Published.json", "Reverse.json", "Royle.json", "ce.json"};

        //if(reportName.matches("");
        return "test";

    }
}
