package controllers;


import com.google.common.base.Predicate;
import com.google.common.base.Stopwatch;
import com.google.common.collect.Maps;
import com.google.common.collect.Ordering;
import com.google.inject.Inject;
import org.apache.jena.query.*;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;


//import org.eurocarbdb.dataaccess.hplc.Glycan;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import play.Logger;
import play.Logger.ALogger;
import play.data.DynamicForm;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.*;
import sparql.SparqlEntity;
import sparql.SparqlException;
import sparql.glycobase.*;
import sparql.glycostore.*;
import sparql.motif.OlinkStructureFilterFeature;
import views.html.*;


import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ExecutionException;


class AccessLoggingAction extends Action.Simple {

    private ALogger accessLogger = Logger.of("access");

    public CompletionStage<Result> call(Http.Context ctx) {
        final Http.Request request = ctx.request();
        accessLogger.info("method={} uri={} remote-address={}", request.method(), request.uri(), request.remoteAddress(), request.queryString(), request.host());
        return delegate.call(ctx);
    }
}

public class Application extends Controller {

    @Inject private FormFactory formFactory;
    private static final ALogger logger = Logger.of(Application.class);

    public Http.Session onRequest(Http.Request request) {

        if(session("notation") == null ) {
            session("notation", "cfg");
        }
        return session();
    }

    @With(AccessLoggingAction.class)
    public Result index(){

        if(session("notation") == null ) {
            session("notation", "cfg");
        }
        return ok(index.render());
    }



    public static List<SparqlEntity> query(String select){

//       System.out.println("this is the query: " + select);

        Query query = QueryFactory.create(select);

       //QueryExecution qe = QueryExecutionFactory.sparqlService("http://137.92.56.159:40935/testdec/query",query);
        //QueryExecution qe = QueryExecutionFactory.sparqlService("http://137.92.56.159:40935/feb2018b/query",query);
        QueryExecution qe = QueryExecutionFactory.sparqlService("http://137.92.56.159:40935/production/query",query);
//        QueryExecution qe = QueryExecutionFactory.sparqlService("http://localhost:3030/glycostore/query",query);
        ResultSet rs = qe.execSelect();
        List<SparqlEntity> results = new ArrayList<SparqlEntity>();

        while (rs.hasNext()) {
            QuerySolution row = rs.next();
            Iterator<String> columns = row.varNames();
            SparqlEntity se = new SparqlEntity();
            while (columns.hasNext()) {
                String column = columns.next();
                RDFNode cell = row.get(column);

                if (cell.isResource()) {
                    Resource resource =  cell.asResource();
                    //do something maybe with the OntModel???
                    if (resource.isLiteral())
                        se.setValue(column, resource.asLiteral().getString());
                    else
                        se.setValue(column, resource.toString());
                }
                else if (cell.isLiteral()) {
                    se.setValue(column, cell.asLiteral().getString());
                } else if (cell.isAnon()) {
                    se.setValue(column, "anon");
                } else {
                    se.setValue(column, cell.toString());
                }
            }
            results.add(se);
        }
        qe.abort();
        qe.close();
        return results;
    }

    public static List<SparqlEntity> queryPgc(String select){

        Query query = QueryFactory.create(select);

        QueryExecution qe = QueryExecutionFactory.sparqlService("http://137.92.56.159:40935/feb2018b/query",query);


        ResultSet rs = qe.execSelect();

        List<SparqlEntity> results = new ArrayList<SparqlEntity>();

        while (rs.hasNext()) {
            QuerySolution row = rs.next();
            Iterator<String> columns = row.varNames();
            SparqlEntity se = new SparqlEntity();
            while (columns.hasNext()) {
                String column = columns.next();
                RDFNode cell = row.get(column);

                if (cell.isResource()) {
                    Resource resource =  cell.asResource();
                    //do something maybe with the OntModel???
                    if (resource.isLiteral())
                        se.setValue(column, resource.asLiteral().getString());
                    else
                        se.setValue(column, resource.toString());
                }
                else if (cell.isLiteral()) {
                    se.setValue(column, cell.asLiteral().getString());
                } else if (cell.isAnon()) {
                    se.setValue(column, "anon");
                } else {
                    se.setValue(column, cell.toString());
                }
            }
            results.add(se);
        }
        qe.abort();
        qe.close();
        return results;
    }

    public static String firstLetterCaps(String input){
        String firstLetter = input.substring(0,1).toUpperCase();
        String restLetters = input.substring(1).toLowerCase();
        return firstLetter + restLetters;
    }


    /**
     *
     * show all structures
     * link this to glycoct and builder
     */
    @With(AccessLoggingAction.class)
    public Result listAllStructures() {
        return ok(listAllStructures.render());
    }


    /**
     * using localhost PGC for testing purposes
     * @return
     * @throws SparqlException
     */
    public Result showPgc() throws SparqlException, IOException {

        double M5 = GuRangeData.getM5PgcTime();

        if(session("notation") == null ) {
            session("notation", "cfg");
        }


        PgcStructureAvgRT pgcStructureAvgRT = new PgcStructureAvgRT();
        SparqlEntity sparqlEntity = new SparqlEntity();
        pgcStructureAvgRT.setSparqlEntity(sparqlEntity);
        //List<SparqlEntity> pgcList = queryPgc(pgcStructureAvgRT.getSparql());
        List<SparqlEntity> pgcList = query(pgcStructureAvgRT.getSparql());

        return ok(views.html.search.showPgc.render(pgcList, M5));
    }

    @With(AccessLoggingAction.class)
    public Result showPgcGlycan(String glycostoreId) throws SparqlException {

        if(session("notation") == null ) {
            session("notation", "cfg");
        }

        PgcStructureRT record = new PgcStructureRT();
        SparqlEntity spqlentityRec = new SparqlEntity();
        spqlentityRec.setValue(GlycanGlycobase.GlycoStoreId , glycostoreId);
        record.setSparqlEntity(spqlentityRec);
        //List<SparqlEntity> RecordList = queryPgc(record.getSparql());
        List<SparqlEntity> structureInfo = query(record.getSparql());

        System.out.println("check in");
        SampleSourceStructure sampleSourceStructure = new SampleSourceStructure();
        //SampleSourceStructure sampleSourceStructure = new SampleSourceStructure();
        System.out.println("check out");
        SparqlEntity sparqlEntitySource = new SparqlEntity();
        sparqlEntitySource.setValue(GlycanGlycobase.GlycoStoreId, glycostoreId);
        sampleSourceStructure.setSparqlEntity(sparqlEntitySource);
        List<SparqlEntity> sourceList = query(sampleSourceStructure.getSparql());

        ArrayList<String> glycoproteins = new ArrayList<String>();
        glycoproteins.add("Human IgG");
        glycoproteins.add("Human IgA");
        glycoproteins.add("RNAseB");
        glycoproteins.add("Ovalbumn");
        glycoproteins.add("Alpha-1-Acid Glycoprotein");
        glycoproteins.add("Lactoferrin");

       for(SparqlEntity s : sourceList){
            glycoproteins.remove(s.getValue("SampleName"));
        }


        String jsonTest = "";
        int spectraId = Integer.parseInt(glycostoreId);
        if (spectraId > 0) {
            JSONParser parser = new JSONParser();
            Object obj = null;
            try {
                String jsonFile = "/tmp/out" + spectraId + ".json";
                obj = parser.parse(new FileReader(jsonFile));
                jsonTest = ((JSONObject) obj).toString();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return ok(views.html.search.showPgcGlycan.render(structureInfo, jsonTest, glycostoreId, sourceList, glycoproteins));

    }


    @With(AccessLoggingAction.class)
    public Result queryoptions() throws SparqlException{

        if(session("notation") == null ) {
            session("notation", "cfg");
        }

        ListReport report = new ListReport();
        SparqlEntity repSpqlEntity = new SparqlEntity();
        report.setSparqlEntity(repSpqlEntity);
        List<SparqlEntity> reportList = query(report.getSparql());

        ListSample sample = new ListSample();
        SparqlEntity sampSpqlEntity = new SparqlEntity();
        sample.setSparqlEntity(sampSpqlEntity);
        List<SparqlEntity> sampleList = query(sample.getSparql());

        ListProtein protein = new ListProtein();
        SparqlEntity proteinSpqlEntity = new SparqlEntity();
        protein.setSparqlEntity(proteinSpqlEntity);
        List<SparqlEntity> proteinList = query(protein.getSparql());

        ListTaxon taxon = new ListTaxon();
        SparqlEntity taxonSpqlEntity = new SparqlEntity();
        taxon.setSparqlEntity(taxonSpqlEntity);
        List<SparqlEntity> taxonList = query(taxon.getSparql());

        ListTissue tissue = new ListTissue();
        SparqlEntity tissueSpqlEntity = new SparqlEntity();
        tissue.setSparqlEntity(tissueSpqlEntity);
        List<SparqlEntity> tissueList = query(tissue.getSparql());

        return ok(query.render(sampleList, proteinList, reportList, tissueList, taxonList));

    }

    /*
    Only applies to N-links due to uoxf dependency when checking features e.g F(6) and B(isect)
    For reviewers need to expand to include O-links
     */
    @With(AccessLoggingAction.class)
    public Result searchGuMotif(String expType, String minValue, String maxValue, Integer core, Integer bisect, Integer outer, Integer hybrid, Integer mannose ) throws SparqlException {

        if(session("notation") == null ) {
            session("notation", "cfg");
        }

        StructureByGuRangeMotif structureByGuRangeMotif = new StructureByGuRangeMotif();
        SparqlEntity sparqlentity = new SparqlEntity();
        sparqlentity.setValue(GlycanGlycobase.EvidenceType, expType);
        sparqlentity.setValue(GlycanGlycobase.GuLowBoundary, minValue);
        sparqlentity.setValue(GlycanGlycobase.GuHighBoundary, maxValue);

         /*
        ** testing motif type search on uoxf name, this will need to change
        */
        if(core == 1) {
            sparqlentity.setValue(GlycanGlycobase.CoreFMotif, "F(6)");
        }
        if(bisect ==1) {
            sparqlentity.setValue(GlycanGlycobase.BisectMotif, "B");
        }
        if(outer == 1) {
            sparqlentity.setValue(GlycanGlycobase.OuterFMotif,  StructureByGuRangeMotif.setUoxfOuterFucose());
        }
        if(hybrid == 1){
            sparqlentity.setValue(GlycanGlycobase.HybridMotif, "A");
        }
        if(mannose == 1){
            sparqlentity.setValue(GlycanGlycobase.MannoseMotif, "M");
        }


        structureByGuRangeMotif.setSparqlEntity(sparqlentity);

        List<SparqlEntity> sparqlEntityList = query(structureByGuRangeMotif.getSparql());


        //int length = sparqlEntityList.map(List::size).orElse(0);
        int length = sparqlEntityList.size();

        for(SparqlEntity s : sparqlEntityList){
            if(s.getValue("SaccharideURI") == null){
                return ok(views.html.errors.noResults.render());
            }
        }
        return ok(views.html.search.searchFilterMotif.render(length,minValue,maxValue,expType,sparqlEntityList, core, bisect, outer, hybrid, mannose));
    }



    @With(AccessLoggingAction.class)
    public Result searchGuMotifOlinked(String expType, String minValue, String maxValue, Integer core1, Integer core2, Integer bloodgrouph, Integer linearbloodgroupb, Integer bloodgroupi, Integer bloodgrouplewisa, Integer bloodgrouplewisx, Integer bloodgroupslewisx ) throws SparqlException {

        if(session("notation") == null ) {
            session("notation", "cfg");
        }

        OlinkStructureFilterFeature olinkStructureFilterFeature = new OlinkStructureFilterFeature();
        SparqlEntity sparqlEntity = new SparqlEntity();
        sparqlEntity.setValue(GlycanGlycobase.EvidenceType, expType);
        sparqlEntity.setValue(GlycanGlycobase.GuLowBoundary, minValue);
        sparqlEntity.setValue(GlycanGlycobase.GuHighBoundary, maxValue);


        if(core1 == 1) {
            sparqlEntity.setValue(GlycanGlycobase.Core1, GlycanGlycobase.Core1);
        }

        if(core2 == 1) {
            sparqlEntity.setValue(GlycanGlycobase.Core2, GlycanGlycobase.Core2);
        }

        if(bloodgrouph == 1) {
            sparqlEntity.setValue(GlycanGlycobase.BloodGroupH, GlycanGlycobase.BloodGroupH);
        }

        if(linearbloodgroupb == 1) {
            sparqlEntity.setValue(GlycanGlycobase.LinearBloodGroupB, GlycanGlycobase.LinearBloodGroupB);
        }

        if(bloodgroupi == 1) {
            sparqlEntity.setValue(GlycanGlycobase.BloodGroupi, GlycanGlycobase.BloodGroupi);
        }


        if(bloodgrouplewisa == 1) {
            sparqlEntity.setValue(GlycanGlycobase.BloodGroupLewisA, GlycanGlycobase.BloodGroupLewisA);
        }

        if(bloodgrouplewisx == 1) {
            sparqlEntity.setValue(GlycanGlycobase.BloodGroupLewisX, GlycanGlycobase.BloodGroupLewisX);
        }


        if(bloodgroupslewisx == 1) {
            sparqlEntity.setValue(GlycanGlycobase.BloodGroupSialylLewisX, GlycanGlycobase.BloodGroupSialylLewisX);
        }

        olinkStructureFilterFeature.setSparqlEntity(sparqlEntity);

        List<SparqlEntity> sparqlEntityList = query(olinkStructureFilterFeature.getSparql());

        int length = sparqlEntityList.size();

        for(SparqlEntity s : sparqlEntityList){
            if(s.getValue("SaccharideURI") == null){
                return ok(views.html.errors.noResults.render());
            }
        }

        return ok(views.html.search.searchGuMotifOlinked.render(length,minValue,maxValue,expType,sparqlEntityList, core1, core2, bloodgrouph, linearbloodgroupb, bloodgroupi, bloodgrouplewisa, bloodgrouplewisx, bloodgroupslewisx));
    }

    @With(AccessLoggingAction.class)
    public Result searchGu(String expType, String minValue, String maxValue, int core, int bisect, int outer, int hybrid, int mannose ) throws SparqlException{

        if(session("notation") == null ) {
            session("notation", "cfg");
        }
        System.out.println("experiment type: "+expType);

        StructureByGuRange structure = new StructureByGuRange();
        SparqlEntity sparqlentity = new SparqlEntity();
        sparqlentity.setValue(GlycanGlycobase.EvidenceType, expType);
        sparqlentity.setValue(GlycanGlycobase.GuLowBoundary, minValue);
        sparqlentity.setValue(GlycanGlycobase.GuHighBoundary, maxValue);
        structure.setSparqlEntity(sparqlentity);
        List<SparqlEntity> sparqlEntityList = query(structure.getSparql());

        Map<String,List<String>> multiMap = new HashMap<String,List<String>>();
        // experiment types: hplc, uplc_2-ab, uplc_procainamide, rpuplc, ce, pgc
        for(SparqlEntity x: sparqlEntityList){
            String id = x.getValue("GlycoBaseId");
            String uoxf = x.getValue("Uoxf");
            if(expType.equals("hplc")){
                List<String> valueList = new ArrayList<>();
                valueList.add(uoxf);
                valueList.addAll(meanStdHplcGu(id));
                multiMap.put(id,valueList);
            }else if(expType.equals("uplc_2-ab")){
                List<String> valueList = new ArrayList<>();
                valueList.add(uoxf);
                valueList.addAll(meanStdUplc2AB(id));
                multiMap.put(id,valueList);
            }else if(expType.equals("uplc_procainamide")){
                List<String> valueList = new ArrayList<>();
                valueList.add(uoxf);
                valueList.addAll(meanStdUplcProc(id));
                multiMap.put(id,valueList);
            }else if(expType.equals("ce")){
                List<String> valueList = new ArrayList<>();
                valueList.add(uoxf);
                valueList.addAll(meanStdCeGu(id));
                multiMap.put(id,valueList);
            }else if(expType.equals("pgc")){
                List<String> valueList = new ArrayList<>();
                valueList.add(uoxf);
                valueList.addAll(meanStdPgc(id));
                multiMap.put(id,valueList);
            }else if(expType.equals("rpuplc")){
                List<String> valueList = new ArrayList<>();
                valueList.add(uoxf);
                valueList.add(avgRp(id));
                valueList.add("NaN");
                multiMap.put(id,valueList); // rpuplc has only one profile, no std value
            }else{
                System.out.println("exp type doesn't meet your filter");
            }
        }
//        System.out.println(multiMap);


//        List<SparqlEntity> sparqlEntityList = null;
//
//        if(GlycanGlycobase.EvidenceType.matches("pgc")) {
//            sparqlEntityList = query(structure.getSparql());
//            int length = sparqlEntityList.size();
//        } else{
//            sparqlEntityList = query(structure.getSparql());
//        }

        String olink = checkOlinkResult(sparqlEntityList);

        int length = sparqlEntityList.size();
        System.out.println(length);
        return ok(views.html.search.searchGuResult.render(length,minValue,maxValue,expType,multiMap, core, bisect, outer, hybrid, mannose, olink));
    }

    Form<GuRangeData> form;
    @With(AccessLoggingAction.class)
    public Result getGuRange(){

        DynamicForm dynamicForm = formFactory.form().bindFromRequest();
        String minValue = dynamicForm.get("Minimum");
        String maxValue = dynamicForm.get("Maximum");
        String expType = dynamicForm.get("type").toLowerCase();

        Map<String, String> formMap = dynamicForm.data();

        int core = 0;
        int bisect = 0;
        int outer =0;
        int hybrid = 0;
        int mannose = 0;
        int core1 = 0;
        int core2 = 0;
        int bloodgrouph = 0;
        int linearbloodgroupb = 0;
        int bloodgroupi = 0;
        int bloodgrouplewisa  = 0;
        int bloodgrouplewisx = 0;
        int bloodgroupslewisx = 0;

        if(formMap.containsKey("core")) {
            core = Integer.parseInt(formMap.get("core"));
        }
        if(formMap.containsKey("bisect")) {
            bisect = Integer.parseInt(formMap.get("bisect"));
        }
        if(formMap.containsKey("outer")) {
            core = Integer.parseInt(formMap.get("outer"));
        }
        if(formMap.containsKey("hybrid")) {
            core = Integer.parseInt(formMap.get("hybrid"));
        }
        if(formMap.containsKey("mannose")) {
            core = Integer.parseInt(formMap.get("mannose"));
        }

        if(formMap.containsKey("core1")) {
            core1 =Integer.parseInt(formMap.get("core1"));
        }

        if(formMap.containsKey("core2")) {
            core2 =Integer.parseInt(formMap.get("core2"));
        }

        if(formMap.containsKey("bloodgrouph")) {
            bloodgrouph =Integer.parseInt(formMap.get("bloodgrouph"));
        }

        if(formMap.containsKey("linearbloodgroupb")) {
            linearbloodgroupb =Integer.parseInt(formMap.get("linearbloodgroupb"));
        }

        if(formMap.containsKey("bloodgroupi")) {
            bloodgroupi =Integer.parseInt(formMap.get("bloodgroupi"));
        }

        if(formMap.containsKey("bloodgrouplewisa")) {
            bloodgrouplewisa =Integer.parseInt(formMap.get("bloodgrouplewisa"));
        }

        if(formMap.containsKey("bloodgrouplewisx")) {
            bloodgrouplewisx =Integer.parseInt(formMap.get("bloodgrouplewisx"));
        }

        if(formMap.containsKey("bloodgroupslewisx")) {
            bloodgroupslewisx =Integer.parseInt(formMap.get("bloodgroupslewisx"));
        }



        if(core >0 || bisect  >0 || outer  >0 || hybrid  >0 || mannose  >0  || core1 >0 || core2 >0 || bloodgrouph >0 || linearbloodgroupb >0 || bloodgroupi >0 || bloodgrouplewisa >0 || bloodgrouplewisx >0 || bloodgroupslewisx >0) {
            return redirect(routes.Application.searchGuMotif(expType, minValue, maxValue, core, bisect, outer, hybrid, mannose));
        }
        else {
            return redirect(routes.Application.searchGu(expType, minValue, maxValue, core, bisect, outer, hybrid, mannose));
        }
    }

    @With(AccessLoggingAction.class)
    public Result searchSample(String sampleName) throws SparqlException, ExecutionException, InterruptedException {

        if(session("notation") == null ) {
            session("notation", "cfg");
        }

        StructureBySample sample = new StructureBySample();
        SparqlEntity sparqlentity = new SparqlEntity();
        sparqlentity.setValue(GlycanGlycobase.SampleName, sampleName);
        sample.setSparqlEntity(sparqlentity);
        List<SparqlEntity> sparqlEntityList = query(sample.getSparql());
        int length = sparqlEntityList.size();

        Map<String, String>  avgMap = AvgTest.testAvg(sparqlEntityList);

        DynamicForm dynamicForm = formFactory.form().bindFromRequest();
        avgMap = filterStructuresMotif(dynamicForm, avgMap);

        ListSample sampleList = new ListSample();
        SparqlEntity sampSpqlEntity = new SparqlEntity();
        sampleList.setSparqlEntity(sampSpqlEntity);
        List<SparqlEntity> allsampleList = query(sampleList.getSparql());

        Set<String> uniqueSamples = new HashSet<String>();
        for(SparqlEntity s : allsampleList){
            uniqueSamples.add(s.getValue("SampleName")); // Can we order these sample names in alphabetic order?
        }


        List<String> uniqueSamplesList = new ArrayList<String>(uniqueSamples);
        List<String> uniqueSampleListOrdered = Ordering.from(String.CASE_INSENSITIVE_ORDER).sortedCopy(uniqueSamplesList);

        return ok(views.html.search.searchSample.render(length, sampleName, avgMap, uniqueSampleListOrdered));
    }

    //@Inject private FormFactory formFactorySample;
    @With(AccessLoggingAction.class)
    public Result getSampleName() throws SparqlException, ExecutionException, InterruptedException {

        DynamicForm dynamicForm = formFactory.form().bindFromRequest();
        String sampleName = dynamicForm.get("SampleName");
        return redirect(routes.Application.searchSample(sampleName));

    }

    @With(AccessLoggingAction.class)
    public Result searchReport(String reportName) throws SparqlException, ExecutionException, InterruptedException {

        if(session("notation") == null ) {
            session("notation", "cfg");
        }

        StructureByReport report = new StructureByReport();
        SparqlEntity sparqlentity = new SparqlEntity();
        sparqlentity.setValue(GlycanGlycobase.ReportName, reportName);
        report.setSparqlEntity(sparqlentity);

        List<SparqlEntity> reportSpqList = query(report.getSparql());

        Stopwatch timer = Stopwatch.createStarted();
        Map<String, String> avgMap = AvgTest.testAvg(reportSpqList);

        System.out.println("Time: " + timer.stop());

        DynamicForm dynamicForm = formFactory.form().bindFromRequest();
        //DynamicForm dynamicForm = Form.form().bindFromRequest();
        avgMap = filterStructuresMotif(dynamicForm, avgMap);


        int count = reportSpqList.size();

        ListReport allReports = new ListReport();
        SparqlEntity repSpqlEntity = new SparqlEntity();
        allReports.setSparqlEntity(repSpqlEntity);
        List<SparqlEntity> allReportList = query(allReports.getSparql());

        Set<String> uniqueReports = new HashSet<String>();
        for (SparqlEntity s : allReportList) {
            uniqueReports.add(s.getValue("ReportName"));
        }

        List<String> uniqueReportsList = new ArrayList<String>(uniqueReports);
        List<String> uniqueReportsListOrdered = Ordering.from(String.CASE_INSENSITIVE_ORDER).sortedCopy(uniqueReportsList);

        return ok(views.html.search.searchReportResult.render(count, reportName, avgMap, uniqueReportsListOrdered));
    }

    @With(AccessLoggingAction.class)
    public static SparqlEntity gslExtraInfo(String glycostoreId) throws SparqlException{
        GSLInfo gsl = new GSLInfo();
        SparqlEntity sparqlEntityGSL = new SparqlEntity();
        sparqlEntityGSL.setValue(GlycanGlycobase.GlycoStoreId, glycostoreId);
        gsl.setSparqlEntity(sparqlEntityGSL);
        List<SparqlEntity> gslSpqlEntityList = query(gsl.getSparql());

        return gslSpqlEntityList.get(0);
    }

    @With(AccessLoggingAction.class)
    public Result getReportName() throws SparqlException, ExecutionException, InterruptedException {

        DynamicForm dynamicForm = formFactory.form().bindFromRequest();
        //DynamicForm dynamicForm = Form.form().bindFromRequest();
        String reportName = dynamicForm.get("report");
        return redirect(routes.Application.searchReport(reportName));
    }

    @With(AccessLoggingAction.class)
    public Result searchTissue(String tissueName) throws SparqlException, ExecutionException, InterruptedException {

        if(session("notation") == null ) {
            session("notation", "cfg");
        }

        StructureByTissue tissue = new StructureByTissue();
        SparqlEntity sparqlentity = new SparqlEntity();
        sparqlentity.setValue(GlycanGlycobase.Tissue, tissueName);
        tissue.setSparqlEntity(sparqlentity);
        List<SparqlEntity> sparqlEntityList = query(tissue.getSparql());
        int length = sparqlEntityList.size();
        Map<String, String>  avgMap = AvgTest.testAvg(sparqlEntityList);

        return ok(views.html.search.searchTissueResult.render(length, tissueName, avgMap));
    }

    @With(AccessLoggingAction.class)
    public Result getTissueName() throws SparqlException, ExecutionException, InterruptedException {


        DynamicForm dynamicForm = formFactory.form().bindFromRequest();
        //DynamicForm dynamicForm = Form.form().bindFromRequest();
        String tissueName = dynamicForm.get("tissue");
        return redirect(routes.Application.searchTissue(tissueName));
    }

    @With(AccessLoggingAction.class)
    public Result searchTaxon(String taxonName) throws SparqlException, ExecutionException, InterruptedException {

        if(session("notation") == null ) {
            session("notation", "cfg");
        }

        StructureByTaxon taxon = new StructureByTaxon();
        SparqlEntity sparqlentity = new SparqlEntity();
        sparqlentity.setValue(GlycanGlycobase.Taxon, taxonName);
        taxon.setSparqlEntity(sparqlentity);
        List<SparqlEntity> sparqlEntityList = query(taxon.getSparql());
        int length = sparqlEntityList.size();
        Map<String, String>  avgMap = AvgTest.testAvg(sparqlEntityList);


        ListTaxon taxonList = new ListTaxon();
        SparqlEntity taxonSpqlEntity = new SparqlEntity();
        taxonList.setSparqlEntity(taxonSpqlEntity);
        List<SparqlEntity> allTaxonList = query(taxonList.getSparql());

        Set<String> uniqueTaxon = new HashSet<String>();
        for(SparqlEntity s : allTaxonList){
            uniqueTaxon.add(s.getValue("Taxon"));
        }

        List<String> uniqueTaxonList = new ArrayList<String>(uniqueTaxon);
        List<String> uniqueTaxonListOrdered = Ordering.from(String.CASE_INSENSITIVE_ORDER).sortedCopy(uniqueTaxonList);


        return ok(views.html.search.searchTaxonResult.render(length, firstLetterCaps(taxonName), avgMap, uniqueTaxonListOrdered));
    }

    @With(AccessLoggingAction.class)
    public Result getTaxonName() throws SparqlException, ExecutionException, InterruptedException {

        DynamicForm dynamicForm = formFactory.form().bindFromRequest();
        //DynamicForm dynamicForm = Form.form().bindFromRequest();
        String taxonName = dynamicForm.get("TaxonName").toUpperCase();
        return redirect(routes.Application.searchTaxon(taxonName));
    }

    @With(AccessLoggingAction.class)
    public Result searchID(String glycobaseID) throws SparqlException{

        if(session("notation") == null ) {
            session("notation", "cfg");
        }

        UoxfByID id = new UoxfByID();
        SparqlEntity sparqlentity = new SparqlEntity();
        sparqlentity.setValue(GlycanGlycobase.GlycoBaseId, glycobaseID);
        id.setSparqlEntity(sparqlentity);
        List<SparqlEntity> sparqlEntityList = query(id.getSparql());
        if(sparqlEntityList.size() == 0){
            int length = 0;
            return ok(views.html.search.searchIdResult.render(length, glycobaseID, ""));
        }else{
            String uoxf = sparqlEntityList.get(0).getValue("Uoxf");
            int length = sparqlEntityList.size();
            return ok(views.html.search.searchIdResult.render(length, glycobaseID, uoxf));
        }
    }

    @With(AccessLoggingAction.class)
    public Result getID(){
        DynamicForm dynamicForm = formFactory.form().bindFromRequest();
        //DynamicForm dynamicForm = Form.form().bindFromRequest();
        String glycobaseID = dynamicForm.get("glycobaseID");
        return redirect(routes.Application.searchID(glycobaseID));
    }

    @With(AccessLoggingAction.class)
    public Result searchProfile(String profile) throws SparqlException{

        if(session("notation") == null ) {
            session("notation", "cfg");
        }

        StructureByProfile structure = new StructureByProfile();
        SparqlEntity sparqlentity = new SparqlEntity();
        sparqlentity.setValue(GlycanGlycobase.ProfileId, profile);
        structure.setSparqlEntity(sparqlentity);
        List<SparqlEntity> sparqlEntityList = query(structure.getSparql());
        int length = sparqlEntityList.size();
        return ok(views.html.search.searchProfileResult.render(length, profile, sparqlEntityList));
    }

    @With(AccessLoggingAction.class)
    public Result getProfileID() throws SparqlException{
        DynamicForm dynamicForm = formFactory.form().bindFromRequest();
        //DynamicForm dynamicForm = Form.form().bindFromRequest();
        String profile = dynamicForm.get("profile");
        return redirect(routes.Application.searchProfile(profile));
    }

    @With(AccessLoggingAction.class)
    public Result searchUoxf(String name) throws SparqlException{

        if(session("notation") == null ) {
            session("notation", "cfg");
        }

        IdByUoxf id = new IdByUoxf();
        SparqlEntity sparqlentity = new SparqlEntity();
        sparqlentity.setValue(GlycanGlycobase.Uoxf, name);
        id.setSparqlEntity(sparqlentity);
        List<SparqlEntity> sparqlEntityList = query(id.getSparql());


        if(sparqlEntityList.size() == 0){
            int count = 0;
            return ok(views.html.search.searchUoxfResult.render(count, "", name));
        }else{
            int count = 1;
            String glycobaseId = sparqlEntityList.get(0).getValue("GlycoBaseId");
            return ok(views.html.search.searchUoxfResult.render(count, glycobaseId, name));
        }
    }

    @With(AccessLoggingAction.class)
    public Result getUoxf(){
        DynamicForm dynamicForm = formFactory.form().bindFromRequest();
        //DynamicForm dynamicForm = Form.form().bindFromRequest();
        String name = dynamicForm.get("uoxf");
        return redirect(routes.Application.searchUoxf(name));
    }

    @With(AccessLoggingAction.class)
    public Result listTaxon() throws SparqlException{
        ListTaxon taxon = new ListTaxon();
        SparqlEntity taxonSpqlEntity = new SparqlEntity();
        taxon.setSparqlEntity(taxonSpqlEntity);
        List<SparqlEntity> taxonList = query(taxon.getSparql());
        return ok(listTaxon.render(taxonList));
    }

    @With(AccessLoggingAction.class)
    public Result displayTaxon() throws SparqlException{
        if(session("notation") == null ) {
            session("notation", "cfg");
        }
        ListTaxon taxon = new ListTaxon();
        SparqlEntity taxonSpqlEntity = new SparqlEntity();
        taxon.setSparqlEntity(taxonSpqlEntity);
        List<SparqlEntity> taxonSparqlList = query(taxon.getSparql());
        List<String> taxonList = new ArrayList<>();
        for(SparqlEntity x:taxonSparqlList){
            DynamicForm dynamicForm = formFactory.form().bindFromRequest();
            String taxonName = dynamicForm.get(x.getValue("Taxon"));
            taxonList.add(taxonName);
        }
        taxonList.removeIf(Objects::isNull);
        List<Set<String>> structures = new ArrayList<>();
        for(String t:taxonList){
            Set<String> idFromTaxon=new HashSet<>();
            StructureByTaxon names = new StructureByTaxon();
            SparqlEntity sparqlentity = new SparqlEntity();
            sparqlentity.setValue(GlycanGlycobase.Taxon, t);
            names.setSparqlEntity(sparqlentity);
            List<SparqlEntity> structureList = query(names.getSparql());
            for(SparqlEntity s:structureList){
                idFromTaxon.add(s.getValue("GlycoBaseId"));
            }
            structures.add(idFromTaxon);
        }

        int i =0;
        Set<String> commonStructures = new HashSet<String>();
        for(Set s:structures){
            if(i==0){
                commonStructures = s;
            }else{
                commonStructures.retainAll(s);
            }
            i +=1;
        }
        System.out.println(commonStructures);
        boolean hpresult=false;
        boolean upresult=false;
        boolean existCommon=false;
        Map<String,String> result = new HashMap<>();
        if(commonStructures != null && !commonStructures.isEmpty()){
            existCommon = true;
            for(String s:commonStructures){
                UoxfByID name = new UoxfByID();
                SparqlEntity sparqlentity = new SparqlEntity();
                sparqlentity.setValue(GlycanGlycobase.GlycoBaseId, s);
                name.setSparqlEntity(sparqlentity);
                List<SparqlEntity> NameList = query(name.getSparql());
                String uoxf = NameList.get(0).getValue("Uoxf");
                String value = uoxf;
                for(String t:taxonList){
                    AvgUpByTaxon upgu = new AvgUpByTaxon();
                    SparqlEntity avgUpSpqEntity = new SparqlEntity();
                    avgUpSpqEntity.setValue(GlycanGlycobase.GlycoBaseId, s);
                    avgUpSpqEntity.setValue(GlycanGlycobase.Taxon, t);
                    upgu.setSparqlEntity(avgUpSpqEntity);
                    List<SparqlEntity> upSpqlEntityList = query(upgu.getSparql());
                    String up=upSpqlEntityList.get(0).getValue("avgUp");
                    if(up !=null && !up.isEmpty()){
                        upresult = true;
                    }else{
                        upresult = false;
                        up = "";
                    }
                    value += "\t"+up;
                    AvgHpByTaxon hpgu = new AvgHpByTaxon();
                    SparqlEntity avgHpSpqEntity = new SparqlEntity();
                    avgHpSpqEntity.setValue(GlycanGlycobase.GlycoBaseId, s);
                    avgHpSpqEntity.setValue(GlycanGlycobase.Taxon, t);
                    hpgu.setSparqlEntity(avgHpSpqEntity);
                    List<SparqlEntity> hpSpqlEntityList = query(hpgu.getSparql());
                    String hp=hpSpqlEntityList.get(0).getValue("avgHp");
                    if(hp !=null && !hp.isEmpty()){
                        hpresult = true;
                    }else{
                        hpresult = false;
                        hp = "";
                    }
                    value += "\t"+hp;
                }
                result.put(s,value);
            }
            System.out.println("has UPLC evidence: "+upresult);
            System.out.println("has HPLC evidence: "+hpresult);
        }

        return ok(compareTaxonResult.render(taxonList,result,hpresult,upresult,existCommon));
    }

    @With(AccessLoggingAction.class)
    public Result listGlycoprotein() throws SparqlException{
        ListProtein protein = new ListProtein();
        SparqlEntity sparqlentity = new SparqlEntity();
        protein.setSparqlEntity(sparqlentity);
        List<SparqlEntity> ProteinList = query(protein.getSparql());
        return ok(listGlycoprotein.render(ProteinList));
    }

    @With(AccessLoggingAction.class)
    public Result displayProtein() throws SparqlException{
        if(session("notation") == null ) {
            session("notation", "cfg");
        }
        ListProtein protein = new ListProtein();
        SparqlEntity sparqlentity = new SparqlEntity();
        protein.setSparqlEntity(sparqlentity);
        List<SparqlEntity> ProteinList = query(protein.getSparql());

        List<String> proteinList = new ArrayList<>();
        for(SparqlEntity x:ProteinList){
            DynamicForm dynamicForm = formFactory.form().bindFromRequest();
            String proteinName = dynamicForm.get(x.getValue("ProteinName"));
            proteinList.add(proteinName);
        }
        proteinList.removeIf(Objects::isNull);

        List<Map<String,String>> structures = new ArrayList<>();
        for(String p:proteinList){
            Map<String,String> idNameByProtein=new HashMap<>();
            StructureByProtein structure = new StructureByProtein();
            SparqlEntity proteinSparqlentity = new SparqlEntity();
            proteinSparqlentity.setValue(GlycanGlycobase.ProteinName, p);
            structure.setSparqlEntity(proteinSparqlentity);
            List<SparqlEntity> structureList = query(structure.getSparql());
            for(SparqlEntity s:structureList){
                idNameByProtein.put(s.getValue("GlycoBaseId"),s.getValue("Uoxf"));
            }
            structures.add(idNameByProtein);
        }

        int i =0;
        Set<String> commonStructures = new HashSet<String>();
        for(Map s:structures){
            if(i==0){
                commonStructures = s.keySet();
            }else{
                commonStructures.retainAll(s.keySet());
            }
            i +=1;
        }
        System.out.println(commonStructures);
        boolean existCommon=false;
        Map<String,String> result = new HashMap<>();
        if(commonStructures != null && !commonStructures.isEmpty()){
            existCommon = true;
            for(String s:commonStructures){
                result.put(s,structures.get(0).get(s));
            }
        }

        return ok(compareProteinResult.render(proteinList,result,existCommon));
    }

    @With(AccessLoggingAction.class)
    public Result listProfie4Type(String type) throws SparqlException{

        if(session("notation") == null ) {
            session("notation", "cfg");
        }

        ListProfile structure = new ListProfile();
        SparqlEntity sparqlentity = new SparqlEntity();
        sparqlentity.setValue(GlycanGlycobase.EvidenceType, type);
        structure.setSparqlEntity(sparqlentity);
        List<SparqlEntity> sparqlEntityList = query(structure.getSparql());
        int length = sparqlEntityList.size();
        return ok(profile4type.render(length, type, sparqlEntityList));
    }

    public static String avgHp(String glycobaseId) throws SparqlException{
        AvgHplcGu hpgu = new AvgHplcGu();
        SparqlEntity avgHpSpqEntity = new SparqlEntity();
        avgHpSpqEntity.setValue(GlycanGlycobase.GlycoBaseId, glycobaseId);
        hpgu.setSparqlEntity(avgHpSpqEntity);
        List<SparqlEntity> hpSpqlEntityList = query(hpgu.getSparql());
        return hpSpqlEntityList.get(0).getValue("avgHp");
    }

    public static String avg2ABUp(String glycobaseId) throws SparqlException{
        AvgUplcGu upgu = new AvgUplcGu();
        SparqlEntity avgUpSpqEntity = new SparqlEntity();
        avgUpSpqEntity.setValue(GlycanGlycobase.GlycoBaseId, glycobaseId);
        avgUpSpqEntity.setValue(GlycanGlycobase.LabelType, "2-AB");
        upgu.setSparqlEntity(avgUpSpqEntity);
        List<SparqlEntity> upSpqlEntityList = query(upgu.getSparql());
        return upSpqlEntityList.get(0).getValue("avgUp");
    }

    public static String avgProcUp(String glycobaseId) throws SparqlException{
        AvgUplcGu upgu = new AvgUplcGu();
        SparqlEntity avgUpSpqEntity = new SparqlEntity();
        avgUpSpqEntity.setValue(GlycanGlycobase.GlycoBaseId, glycobaseId);
        avgUpSpqEntity.setValue(GlycanGlycobase.LabelType, "Procainamide");
        upgu.setSparqlEntity(avgUpSpqEntity);
        List<SparqlEntity> upSpqlEntityList = query(upgu.getSparql());
        return upSpqlEntityList.get(0).getValue("avgUp");
    }

    public static String avgRp(String glycobaseId) throws SparqlException{
        AvgRpGu rpgu = new AvgRpGu();
        SparqlEntity avgRpSpqEntity = new SparqlEntity();
        avgRpSpqEntity.setValue(GlycanGlycobase.GlycoBaseId, glycobaseId);
        rpgu.setSparqlEntity(avgRpSpqEntity);
        List<SparqlEntity> rpSpqlEntityList = query(rpgu.getSparql());
        return rpSpqlEntityList.get(0).getValue("avgRp");
    }

    public static String avgCe(String glycobaseId) throws SparqlException{
        AvgCeGu cegu = new AvgCeGu();
        SparqlEntity avgCeSpqEntity = new SparqlEntity();
        avgCeSpqEntity.setValue(GlycanGlycobase.GlycoBaseId, glycobaseId);
        cegu.setSparqlEntity(avgCeSpqEntity);
        List<SparqlEntity> ceSpqlEntityList = query(cegu.getSparql());
        return ceSpqlEntityList.get(0).getValue("avgCe");
    }

    public static String avgPgc(String glycostoreId) throws SparqlException{
        AvgPgc avgPgc = new AvgPgc();
        SparqlEntity sparqlEntityPgc = new SparqlEntity();
        sparqlEntityPgc.setValue(GlycanGlycobase.GlycoStoreId, glycostoreId);
        avgPgc.setSparqlEntity(sparqlEntityPgc);
        List<SparqlEntity> pgcSpqlEntityList = query(avgPgc.getSparql());
        return pgcSpqlEntityList.get(0).getValue("avgPgc");
    }

    public static List<String> meanStdCeGu(String glycobaseId) throws SparqlException {
        List<String> valueList = new ArrayList<>();
        StdCeGu stdCeGu = new StdCeGu();
        SparqlEntity sparqlEntityCe = new SparqlEntity();
        sparqlEntityCe.setValue(GlycanGlycobase.GlycoBaseId, glycobaseId);
        stdCeGu.setSparqlEntity(sparqlEntityCe);
        List<SparqlEntity> resultCeGuStd  = query(stdCeGu.getSparql());
        valueList.add(0,resultCeGuStd.get(0).getValue("mean"));
        valueList.add(1,resultCeGuStd.get(0).getValue("sdev"));
        return valueList;
    }

    public static List<String> meanStdHplcGu(String glycobaseId) throws SparqlException {
        List<String> valueList = new ArrayList<>();
        StdHplcGu stdHplcGu = new StdHplcGu();
        SparqlEntity sparqlEntityHplc = new SparqlEntity();
        sparqlEntityHplc.setValue(GlycanGlycobase.GlycoBaseId, glycobaseId);
        stdHplcGu.setSparqlEntity(sparqlEntityHplc);
        List<SparqlEntity> resultHplcGuStd  = query(stdHplcGu.getSparql());
        valueList.add(0,resultHplcGuStd.get(0).getValue("mean"));
        valueList.add(1,resultHplcGuStd.get(0).getValue("sdev"));
        return valueList;
    }

    public static List<String> meanStdUplc2AB(String glycobaseId) throws SparqlException {
        List<String> valueList = new ArrayList<>();
        StdUplcGu2AB stdUplc2AB = new StdUplcGu2AB();
        SparqlEntity sparqlEntity = new SparqlEntity();
        sparqlEntity.setValue(GlycanGlycobase.GlycoBaseId, glycobaseId);
        stdUplc2AB.setSparqlEntity(sparqlEntity);
        List<SparqlEntity> resultUplc2ABStd  = query(stdUplc2AB.getSparql());
        valueList.add(0,resultUplc2ABStd.get(0).getValue("mean"));
        valueList.add(1,resultUplc2ABStd.get(0).getValue("sdev"));
        return valueList;
    }

    public static List<String> meanStdUplcProc(String glycobaseId) throws SparqlException {
        List<String> valueList = new ArrayList<>();
        StdUplcGuProcainamide stdUplcProc = new StdUplcGuProcainamide();
        SparqlEntity sparqlEntity = new SparqlEntity();
        sparqlEntity.setValue(GlycanGlycobase.GlycoBaseId, glycobaseId);
        stdUplcProc.setSparqlEntity(sparqlEntity);
        List<SparqlEntity> resultUplcProcStd  = query(stdUplcProc.getSparql());
        valueList.add(0,resultUplcProcStd.get(0).getValue("mean"));
        valueList.add(1,resultUplcProcStd.get(0).getValue("sdev"));
        return valueList;
    }

    public static List<String> meanStdPgc(String glycobaseId) throws SparqlException {
        List<String> valueList = new ArrayList<>();
        AvgStdPgcGlycoBaseId AvgStdPgcGlycoBaseId = new AvgStdPgcGlycoBaseId();
        SparqlEntity sparqlEntity = new SparqlEntity();
        sparqlEntity.setValue(GlycanGlycobase.GlycoBaseId, glycobaseId);
        AvgStdPgcGlycoBaseId.setSparqlEntity(sparqlEntity);
        List<SparqlEntity> resultUplcProcStd  = query(AvgStdPgcGlycoBaseId.getSparql());
        valueList.add(0,resultUplcProcStd.get(0).getValue("mean"));
        valueList.add(1,resultUplcProcStd.get(0).getValue("sdev"));
        return valueList;
    }

    @With(AccessLoggingAction.class)
    public Result showGlycan(String glycobaseId) throws SparqlException, Spectra.SpectraNotFoundException {

        if(session("notation") == null ) {
            session("notation", "cfg");
        }

        RecordByID record = new RecordByID();
        SparqlEntity spqlentityRec = new SparqlEntity();
        spqlentityRec.setValue(GlycanGlycobase.GlycoBaseId, glycobaseId);
        record.setSparqlEntity(spqlentityRec);
        List<SparqlEntity> RecordList = query(record.getSparql());
        int recordLen = RecordList.size();

        Set<String> uniqueReports = new HashSet<String>();
        for (SparqlEntity s : RecordList) {
            uniqueReports.add(s.getValue("ReportName"));
        }


        LiteratureByID literature = new LiteratureByID();
        SparqlEntity spqlentityLit = new SparqlEntity();
        spqlentityLit.setValue(GlycanGlycobase.GlycoBaseId, glycobaseId);
        literature.setSparqlEntity(spqlentityLit);
        List<SparqlEntity> LiteratureList = query(literature.getSparql());
        int literatureLen = LiteratureList.size();


        ReactionByID reaction = new ReactionByID();
        SparqlEntity spqlentityAct = new SparqlEntity();
        spqlentityAct.setValue(GlycanGlycobase.GlycoBaseId, glycobaseId);
        reaction.setSparqlEntity(spqlentityAct);
        List<SparqlEntity> ReactionList = query(reaction.getSparql());
        int reactionLen = ReactionList.size();

        ProfileByID profile = new ProfileByID();
        SparqlEntity spqlentityPrf = new SparqlEntity();
        spqlentityPrf.setValue(GlycanGlycobase.GlycoBaseId, glycobaseId);
        profile.setSparqlEntity(spqlentityPrf);
        List<SparqlEntity> ProfileList = query(profile.getSparql());
        int profileLen = ProfileList.size();

        UoxfByID name = new UoxfByID();
        SparqlEntity sparqlentity = new SparqlEntity();
        sparqlentity.setValue(GlycanGlycobase.GlycoBaseId, glycobaseId);
        name.setSparqlEntity(sparqlentity);
        List<SparqlEntity> NameList = query(name.getSparql());
        String uoxf = NameList.get(0).getValue("Uoxf");

        SequenceByID sequence = new SequenceByID();
        SparqlEntity spqlentityCT = new SparqlEntity();
        spqlentityCT.setValue(GlycanGlycobase.GlycoBaseId, glycobaseId);
        sequence.setSparqlEntity(spqlentityCT);
        List<SparqlEntity> glycoCTList = query(sequence.getSparql());
        String glycoCT = glycoCTList.get(0).getValue("Sequence");

        Spectra spectra = new Spectra();
        //int spectraId = -1;
        int spectraId = spectra.searchSpectraMap(uoxf);


        String jsonTest = "";
        if (spectraId > 0) {
            JSONParser parser = new JSONParser();
            Object obj = null;
            try {
                String jsonFile = "/tmp/out" + spectraId + ".json";
                obj = parser.parse(new FileReader(jsonFile));
                jsonTest = ((JSONObject) obj).toString();

            } catch (IOException e) {
                e.printStackTrace();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        StdCeGu stdCeGu = new StdCeGu();
        SparqlEntity sparqlEntity = new SparqlEntity();
        sparqlEntity.setValue(GlycanGlycobase.GlycoBaseId, glycobaseId);
        stdCeGu.setSparqlEntity(sparqlEntity);
        List<SparqlEntity> resultCeGuStd  = query(stdCeGu.getSparql());

        StdHplcGu stdHplcGu = new StdHplcGu();
        SparqlEntity sparqlEntityHplc = new SparqlEntity();
        sparqlEntityHplc.setValue(GlycanGlycobase.GlycoBaseId, glycobaseId);
        stdHplcGu.setSparqlEntity(sparqlEntityHplc);
        List<SparqlEntity> resultHplcGuStd  = query(stdHplcGu.getSparql());

        StdUplcGu stdUplcGu = new StdUplcGu();
        SparqlEntity sparqlEntityUplc = new SparqlEntity();
        sparqlEntityUplc.setValue(GlycanGlycobase.GlycoBaseId, glycobaseId);
        stdUplcGu.setSparqlEntity(sparqlEntityUplc);
        List<SparqlEntity> resultUplcGuStd  = query(stdUplcGu.getSparql());

        AvgStdPgcGlycoBaseId avgStdPgcGlycoBaseId = new AvgStdPgcGlycoBaseId();
        SparqlEntity sparqlEntityPgc = new SparqlEntity();
        sparqlEntityPgc.setValue(GlycanGlycobase.GlycoBaseId, glycobaseId);
        avgStdPgcGlycoBaseId.setSparqlEntity(sparqlEntityPgc);
        List<SparqlEntity> resultPgc = query(avgStdPgcGlycoBaseId.getSparql());

        StdUplcGu2AB stdUplcGu2AB = new StdUplcGu2AB();
        SparqlEntity sparqlEntity2AB = new SparqlEntity();
        sparqlEntity2AB.setValue(GlycanGlycobase.GlycoBaseId, glycobaseId);
        stdUplcGu2AB.setSparqlEntity(sparqlEntity2AB);
        List<SparqlEntity> result2AB  = query(stdUplcGu2AB.getSparql());

        StdUplcGuProcainamide stdUplcGuProcainamide = new StdUplcGuProcainamide();
        SparqlEntity sparqlEntityProc = new SparqlEntity();
        sparqlEntityProc.setValue(GlycanGlycobase.GlycoBaseId, glycobaseId);
        stdUplcGuProcainamide.setSparqlEntity(sparqlEntityProc);
        List<SparqlEntity> resultProc  = query(stdUplcGuProcainamide.getSparql());



        return ok(
                glycan.render(glycobaseId, uoxf, glycoCT,jsonTest, spectraId, recordLen, reactionLen, literatureLen, profileLen, RecordList, LiteratureList, ReactionList, ProfileList, uniqueReports, resultCeGuStd, resultHplcGuStd, resultUplcGuStd, resultPgc, result2AB, resultProc));
    }

    /*public Result listProteins()throws SparqlException{

        if(session("notation") == null ) {
            session("notation", "cfg");
        }

        ListProtein structure = new ListProtein();
        SparqlEntity sparqlentity = new SparqlEntity();
        structure.setSparqlEntity(sparqlentity);
        List<SparqlEntity> ProteinList = query(structure.getSparql());
        return ok(glycoprotein.render(ProteinList));
    }*/

    @With(AccessLoggingAction.class)
    public Result searchProtein(String proteinName) throws SparqlException, ExecutionException, InterruptedException {

        if(session("notation") == null ) {
            session("notation", "cfg");
        }


                /*
                * Problem we have glycan:has_protein_name in all
                * but pgc use glycan:is_from_source
                * so need to run two queries to get full structure ids from a protein
                 */

        StructureByProtein structure = new StructureByProtein();
        SparqlEntity sparqlentity = new SparqlEntity();
        sparqlentity.setValue(GlycanGlycobase.ProteinName, proteinName);
        structure.setSparqlEntity(sparqlentity);

        //List<SparqlEntity> sparqlEntityListPGC = query(structure.getSparql().replaceAll("glycan:has_protein_name ", "glycan:is_from_source "));

        List<SparqlEntity> sparqlEntityList = query(structure.getSparql());
        System.out.println("---> " + structure.getSparql());

        //List<SparqlEntity> newList = Stream.concat(sparqlEntityListPGC.stream(), sparqlEntityList.stream())
         //       .collect(Collectors.toList());


        int length = sparqlEntityList.size();
        Map<String, String>  avgMap = AvgTest.testAvg(sparqlEntityList);
        //Map<String, String>  avgMap = AvgTest.testAvg(sparqlEntityListPGC);
        //Map<String, String>  avgMap = null;

        return ok(views.html.search.searchProteinResult.render(length,proteinName, avgMap));
    }

    @With(AccessLoggingAction.class)
    public Result getProtein(){
        DynamicForm dynamicForm = formFactory.form().bindFromRequest();
        //DynamicForm dynamicForm = Form.form().bindFromRequest();
        String proteinName = dynamicForm.get("ProteinName");
        return redirect(routes.Application.searchProtein(proteinName));
    }

    @With(AccessLoggingAction.class)
    public static String monoMass(String glycobaseID)throws SparqlException{
        MassByID mass = new MassByID();
        SparqlEntity massSpqEntity = new SparqlEntity();
        massSpqEntity.setValue(GlycanGlycobase.GlycoBaseId, glycobaseID);
        mass.setSparqlEntity(massSpqEntity);
        List<SparqlEntity> massSpqlEntityList = query(mass.getSparql());
        return massSpqlEntityList.get(0).getValue("MonoMass");
    }

    @With(AccessLoggingAction.class)
    public Result listReferences() throws SparqlException{
        if(session("notation") == null ) {
            session("notation", "cfg");
        }
        int length = 0;
        ListLiterature structure = new ListLiterature();
        SparqlEntity sparqlentity = new SparqlEntity();
        structure.setSparqlEntity(sparqlentity);
        List<SparqlEntity> sparqlEntityList = query(structure.getSparql());
        length = sparqlEntityList.size();

        return ok(references.render(length, sparqlEntityList));
    }

    @With(AccessLoggingAction.class)
    public Result searchReference(String title) throws SparqlException{
        if(session("notation") == null ) {
            session("notation", "cfg");
        }
        //DynamicForm dynamicForm = Form.form().bindFromRequest();
        //String title = dynamicForm.get("title");
        StructureByReference structure = new StructureByReference();
        SparqlEntity sparqlentity = new SparqlEntity();
        sparqlentity.setValue(GlycanGlycobase.PaperTitle, title);
        structure.setSparqlEntity(sparqlentity);
        List<SparqlEntity> sparqlEntityList = query(structure.getSparql());
        int length = sparqlEntityList.size();

        LiteratureDetail paper = new LiteratureDetail();
        SparqlEntity paperSpqEntity = new SparqlEntity();
        paperSpqEntity.setValue(GlycanGlycobase.PaperTitle, title);
        paper.setSparqlEntity(paperSpqEntity);
        List<SparqlEntity> paperSpqList = query(paper.getSparql());

        PaperLinkedSource source = new PaperLinkedSource();
        SparqlEntity sourceSpqEntity = new SparqlEntity();
        sourceSpqEntity.setValue(GlycanGlycobase.PaperTitle, title);
        source.setSparqlEntity(sourceSpqEntity);
        List<SparqlEntity> sourceSpqList = query(source.getSparql());

        return ok(views.html.search.searchPaperResult.render(length,title, sparqlEntityList, paperSpqList, sourceSpqList));
    }

    public Result about(){

        return ok(about.render());
    }

    public Result contribute(){
        return ok(contribute.render());
    }

    public Result format(String notation) {
        session("notation", notation);
        String refererUrl = request().getHeader("referer");
        return redirect(refererUrl);
    }


    //TODO  mkdir for uploads on server and modify the rename line
    public Result upload() {

        DynamicForm dynamicForm = formFactory.form().bindFromRequest();

        String name = dynamicForm.get("name");
        String email = dynamicForm.get("email");
        String captcha = dynamicForm.get("g-recaptcha-response");

        //String gRecaptchaResponse = request().get  getParameter("g-recaptcha-response");
        boolean success = RecaptchaService.verify(captcha);

        Http.MultipartFormData<File> body = request().body().asMultipartFormData();
        Http.MultipartFormData.FilePart<File> uploadedFile = body.getFile("uploaded");
        if(success == true) {
            if(name.isEmpty()|| email.isEmpty() ){
                return badRequest("Invalid request, missing name and/or email");
            }
            if (uploadedFile != null) {
                if (!"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet".equalsIgnoreCase(uploadedFile.getContentType())) {
                    return badRequest("Invalid request, only excel sheets are allowed.");
                } else {
                    String fileName = uploadedFile.getFilename();
                    String contentType = uploadedFile.getContentType();
                    File file = uploadedFile.getFile();
                    File file2 = uploadedFile.getFile();;
                    String nameChange = fileName + "_" + name + "_" + email;
                    file.renameTo(new File("/tmp", nameChange));
                    file2.renameTo(new File("/tmp", nameChange ));

                    return ok(views.html.thankyou.render(fileName));
                }
            } else {
                flash("error", "Missing file");
                return badRequest("Invalid request, no file has been sent.");
            }
        } else {
            flash("error", "Missing reCAPTCHA response");
            return badRequest("Invalid request, no reCAPTCHA response.");
        }




    }

    /*
    This takes values for quick search in nav bar
    note use of lower case on exp. type
     */
    public Result searchFilter() {
        Double searchValue = 0.0;
        DynamicForm dynamicForm = formFactory.form().bindFromRequest();
        //DynamicForm requestData = Form.form().bindFromRequest();
        String expType = dynamicForm.get("type");
        String value = dynamicForm.get("search");

        if (expType.contains("Name")) {
            return redirect(routes.Application.searchUoxf(value));
        } else {
            Double minValue = Double.valueOf(value) - 0.5;
            Double maxValue = Double.valueOf(value) + 0.5;

            return redirect(routes.Application.searchGu(expType.toLowerCase(), String.valueOf(minValue), String.valueOf(value), 0, 0 ,0 ,0 ,0));




        }
    }


    @With(AccessLoggingAction.class)
    public Result startShowPgcData() {
        if(session("notation") == null ) {
            session("notation", "cfg");
        }

        String nameUoxf = "";
        String secondNameUoxf = "";
        String min = "";
        String max = "";
        String massLow = "";
        String massHigh = "";

        return ok(views.html.d3.pgcVisualTime.render(nameUoxf, secondNameUoxf, min, max, massLow, massHigh));
    }


    @With(AccessLoggingAction.class)
    public Result showPgcDataTemp() {
        if(session("notation") == null ) {
            session("notation", "cfg");
        }

        String nameUoxf = "";
        String secondNameUoxf = "";
        String min = "";
        String max = "";
        String massLow = "";
        String massHigh = "";


        DynamicForm dynamicForm = formFactory.form().bindFromRequest();

             nameUoxf = dynamicForm.get("nameUoxf");
             secondNameUoxf = dynamicForm.get("secondNameUoxf");
             min = dynamicForm.get("Minimum");
             max = dynamicForm.get("Maximum");
             massLow = dynamicForm.get("MassLow");
             massHigh = dynamicForm.get("MassHigh");

        return ok(views.html.d3.pgcVisualTime.render(nameUoxf, secondNameUoxf, min, max, massLow, massHigh));
    }

    @With(AccessLoggingAction.class)
    public static Map<String, String> filterStructuresMotif(DynamicForm dynamicForm, Map<String, String> avgMap){

        int core = 0;
        int bisect = 0;
        int outer = 0;
        int hybrid = 0;
        int mannose = 0;

        Map<String, String> formMap = dynamicForm.data();

        if(formMap.containsKey("core")) {
            core = Integer.parseInt(formMap.get("core"));
        }
        if(formMap.containsKey("bisect")) {
            bisect = Integer.parseInt(formMap.get("bisect"));
        }
        if(formMap.containsKey("outer")) {
            core = Integer.parseInt(formMap.get("outer"));
        }
        if(formMap.containsKey("hybrid")) {
            core = Integer.parseInt(formMap.get("hybrid"));
        }
        if(formMap.containsKey("mannose")) {
            core = Integer.parseInt(formMap.get("mannose"));
        }

        if (core == 1) {
            Predicate<String> containsF6 = new Predicate<String>() {
                @Override
                public boolean apply(String input) {
                    return input.contains("F(6");
                }
            };
            avgMap = Maps.filterValues(avgMap, containsF6);
        }
        if (bisect == 1) {
            Predicate<String> containsB = new Predicate<String>() {
                @Override
                public boolean apply(String input) {
                    return input.contains("B");
                }
            };
            avgMap = Maps.filterValues(avgMap, containsB);
        }
        if (outer == 1) {
            Predicate<String> containsF3 = new Predicate<String>() {
                @Override
                public boolean apply(String input) {
                    return input.contains("F(3");
                }
            };
            avgMap = Maps.filterValues(avgMap, containsF3);
        }

        if (mannose == 1) {
            Predicate<String> containsM = new Predicate<String>() {
                @Override
                public boolean apply(String input) {
                    return input.contains("M");
                }
            };
            avgMap = Maps.filterValues(avgMap, containsM);

            Predicate<String> containsA = new Predicate<String>() {
                @Override
                public boolean apply(String input) {
                    return !input.contains("A");
                }
            };

            avgMap = Maps.filterValues(avgMap, containsA);

        }

        if (hybrid == 1) {
            Predicate<String> containsM = new Predicate<String>() {
                @Override
                public boolean apply(String input) {
                    return input.contains("M");
                }
            };
            avgMap = Maps.filterValues(avgMap, containsM);

            Predicate<String> containsA = new Predicate<String>() {
                @Override
                public boolean apply(String input) {
                    return input.contains("A");
                }
            };

            avgMap = Maps.filterValues(avgMap, containsA);
        }

        if (hybrid == 1 && mannose == 1){
            Predicate<String> containsM = new Predicate<String>() {
                @Override
                public boolean apply(String input) {
                    return input.contains("M");
                }
            };
            avgMap = Maps.filterValues(avgMap, containsM);

            Predicate<String> containsA = new Predicate<String>() {
                @Override
                public boolean apply(String input) {
                    return input.contains("A");
                }
            };

            avgMap = Maps.filterValues(avgMap, containsA);

        }

    return avgMap;
    }


    public String checkOlinkResult(List<SparqlEntity> sparqlEntity) {
        String olink = null;
        for (SparqlEntity entity : sparqlEntity) {
            String name = entity.getData().get("Uoxf").toString();
            if (name.contains("-")) {
                olink = "yes";
                break;
            }
        }
        return olink;
    }



}
