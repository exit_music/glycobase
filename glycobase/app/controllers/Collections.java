package controllers;

import com.google.common.collect.Ordering;
import com.google.inject.Inject;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import play.data.DynamicForm;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;
import sparql.SparqlEntity;
import sparql.SparqlException;
import sparql.glycobase.GlycanGlycobase;
import sparql.glycobase.ListReport;
import sparql.glycobase.OlinkStructureCollectionFeature;
import sparql.glycostore.Comments;
import views.html.*;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

/**
 * Created by s2980369 on 27/7/17.
 */
public class Collections extends Controller {

    @Inject
    private FormFactory formFactory;

    /**
     *
     * simple Result listing all collections
     * for example UPLC BTI or CE
     */
    @With(AccessLoggingAction.class)
    public Result listAllCollections() {
        return ok(views.html.collections.summaryCollections.render());
    }


    /*
    This gets the most relevant refs for a collection
    Manually decided and needs updates to the json file
     */
    public Result getCollectionsRef(){

        String collectionName = "NIBRT_Public_Collection";
        ArrayList<String> displayRef = new ArrayList<String>();

        JSONParser parser = new JSONParser();
        try
        {
            Object object = parser
                    .parse(new FileReader("/tmp/collections.json"));

            //convert Object to JSONObject
            JSONObject jsonObject = (JSONObject)object;

            //Reading the String

            //Reading the array
            JSONArray collection = (JSONArray)jsonObject.get(collectionName);

            for(Object a : collection) {
                JSONObject jsonLineItem = (JSONObject) a;
               // JSONObject O = (JSONObject) collection.get(0);
                System.out.println("\t"+ jsonLineItem.get("lead") );
                String display = jsonLineItem.get("lead") + ", " + jsonLineItem.get("title") + ", " + jsonLineItem.get("year");
                displayRef.add(display);
            }

            //Printing all the values


        }
        catch(FileNotFoundException fe)
        {
            fe.printStackTrace();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return ok(index.render());
    }


    //this needs refinement
    ////here
    @With(AccessLoggingAction.class)
    public Result searchMotifOlinkedCollection(String reportName, Integer core1, Integer core2, Integer bloodgrouph, Integer linearbloodgroupb, Integer bloodgroupi, Integer bloodgrouplewisa, Integer bloodgrouplewisx, Integer bloodgroupslewisx ) throws SparqlException, IOException {

        if(session("notation") == null ) {
            session("notation", "cfg");
        }

        OlinkStructureCollectionFeature olinkStructureCollectionFeature = new OlinkStructureCollectionFeature();
        SparqlEntity sparqlEntity = new SparqlEntity();
        sparqlEntity.setValue(GlycanGlycobase.ReportName, reportName.trim() );

        if(core1 == 1) {
            sparqlEntity.setValue(GlycanGlycobase.Core1, GlycanGlycobase.Core1);
        }

        if(core2 == 1) {
            sparqlEntity.setValue(GlycanGlycobase.Core2, GlycanGlycobase.Core2);
        }

        if(bloodgrouph == 1) {
            sparqlEntity.setValue(GlycanGlycobase.BloodGroupH, GlycanGlycobase.BloodGroupH);
        }

        if(linearbloodgroupb == 1) {
            sparqlEntity.setValue(GlycanGlycobase.LinearBloodGroupB, GlycanGlycobase.LinearBloodGroupB);
        }

        if(bloodgroupi == 1) {
            sparqlEntity.setValue(GlycanGlycobase.BloodGroupi, GlycanGlycobase.BloodGroupi);
        }


        if(bloodgrouplewisa == 1) {
            sparqlEntity.setValue(GlycanGlycobase.BloodGroupLewisA, GlycanGlycobase.BloodGroupLewisA);
        }

        if(bloodgrouplewisx == 1) {
            sparqlEntity.setValue(GlycanGlycobase.BloodGroupLewisX, GlycanGlycobase.BloodGroupLewisX);
        }


        if(bloodgroupslewisx == 1) {
            sparqlEntity.setValue(GlycanGlycobase.BloodGroupSialylLewisX, GlycanGlycobase.BloodGroupSialylLewisX);
        }

        olinkStructureCollectionFeature.setSparqlEntity(sparqlEntity);

        List<SparqlEntity> sparqlEntityList = Application.query(olinkStructureCollectionFeature.getSparql());

        int length = sparqlEntityList.size();
        Set<String> matchOlinks = new HashSet<>();

        for(SparqlEntity s : sparqlEntityList){
            if(s.getValue("SaccharideURI") == null){
                return ok(views.html.errors.noResults.render());
            } else {
                matchOlinks.add(s.getValue("GlycoBaseId"));
            }
        }

        //bad reuuse of buildcollections

        String reportNameJson = reportName.replaceAll(" ", "_").replaceAll("\\(", "").replaceAll("\\)", "");

        //System.out.println(reportNameJson);

        String contents = new String(Files.readAllBytes(Paths.get("/tmp/" + reportNameJson + ".json")));
        String[] i = contents.split("\\\",");

        Map<String, String> m = new HashMap<String, String>();

        for(String ii : i){
            String [] x = ii.split("\\\":");
            //System.out.println("i " + x[0] );

            if (!x[0].contains("\\}") && !x[0].contains("}\"")) {

                x[0] = x[0].replaceAll("\\\\", "").replaceAll("\"", "").replace("{","");
                x[1] = x[1].replaceAll("\"", "").replaceAll("\\\\$", "").replaceAll("^\\\\", "").replaceAll("\\\\}$", "");
                x[1] = x[1].replaceAll("\\\\t", "\t").replaceAll("\\\\", "");
                m.put(x[0], x[1]);

                System.out.println("i " + x[0] );
            }
        }

        DynamicForm dynamicForm = formFactory.form().bindFromRequest();

        //retain sparql matched structures containing selected features from the collections map
        m.keySet().retainAll(matchOlinks);

        int count = m.size();

        ListReport allReports = new ListReport();
        SparqlEntity repSpqlEntity = new SparqlEntity();
        allReports.setSparqlEntity(repSpqlEntity);
        List<SparqlEntity> allReportList = Application.query(allReports.getSparql());

        Set<String> uniqueReports = new HashSet<String>();
        for (SparqlEntity s : allReportList) {
            uniqueReports.add(s.getValue("ReportName"));
        }

        List<String> uniqueReportsList = new ArrayList<String>(uniqueReports);
        List<String> uniqueReportsListOrdered = Ordering.from(String.CASE_INSENSITIVE_ORDER).sortedCopy(uniqueReportsList);

        ArrayList<String> displayTopRefs = BuildCollectionsJson.displayTopRefs(reportNameJson);

        //get comments if any on data report
        Comments comments = new Comments();
        SparqlEntity sparqlentity = new SparqlEntity();
        String reportNameModified = reportName;
        sparqlentity.setValue(GlycanGlycobase.ReportName, reportName);
        comments.setSparqlEntity(sparqlentity);
        List<SparqlEntity> reportComment = Application.query(comments.getSparql());

        //return ok("yes");
        return ok(views.html.search.testSearchOlinkCollection.render(count, reportName, m, uniqueReportsListOrdered, displayTopRefs, reportComment));
        //return ok(views.html.search.testSearchOlinkCollection.render(reportName, length, sparqlEntityList, core1, core2, bloodgrouph, linearbloodgroupb, bloodgroupi, bloodgrouplewisa, bloodgrouplewisx, bloodgroupslewisx));

    }
}
