package controllers;

import play.mvc.Controller;
import sparql.SparqlEntity;
import sparql.SparqlException;
import sparql.glycobase.*;
import sparql.glycostore.*;

import java.util.*;
import java.util.concurrent.*;

import static controllers.Application.query;
import static java.util.concurrent.Executors.callable;


/**
 * Created by matthew on 18/12/15.
 */
public class AvgTest extends Controller {

    /*
    Likely to replace this with one sparql query
     */
    public static List<SparqlEntity> avgProtein(String protein) throws SparqlException, InterruptedException, ExecutionException {

        StructureProteinAllLc structureProteinAllLc = new StructureProteinAllLc();
        SparqlEntity sparqlEntity = new SparqlEntity();
        sparqlEntity.setValue(GlycanGlycobase.ProteinName, protein);
        structureProteinAllLc.setSparqlEntity(sparqlEntity);
        List<SparqlEntity> sparqlEntityList = query(structureProteinAllLc.getSparql());

        return sparqlEntityList;

    }

    public static Map<String, String> testAvg(List<SparqlEntity> sparqlEntityList) throws SparqlException, InterruptedException, ExecutionException {

        ExecutorService executor = new ThreadPoolExecutor(8, 16,
                5000, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<Runnable>());

        Map<String, String> map = new HashMap<String, String>();

        for(SparqlEntity entry:sparqlEntityList) {
            String id = entry.getValue("GlycoBaseId");
            String resultFuture = "";

            //final ExecutorService executor = Executors.newCachedThreadPool();
            final Future<String> futureResult1 = executor.submit (new averageCe(id));
            final Future<String> futureResult2 = executor.submit(new averageHp(id));
            final Future<String> futureResult3 = executor.submit(new averageRp(id));
            final Future<String> futureResult4 = executor.submit(new average2ABUp(id));
            final Future<String> futureResult5 = executor.submit(new getSequenceName(id));
            final Future<String> futureResult7 = executor.submit(new getMonoMass(id));
            final Future<String> futureResult8 = executor.submit(new averageProcUp(id));


            final Future<String> futureResult6 = executor.submit(new averagePgc(id));
            final String result6 = futureResult6.get();

            final String result1 = futureResult1.get();
            final String result2 = futureResult2.get();
            final String result3 = futureResult3.get();
            final String result4 = futureResult4.get();
            final String result5 = futureResult5.get();
            final String result7 = futureResult7.get();
            final String result8 = futureResult8.get();


            //String results = "0" + "\t" + "0" +  "\t" + "0" + "\t" + "0" + "\t" + "0" + "\t"
            //        + "0" + "\t" + "0" + "\t" + result6;

            String results = result1 + "\t" + result5 +  "\t" + result2 + "\t" + result3 + "\t" + result4 + "\t"
                   + result7 + "\t" + result8 + "\t" + result6;
            map.put(id, results);

        }


        executor.shutdown();
        return map;

    }

    public static class averageHp implements Callable {

        public String glycanSequenceId;

        public averageHp(String glycanSequenceId) {
            this.glycanSequenceId = glycanSequenceId;
        }

        @Override
        public String call() throws Exception {
            AvgHplcGu hpgu = new AvgHplcGu();
            SparqlEntity avgHpSpqEntity = new SparqlEntity();
            avgHpSpqEntity.setValue(GlycanGlycobase.GlycoBaseId, glycanSequenceId);
            hpgu.setSparqlEntity(avgHpSpqEntity);
            List<SparqlEntity> hpSpqlEntityList = query(hpgu.getSparql());
            System.out.println("Query for HPLC " + hpgu.getSparql());
            return hpSpqlEntityList.get(0).getValue("avgHp");
        }
    }

    public static class averageCe implements Callable {

        public String glycanSequenceId;

        public averageCe(String glycanSequenceId) {
            this.glycanSequenceId = glycanSequenceId;
        }

        @Override
        public String call() throws Exception {
            AvgCeGu cegu = new AvgCeGu();
            SparqlEntity avgHpSpqEntity = new SparqlEntity();
            avgHpSpqEntity.setValue(GlycanGlycobase.GlycoBaseId, glycanSequenceId);
            cegu.setSparqlEntity(avgHpSpqEntity);
            List<SparqlEntity> hpSpqlEntityList = query(cegu.getSparql());

            return hpSpqlEntityList.get(0).getValue("avgCe");
        }
    }

    public static class averageRp implements Callable {

        public String glycanSequenceId;

        public averageRp(String glycanSequenceId) {
            this.glycanSequenceId = glycanSequenceId;
        }

        @Override
        public String call() throws Exception {
            AvgRpGu rpgu = new AvgRpGu();
            SparqlEntity avgHpSpqEntity = new SparqlEntity();
            avgHpSpqEntity.setValue(GlycanGlycobase.GlycoBaseId, glycanSequenceId);
            rpgu.setSparqlEntity(avgHpSpqEntity);
            List<SparqlEntity> hpSpqlEntityList = query(rpgu.getSparql());

            //System.out.println("hello " + hpSpqlEntityList.get(0).getValue("avgCe") );
            return hpSpqlEntityList.get(0).getValue("avgRp");
        }
    }


    public static class averagePgc implements Callable {

        public String glycanSequenceId;

        public averagePgc(String glycanSequenceId) {
            this.glycanSequenceId = glycanSequenceId;
        }

        @Override
        public String call() throws Exception {
            AvgPgc avgPgc = new AvgPgc();
            SparqlEntity avgPgcSpqEntity = new SparqlEntity();
            avgPgcSpqEntity.setValue(GlycanGlycobase.GlycoStoreId, glycanSequenceId);  //was set to BaseId
            avgPgc.setSparqlEntity(avgPgcSpqEntity);
            List<SparqlEntity> pgcSpqlEntityList = query(avgPgc.getSparql());

            //System.out.println("hello " + hpSpqlEntityList.get(0).getValue("avgCe") );
            //System.out.println("Query for PGC " + avgPgc.getSparql());
            return pgcSpqlEntityList.get(0).getValue("avgPgc");
        }
    }

    public static class average2ABUp implements Callable {

        public String glycanSequenceId;

        public average2ABUp(String glycanSequenceId) {
            this.glycanSequenceId = glycanSequenceId;
        }

        @Override
        public String call() throws Exception {
            AvgUplcGu upgu = new AvgUplcGu();
            SparqlEntity avgHpSpqEntity = new SparqlEntity();
            avgHpSpqEntity.setValue(GlycanGlycobase.GlycoBaseId, glycanSequenceId);
            avgHpSpqEntity.setValue(GlycanGlycobase.LabelType, "2-AB");
            upgu.setSparqlEntity(avgHpSpqEntity);
            List<SparqlEntity> hpSpqlEntityList = query(upgu.getSparql());

            //System.out.println("Query for HPLC " + upgu.getSparql());
            return hpSpqlEntityList.get(0).getValue("avgUp");
        }
    }

    public static class averageProcUp implements Callable {

        public String glycanSequenceId;

        public averageProcUp(String glycanSequenceId) {
            this.glycanSequenceId = glycanSequenceId;
        }

        @Override
        public String call() throws Exception {
            AvgUplcGu upgu = new AvgUplcGu();
            SparqlEntity avgHpSpqEntity = new SparqlEntity();
            avgHpSpqEntity.setValue(GlycanGlycobase.GlycoBaseId, glycanSequenceId);
            avgHpSpqEntity.setValue(GlycanGlycobase.LabelType, "Procainamide");
            upgu.setSparqlEntity(avgHpSpqEntity);
            List<SparqlEntity> hpSpqlEntityList = query(upgu.getSparql());

            //System.out.println("hello " + hpSpqlEntityList.get(0).getValue("avgCe") );
            return hpSpqlEntityList.get(0).getValue("avgUp");
        }
    }

    public static  class getSequenceName implements Callable {

        public String glycanSequenceId;

        public getSequenceName(String glycanSequenceId) {
            this.glycanSequenceId = glycanSequenceId;
        }

        @Override
        public String call() throws Exception {
            UoxfByID uoxf = new UoxfByID();
            SparqlEntity sparqlEntityId = new SparqlEntity();
            sparqlEntityId.setValue(GlycanGlycobase.GlycoBaseId,glycanSequenceId);
            uoxf.setSparqlEntity(sparqlEntityId);
            List<SparqlEntity> idspqlEntityList = query(uoxf.getSparql());
            return idspqlEntityList.get(0).getValue("Uoxf");
        }
    }

    public static class getMonoMass implements Callable {

        public String glycanSequenceId;

        public getMonoMass(String glycanSequenceId) {
            this.glycanSequenceId = glycanSequenceId;
        }

        @Override
        public String call() throws Exception {
            MassByID mass = new MassByID();
            SparqlEntity massSpqEntity = new SparqlEntity();
            massSpqEntity.setValue(GlycanGlycobase.GlycoBaseId, glycanSequenceId);
            mass.setSparqlEntity(massSpqEntity);
            List<SparqlEntity> massSpqlEntityList = query(mass.getSparql());

            return massSpqlEntityList.get(0).getValue("MonoMass");
        }
    }

}