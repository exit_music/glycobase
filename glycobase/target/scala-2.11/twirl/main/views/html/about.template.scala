
package views.html

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._

object about extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template0[play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply():play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](_display_(/*1.2*/main/*1.6*/ {_display_(Seq[Any](format.raw/*1.8*/("""

    """),format.raw/*3.5*/("""<div class="section">
        <div class="container" >
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header text-primary">
                        <h2 class="subheader">The Team</h2>
                    </div>
                    <p>Matthew Campbell, Jodie Abrahams and Nicolle Packer (Institute for Glycomics, Griffith University, Australia)</p>
                    <p>Sophie Zhao, Ian Walsh, Terry Nguyen-Khuong and Pauline Rudd (BTI A*STAR, Singapore)</p>
                    <p>Louise Royle and Daniel Spencer (Ludger Ltd, UK)</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header text-primary">
                        <h2 class="subheader">Cite</h2>
                    </div>

                    <p>If you cite or display any content, or reference this database (in any format), you must include a reference to:</p>
                    <p><ul>Campbell MP et al., <a href="https://academic.oup.com/bioinformatics/article-lookup/doi/10.1093/bioinformatics/btn090">
                    GlycoBase and autoGU: tools for HPLC-based glycan analysis)</a>, Bioinformatics, 2008</ul>
                    <ul>Abrahams, JL et., Building a PGC-LC-MS N-glycan retention library and elution mapping resource, Glycoconj. J., (in press)  DOI: 10.1007/s10719-017-9793-4</ul></p>
                    <p>Please include a reference to our website: www.glycostore.org</p>

                    <div class="page-header text-primary">
                        <h2 class="subheader">Integrating Data</h2>
                    </div>
                    <p>When integrating our data into a website or tool (software application, web service or similar), please cite the source in an accurate and easily discoverable manner. The above citation is required at all times.</p>

                </div>
            </div>
        </div>
    </div>
    <div class="section">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <img src=""""),_display_(/*40.32*/routes/*40.38*/.Assets.versioned("images/collaborators/glycomics.jpg")),format.raw/*40.93*/("""" class="center-block img-responsive">
                </div>
                <div class="col-md-4">
                    <img src=""""),_display_(/*43.32*/routes/*43.38*/.Assets.versioned("images/collaborators/bti.png")),format.raw/*43.87*/("""" class="center-block img-responsive">
                </div>
                <div class="col-md-4">
                    <img src=""""),_display_(/*46.32*/routes/*46.38*/.Assets.versioned("images/collaborators/mq.png")),format.raw/*46.86*/("""" class="center-block img-responsive">
                </div>
            </div>
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-3">
                    <img src=""""),_display_(/*52.32*/routes/*52.38*/.Assets.versioned("images/ludger_logo.jpg")),format.raw/*52.81*/("""" class="center-block img-responsive">
                </div>
                <div class="col-md-3">
                    <img src=""""),_display_(/*55.32*/routes/*55.38*/.Assets.versioned("images/collaborators/NIBRT.png")),format.raw/*55.89*/("""" class="center-block img-responsive">
                </div>
                <div class="col-md-3"></div>
            </div>
        </div>
    </div>
""")))}),format.raw/*61.2*/("""

"""))
      }
    }
  }

  def render(): play.twirl.api.HtmlFormat.Appendable = apply()

  def f:(() => play.twirl.api.HtmlFormat.Appendable) = () => apply()

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Mon Feb 26 14:39:19 AEST 2018
                  SOURCE: /Users/s2980369/IdeaProjects/glycobase/glycobase/app/views/about.scala.html
                  HASH: 0e326b74974ba271574fcbefc5bf97555a079829
                  MATRIX: 1030->1|1041->5|1079->7|1111->13|3284->2159|3299->2165|3375->2220|3534->2352|3549->2358|3619->2407|3778->2539|3793->2545|3862->2593|4115->2819|4130->2825|4194->2868|4353->3000|4368->3006|4440->3057|4623->3210
                  LINES: 33->1|33->1|33->1|35->3|72->40|72->40|72->40|75->43|75->43|75->43|78->46|78->46|78->46|84->52|84->52|84->52|87->55|87->55|87->55|93->61
                  -- GENERATED --
              */
          