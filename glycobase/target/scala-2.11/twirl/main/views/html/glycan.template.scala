
package views.html

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._
/*1.2*/import sparql._
/*3.2*/import play.api.Play

object glycan extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template19[String,String,String,Int,Int,Int,Int,Int,List[SparqlEntity],List[SparqlEntity],List[SparqlEntity],List[SparqlEntity],Set[String],List[SparqlEntity],List[SparqlEntity],List[SparqlEntity],List[SparqlEntity],List[SparqlEntity],List[SparqlEntity],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*4.2*/(glycobaseid: String, uoxf:String, json:String, spectraId:Int, recordLen:Int, reactionLen:Int, literatureLen:Int, profileLen:Int, RecordList:List[SparqlEntity], LiteratureList:List[SparqlEntity], ReactionList:List[SparqlEntity], ProfileList:List[SparqlEntity], UniqueReports:Set[String], resultCeGuStd:List[SparqlEntity], resultHplcGuStd:List[SparqlEntity], resultUplcGuStd:List[SparqlEntity], resultPgc:List[SparqlEntity], result2AB:List[SparqlEntity], resultProc:List[SparqlEntity]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*4.486*/("""





"""),_display_(/*10.2*/main/*10.6*/ {_display_(Seq[Any](format.raw/*10.8*/("""

    """),_display_(/*12.6*/if(spectraId > 0)/*12.23*/{_display_(Seq[Any](format.raw/*12.24*/("""
        """),format.raw/*13.9*/("""<script src=""""),_display_(/*13.23*/routes/*13.29*/.Assets.versioned("javascripts/st.js")),format.raw/*13.67*/(""""></script>
        <script src=""""),_display_(/*14.23*/routes/*14.29*/.Assets.versioned("javascripts/d3.js")),format.raw/*14.67*/(""""></script>
        <link rel="stylesheet" type="text/css" media="screen" href=""""),_display_(/*15.70*/routes/*15.76*/.Assets.versioned("stylesheets/st.css")),format.raw/*15.115*/("""">
    """)))}),format.raw/*16.6*/("""

    """),format.raw/*25.13*/("""

    """),format.raw/*27.5*/("""<div class="section">
        <div class="container" >
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header">
                        <div class="pull-left">
                            <h1 class="info-header-grey text-primary">Structure Information</h1>
                            <div class="minitext">"""),_display_(/*34.52*/uoxf),format.raw/*34.56*/("""</div>
                        </div>
                        <div class="pull-right">
                            <div class="text-right brand"></div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-9">
                    """),_display_(/*45.22*/if(session.get("notation"))/*45.49*/{_display_(Seq[Any](format.raw/*45.50*/("""
                        """),format.raw/*46.25*/("""<img class="img-responsive glycan-img" src=""""),_display_(/*46.70*/routes/*46.76*/.Assets.versioned("images/lg-"+session.get("notation")+"/lg-"+session.get("notation")+"-" +glycobaseid+".png")),format.raw/*46.186*/(""""  alt=""/>
                    """)))}/*47.23*/else/*47.27*/{_display_(Seq[Any](format.raw/*47.28*/("""
                        """),format.raw/*48.25*/("""<img class="img-responsive glycan-img" src=""""),_display_(/*48.70*/routes/*48.76*/.Assets.versioned("images/lg-cfg-uoxf/lg-cfg-uoxf-" +glycobaseid+".png")),format.raw/*48.148*/(""""  alt=""/>
                    """)))}),format.raw/*49.22*/("""

                        """),format.raw/*51.25*/("""<!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Publications</a></li>
                        <li role="presentation"><a href="#data" aria-controls="data" role="tab" data-toggle="tab">Data Source Description</a></li>
                        <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Profiles</a></li>
                        <li role="presentation"><a href="#enzymes" aria-controls="profile" role="tab" data-toggle="tab">Exoglycosidase Info</a></li>
                        """),_display_(/*57.26*/if(spectraId>0)/*57.41*/{_display_(Seq[Any](format.raw/*57.42*/("""
                            """),format.raw/*58.29*/("""<li role="presentation"><a href="#spectra" aria-controls="profile" role="tab" data-toggle="tab">MS2 Spectra</a></li>
                        """)))}),format.raw/*59.26*/("""
                    """),format.raw/*60.21*/("""</ul>

                        <!-- Tab panes -->
                    <div class="tab-content">

                        <div role="tabpanel" class="tab-pane active" id="home">
                            <h4>"""),_display_(/*66.34*/literatureLen),format.raw/*66.47*/(""" """),format.raw/*66.48*/("""Publications</h4>
                            """),_display_(/*67.30*/if(literatureLen > 0)/*67.51*/{_display_(Seq[Any](format.raw/*67.52*/("""
                                """),format.raw/*68.33*/("""<table class="table table-striped">
                                    <thead><tr>
                                        <th>Title</th><th>Year</th><th>GU (2-AB)</th><th>GU (Procainamide)</th><th>PeakArea</th><th>MS1</th><th>MS2</th>
                                    </tr></thead>
                                    <tbody>
                                    """),_display_(/*73.38*/for(lit <-LiteratureList) yield /*73.63*/{_display_(Seq[Any](format.raw/*73.64*/("""
                                        """),format.raw/*74.41*/("""<tr>
                                            <td><a href="/reference/"""),_display_(/*75.70*/lit/*75.73*/.getValue("Title")),format.raw/*75.91*/("""">"""),_display_(/*75.94*/lit/*75.97*/.getValue("Title")),format.raw/*75.115*/("""</a></td>
                                            <td>"""),_display_(/*76.50*/lit/*76.53*/.getValue("Year")),format.raw/*76.70*/("""</td>
                                            <td>"""),_display_(/*77.50*/lit/*77.53*/.getValue("Gu2AB")),format.raw/*77.71*/("""</td>
                                            <td>"""),_display_(/*78.50*/lit/*78.53*/.getValue("GuProcainamide")),format.raw/*78.80*/("""</td>
                                            <td>"""),_display_(/*79.50*/lit/*79.53*/.getValue("PeakArea")),format.raw/*79.74*/("""</td>
                                            <td>"""),_display_(/*80.50*/lit/*80.53*/.getValue("Ms1Verified")),format.raw/*80.77*/("""</td>
                                            <td>"""),_display_(/*81.50*/lit/*81.53*/.getValue("Ms2Verified")),format.raw/*81.77*/("""</td>
                                        </tr>
                                    """)))}),format.raw/*83.38*/("""</tbody>
                                </table>
                            """)))}),format.raw/*85.30*/("""
                        """),format.raw/*86.25*/("""</div>

                        <div role="tabpanel" class="tab-pane" id="data">
                        """),_display_(/*89.26*/if(recordLen > 0)/*89.43*/{_display_(Seq[Any](format.raw/*89.44*/("""
                            """),format.raw/*90.29*/("""<table class="table table-striped">
                                <thead><tr>
                                    <th>GU</th><th>Label</th><th>EvidenceType</th><th>Report</th><th>Sample</th><th>Taxon</th><th>Profile</th>
                                </tr></thead>
                                <tbody>
                                """),_display_(/*95.34*/for(record <-RecordList) yield /*95.58*/{_display_(Seq[Any](format.raw/*95.59*/("""
                                    """),format.raw/*96.37*/("""<tr>
                                        <td>"""),_display_(/*97.46*/record/*97.52*/.getValue("Gu")),format.raw/*97.67*/("""</td>
                                        <td>"""),_display_(/*98.46*/record/*98.52*/.getValue("LabelType")),format.raw/*98.74*/("""</td>
                                        <td><a href="""),_display_(/*99.54*/routes/*99.60*/.Application.listProfie4Type(record.getValue("EvidenceType").split("_")(1))),format.raw/*99.135*/(""">"""),_display_(/*99.137*/record/*99.143*/.getValue("EvidenceType").split("_")/*99.179*/(1)),format.raw/*99.182*/("""</a></td>
                                        """),_display_(/*100.42*/if(record.getValue("ReportName"))/*100.75*/{_display_(Seq[Any](format.raw/*100.76*/("""
                                            """),format.raw/*101.45*/("""<td><a href="""),_display_(/*101.58*/routes/*101.64*/.Application.searchReport(record.getValue("ReportName"))),format.raw/*101.120*/(""">"""),_display_(/*101.122*/record/*101.128*/.getValue("ReportName")),format.raw/*101.151*/("""</a></td>
                                        """)))}/*102.42*/else/*102.46*/{_display_(Seq[Any](format.raw/*102.47*/("""<td>  </td>""")))}),format.raw/*102.59*/("""
                                        """),_display_(/*103.42*/if(record.getValue("SampleName"))/*103.75*/{_display_(Seq[Any](format.raw/*103.76*/("""
                                            """),format.raw/*104.45*/("""<td><a href="""),_display_(/*104.58*/routes/*104.64*/.Application.searchSample(record.getValue("SampleName"))),format.raw/*104.120*/(""">"""),_display_(/*104.122*/record/*104.128*/.getValue("SampleName")),format.raw/*104.151*/("""</a></td>
                                        """)))}/*105.42*/else/*105.46*/{_display_(Seq[Any](format.raw/*105.47*/("""<td>  </td>""")))}),format.raw/*105.59*/("""
                                        """),_display_(/*106.42*/if(record.getValue("Taxon"))/*106.70*/{_display_(Seq[Any](format.raw/*106.71*/("""
                                            """),format.raw/*107.45*/("""<td><a href="""),_display_(/*107.58*/routes/*107.64*/.Application.searchTaxon(record.getValue("Taxon"))),format.raw/*107.114*/(""">"""),_display_(/*107.116*/record/*107.122*/.getValue("Taxon").toLowerCase.capitalize),format.raw/*107.163*/("""</a></td>
                                        """)))}/*108.42*/else/*108.46*/{_display_(Seq[Any](format.raw/*108.47*/("""<td>  </td>""")))}),format.raw/*108.59*/("""
                                        """),_display_(/*109.42*/if(record.getValue("EvidenceURI"))/*109.76*/{_display_(Seq[Any](format.raw/*109.77*/("""
                                            """),format.raw/*110.45*/("""<td><a href="""),_display_(/*110.58*/routes/*110.64*/.Application.searchProfile(record.getValue("EvidenceURI").split("/")(4))),format.raw/*110.136*/(""">"""),_display_(/*110.138*/record/*110.144*/.getValue("EvidenceURI").split("/")/*110.179*/(4)),format.raw/*110.182*/("""</a></td>
                                        """)))}/*111.42*/else/*111.46*/{_display_(Seq[Any](format.raw/*111.47*/("""<td> </td>""")))}),format.raw/*111.58*/("""
                                    """),format.raw/*112.37*/("""</tr>
                                """)))}),format.raw/*113.34*/("""</tbody>
                            </table>
                        """)))}),format.raw/*115.26*/("""
                        """),format.raw/*116.25*/("""</div>
                        <div role="tabpanel" class="tab-pane" id="profile">

                        """),_display_(/*119.26*/if(profileLen > 0)/*119.44*/{_display_(Seq[Any](format.raw/*119.45*/("""
                            """),format.raw/*120.29*/("""<table class="table table-striped">
                                <thead><tr>
                                    <th>Evidence Profile</th><th>Evidence Type</th>
                                </tr></thead>
                                <tbody>
                                """),_display_(/*125.34*/for(profile <-ProfileList) yield /*125.60*/{_display_(Seq[Any](format.raw/*125.61*/("""
                                    """),format.raw/*126.37*/("""<tr>
                                        """),_display_(/*127.42*/if(profile.getValue("ProfileURI"))/*127.76*/ {_display_(Seq[Any](format.raw/*127.78*/("""
                                            """),format.raw/*128.45*/("""<td><a href="""),_display_(/*128.58*/routes/*128.64*/.Application.searchProfile(profile.getValue("ProfileURI").split("/")(4))),format.raw/*128.136*/(""">"""),_display_(/*128.138*/profile/*128.145*/.getValue("ProfileURI").split("/")/*128.179*/(4)),format.raw/*128.182*/("""</a></td>
                                        """)))}),format.raw/*129.42*/("""
                                        """),_display_(/*130.42*/if(profile.getValue("EvidenceType"))/*130.78*/ {_display_(Seq[Any](format.raw/*130.80*/("""
                                            """),format.raw/*131.45*/("""<td><a href="""),_display_(/*131.58*/routes/*131.64*/.Application.listProfie4Type(profile.getValue("EvidenceType").split("_")(1))),format.raw/*131.140*/(""">"""),_display_(/*131.142*/profile/*131.149*/.getValue("EvidenceType").split("_")/*131.185*/(1)),format.raw/*131.188*/("""</a></td>
                                        """)))}),format.raw/*132.42*/("""
                                    """),format.raw/*133.37*/("""</tr>
                                """)))}),format.raw/*134.34*/("""</tbody>
                            </table>
                        """)))}),format.raw/*136.26*/("""

                        """),format.raw/*138.25*/("""</div>
                        <div role="tabpanel" class="tab-pane" id="enzymes">
                            <h4>"""),_display_(/*140.34*/reactionLen),format.raw/*140.45*/(""" """),format.raw/*140.46*/("""reactions</h4>
                            """),_display_(/*141.30*/if(reactionLen > 0)/*141.49*/{_display_(Seq[Any](format.raw/*141.50*/("""
                                """),format.raw/*142.33*/("""<table class="table ">
                                    <thead><tr>
                                        <th>Digest Parent</th><th>Enzyme</th><th>Current Structure</th><th>Enzyme</th><th>Digest Product</th>
                                    </tr></thead>
                                    <tbody>
                                    """),_display_(/*147.38*/for(reaction <-ReactionList) yield /*147.66*/{_display_(Seq[Any](format.raw/*147.67*/("""
                                        """),format.raw/*148.41*/("""<tr>"""),_display_(/*148.46*/if(reaction.getValue("SubstrateUoxf")==uoxf)/*148.90*/{_display_(Seq[Any](format.raw/*148.91*/("""
                                            """),format.raw/*149.45*/("""<td></td><td></td><td><img src="""),_display_(/*149.77*/{routes.Image.showImageReaction(glycobaseid, session.get("notation"), "normal")}),format.raw/*149.157*/(""" """),format.raw/*149.158*/("""" alt=""/><br/>"""),_display_(/*149.174*/uoxf),format.raw/*149.178*/("""</td>
                                            <td>"""),_display_(/*150.50*/reaction/*150.58*/.getValue("Enzyme")),format.raw/*150.77*/("""</td>
                                            <td><img src="""),_display_(/*151.59*/{routes.Image.showImageReaction(reaction.getValue("ProductId"), session.get("notation"), "normal")}),format.raw/*151.158*/(""" """),format.raw/*151.159*/("""" alt=""/><br/> """),_display_(/*151.176*/reaction/*151.184*/.getValue("ProductUoxf")),format.raw/*151.208*/("""</td>
                                        """)))}/*152.42*/else/*152.46*/{_display_(Seq[Any](format.raw/*152.47*/("""
                                            """),format.raw/*153.45*/("""<td><img src="""),_display_(/*153.59*/{routes.Image.showImageReaction(reaction.getValue("SubstrateId"), session.get("notation"), "normal")}),format.raw/*153.160*/(""" """),format.raw/*153.161*/("""" alt=""/><br/>"""),_display_(/*153.177*/reaction/*153.185*/.getValue("SubstrateUoxf")),format.raw/*153.211*/("""</td>
                                            <td>"""),_display_(/*154.50*/reaction/*154.58*/.getValue("Enzyme")),format.raw/*154.77*/("""</td>
                                            <td><img src="""),_display_(/*155.59*/{routes.Image.showImageReaction(glycobaseid, session.get("notation"), "normal")}),format.raw/*155.139*/(""" """),format.raw/*155.140*/("""" alt=""/><br/>"""),_display_(/*155.156*/uoxf),format.raw/*155.160*/("""</td><td></td><td></td>

                                        """)))}),format.raw/*157.42*/("""</tr>
                                    """)))}),format.raw/*158.38*/("""</tbody>
                                </table>
                            """)))}),format.raw/*160.30*/("""
                        """),format.raw/*161.25*/("""</div>

                        <div role="tabpanel" class="tab-pane" id="spectra">
                            <h4>Spectra</h4>
                            <div id="wrap">
                                <div id="stgraph" class="stgraph"></div>
                            </div>
                        </div>


                        <div role="tabpanel" class="tab-pane" id="messages">...</div>
                        <div role="tabpanel" class="tab-pane" id="settings">...</div>
                    </div>


                </div>

                <div class="col-md-3 sidebar">
                    """),_display_(/*179.22*/views/*179.27*/.html.format.format()),format.raw/*179.48*/("""
                    """),format.raw/*180.21*/("""<hr/>
                    """),format.raw/*181.61*/("""
                    """),_display_(/*182.22*/views/*182.27*/.html.structureStats.std(resultCeGuStd, resultHplcGuStd, resultUplcGuStd, resultPgc, result2AB, resultProc)),format.raw/*182.134*/("""

                    """),_display_(/*184.22*/if(spectraId>0)/*184.37*/{_display_(Seq[Any](format.raw/*184.38*/("""
                        """),_display_(/*185.26*/views/*185.31*/.html.general.spectraIT()),format.raw/*185.56*/("""
                    """)))}),format.raw/*186.22*/("""

                    """),format.raw/*188.21*/("""<hr/>
                    <h3 class="fh-desc" >Information</h3>
                    """),_display_(/*190.22*/for(report <- UniqueReports) yield /*190.50*/{_display_(Seq[Any](format.raw/*190.51*/("""
                        """),_display_(/*191.26*/if(report.length() > 30)/*191.50*/ {_display_(Seq[Any](format.raw/*191.52*/("""
                            """),format.raw/*192.29*/("""<span class="badge">"""),_display_(/*192.50*/report/*192.56*/.toString().substring(0, 30)),format.raw/*192.84*/("""</span>
                        """)))}/*193.27*/else/*193.32*/{_display_(Seq[Any](format.raw/*193.33*/("""
                            """),format.raw/*194.29*/("""<span class="badge">"""),_display_(/*194.50*/report),format.raw/*194.56*/("""</span>
                        """)))}),format.raw/*195.26*/("""
                    """)))}),format.raw/*196.22*/("""

                """),format.raw/*198.17*/("""</div>
            </div>
        </div>
    </div>

    <script type="text/javascript">

            var obj = """),_display_(/*205.24*/Html(json)),format.raw/*205.34*/("""

            """),format.raw/*207.13*/("""var chart = st.chart          // new chart
                    .ms()                 // of type MS
                    .xlabel("m/z")        // x-axis label
                    .ylabel("Intensity"); // y-axis label
            chart.render("#stgraph");     // render chart to id 'stgraph'

            var handle = st.data          // new handler
                    .set()                    // of type set
                    // y-domain limits
                    .x("peaks.mz")            // x-accessor
                    .y("peaks.intensity");    // y-accessor

            // bind the data handler to the chart
            chart.load(handle);
            // load the spectrum and annotations for Uridine
            //handle.add("/assets/spectra/out2.json");
            handle.add(obj);


    </script>

""")))}),format.raw/*228.2*/("""
"""))
      }
    }
  }

  def render(glycobaseid:String,uoxf:String,json:String,spectraId:Int,recordLen:Int,reactionLen:Int,literatureLen:Int,profileLen:Int,RecordList:List[SparqlEntity],LiteratureList:List[SparqlEntity],ReactionList:List[SparqlEntity],ProfileList:List[SparqlEntity],UniqueReports:Set[String],resultCeGuStd:List[SparqlEntity],resultHplcGuStd:List[SparqlEntity],resultUplcGuStd:List[SparqlEntity],resultPgc:List[SparqlEntity],result2AB:List[SparqlEntity],resultProc:List[SparqlEntity]): play.twirl.api.HtmlFormat.Appendable = apply(glycobaseid,uoxf,json,spectraId,recordLen,reactionLen,literatureLen,profileLen,RecordList,LiteratureList,ReactionList,ProfileList,UniqueReports,resultCeGuStd,resultHplcGuStd,resultUplcGuStd,resultPgc,result2AB,resultProc)

  def f:((String,String,String,Int,Int,Int,Int,Int,List[SparqlEntity],List[SparqlEntity],List[SparqlEntity],List[SparqlEntity],Set[String],List[SparqlEntity],List[SparqlEntity],List[SparqlEntity],List[SparqlEntity],List[SparqlEntity],List[SparqlEntity]) => play.twirl.api.HtmlFormat.Appendable) = (glycobaseid,uoxf,json,spectraId,recordLen,reactionLen,literatureLen,profileLen,RecordList,LiteratureList,ReactionList,ProfileList,UniqueReports,resultCeGuStd,resultHplcGuStd,resultUplcGuStd,resultPgc,result2AB,resultProc) => apply(glycobaseid,uoxf,json,spectraId,recordLen,reactionLen,literatureLen,profileLen,RecordList,LiteratureList,ReactionList,ProfileList,UniqueReports,resultCeGuStd,resultHplcGuStd,resultUplcGuStd,resultPgc,result2AB,resultProc)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Mon Feb 26 14:39:19 AEST 2018
                  SOURCE: /Users/s2980369/IdeaProjects/glycobase/glycobase/app/views/glycan.scala.html
                  HASH: 7f2a8a9e508a41d65e58e3024e00ace2e6d92762
                  MATRIX: 651->1|674->19|1237->41|1817->525|1850->532|1862->536|1901->538|1934->545|1960->562|1999->563|2035->572|2076->586|2091->592|2150->630|2211->664|2226->670|2285->708|2393->789|2408->795|2469->834|2507->842|2541->1074|2574->1080|2968->1447|2993->1451|3415->1846|3451->1873|3490->1874|3543->1899|3615->1944|3630->1950|3762->2060|3814->2094|3827->2098|3866->2099|3919->2124|3991->2169|4006->2175|4100->2247|4164->2280|4218->2306|4934->2995|4958->3010|4997->3011|5054->3040|5227->3182|5276->3203|5513->3413|5547->3426|5576->3427|5650->3474|5680->3495|5719->3496|5780->3529|6175->3897|6216->3922|6255->3923|6324->3964|6425->4038|6437->4041|6476->4059|6506->4062|6518->4065|6558->4083|6644->4142|6656->4145|6694->4162|6776->4217|6788->4220|6827->4238|6909->4293|6921->4296|6969->4323|7051->4378|7063->4381|7105->4402|7187->4457|7199->4460|7244->4484|7326->4539|7338->4542|7383->4566|7503->4655|7613->4734|7666->4759|7799->4865|7825->4882|7864->4883|7921->4912|8290->5254|8330->5278|8369->5279|8434->5316|8511->5366|8526->5372|8562->5387|8640->5438|8655->5444|8698->5466|8784->5525|8799->5531|8896->5606|8926->5608|8942->5614|8988->5650|9013->5653|9092->5704|9135->5737|9175->5738|9249->5783|9290->5796|9306->5802|9385->5858|9416->5860|9433->5866|9479->5889|9550->5940|9564->5944|9604->5945|9648->5957|9718->5999|9761->6032|9801->6033|9875->6078|9916->6091|9932->6097|10011->6153|10042->6155|10059->6161|10105->6184|10176->6235|10190->6239|10230->6240|10274->6252|10344->6294|10382->6322|10422->6323|10496->6368|10537->6381|10553->6387|10626->6437|10657->6439|10674->6445|10738->6486|10809->6537|10823->6541|10863->6542|10907->6554|10977->6596|11021->6630|11061->6631|11135->6676|11176->6689|11192->6695|11287->6767|11318->6769|11335->6775|11381->6810|11407->6813|11478->6864|11492->6868|11532->6869|11575->6880|11641->6917|11712->6956|11815->7027|11869->7052|12006->7161|12034->7179|12074->7180|12132->7209|12443->7492|12486->7518|12526->7519|12592->7556|12666->7602|12710->7636|12751->7638|12825->7683|12866->7696|12882->7702|12977->7774|13008->7776|13026->7783|13071->7817|13097->7820|13180->7871|13250->7913|13296->7949|13337->7951|13411->7996|13452->8009|13468->8015|13567->8091|13598->8093|13616->8100|13663->8136|13689->8139|13772->8190|13838->8227|13909->8266|14012->8337|14067->8363|14211->8479|14244->8490|14274->8491|14346->8535|14375->8554|14415->8555|14477->8588|14849->8932|14894->8960|14934->8961|15004->9002|15037->9007|15091->9051|15131->9052|15205->9097|15265->9129|15368->9209|15399->9210|15444->9226|15471->9230|15554->9285|15572->9293|15613->9312|15705->9376|15827->9475|15858->9476|15904->9493|15923->9501|15970->9525|16037->9572|16051->9576|16091->9577|16165->9622|16207->9636|16331->9737|16362->9738|16407->9754|16426->9762|16475->9788|16558->9843|16576->9851|16617->9870|16709->9934|16812->10014|16843->10015|16888->10031|16915->10035|17013->10101|17088->10144|17199->10223|17253->10248|17888->10855|17903->10860|17946->10881|17996->10902|18051->10968|18101->10990|18116->10995|18246->11102|18297->11125|18322->11140|18362->11141|18416->11167|18431->11172|18478->11197|18532->11219|18583->11241|18696->11326|18741->11354|18781->11355|18835->11381|18869->11405|18910->11407|18968->11436|19017->11457|19033->11463|19083->11491|19136->11525|19150->11530|19190->11531|19248->11560|19297->11581|19325->11587|19390->11620|19444->11642|19491->11660|19632->11773|19664->11783|19707->11797|20551->12610
                  LINES: 24->1|25->3|30->4|35->4|41->10|41->10|41->10|43->12|43->12|43->12|44->13|44->13|44->13|44->13|45->14|45->14|45->14|46->15|46->15|46->15|47->16|49->25|51->27|58->34|58->34|69->45|69->45|69->45|70->46|70->46|70->46|70->46|71->47|71->47|71->47|72->48|72->48|72->48|72->48|73->49|75->51|81->57|81->57|81->57|82->58|83->59|84->60|90->66|90->66|90->66|91->67|91->67|91->67|92->68|97->73|97->73|97->73|98->74|99->75|99->75|99->75|99->75|99->75|99->75|100->76|100->76|100->76|101->77|101->77|101->77|102->78|102->78|102->78|103->79|103->79|103->79|104->80|104->80|104->80|105->81|105->81|105->81|107->83|109->85|110->86|113->89|113->89|113->89|114->90|119->95|119->95|119->95|120->96|121->97|121->97|121->97|122->98|122->98|122->98|123->99|123->99|123->99|123->99|123->99|123->99|123->99|124->100|124->100|124->100|125->101|125->101|125->101|125->101|125->101|125->101|125->101|126->102|126->102|126->102|126->102|127->103|127->103|127->103|128->104|128->104|128->104|128->104|128->104|128->104|128->104|129->105|129->105|129->105|129->105|130->106|130->106|130->106|131->107|131->107|131->107|131->107|131->107|131->107|131->107|132->108|132->108|132->108|132->108|133->109|133->109|133->109|134->110|134->110|134->110|134->110|134->110|134->110|134->110|134->110|135->111|135->111|135->111|135->111|136->112|137->113|139->115|140->116|143->119|143->119|143->119|144->120|149->125|149->125|149->125|150->126|151->127|151->127|151->127|152->128|152->128|152->128|152->128|152->128|152->128|152->128|152->128|153->129|154->130|154->130|154->130|155->131|155->131|155->131|155->131|155->131|155->131|155->131|155->131|156->132|157->133|158->134|160->136|162->138|164->140|164->140|164->140|165->141|165->141|165->141|166->142|171->147|171->147|171->147|172->148|172->148|172->148|172->148|173->149|173->149|173->149|173->149|173->149|173->149|174->150|174->150|174->150|175->151|175->151|175->151|175->151|175->151|175->151|176->152|176->152|176->152|177->153|177->153|177->153|177->153|177->153|177->153|177->153|178->154|178->154|178->154|179->155|179->155|179->155|179->155|179->155|181->157|182->158|184->160|185->161|203->179|203->179|203->179|204->180|205->181|206->182|206->182|206->182|208->184|208->184|208->184|209->185|209->185|209->185|210->186|212->188|214->190|214->190|214->190|215->191|215->191|215->191|216->192|216->192|216->192|216->192|217->193|217->193|217->193|218->194|218->194|218->194|219->195|220->196|222->198|229->205|229->205|231->207|252->228
                  -- GENERATED --
              */
          