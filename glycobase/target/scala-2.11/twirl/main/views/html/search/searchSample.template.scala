
package views.html.search

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._
/*1.2*/import sparql._

object searchSample extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template4[Int,String,Map[String, String],List[String],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*3.2*/(count: Int, sname: String, result: Map[String, String], sample:List[String]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*3.79*/("""


"""),_display_(/*6.2*/main/*6.6*/ {_display_(Seq[Any](format.raw/*6.8*/("""

    """),format.raw/*15.13*/("""
    """),format.raw/*16.5*/("""<div class="section">
        <div class="container" >
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header">
                        <div class="pull-left">
                            <h1 class="info-header-grey text-primary">Sample Source - """),_display_(/*22.88*/sname),format.raw/*22.93*/("""</h1>
                            <div class="minitext">"""),_display_(/*23.52*/count),format.raw/*23.57*/(""" """),format.raw/*23.58*/("""glycan structures</div>
                        </div>
                        <div class="pull-right">
                            <div class="text-right brand"></div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-9">
                """),_display_(/*34.18*/if(count > 0)/*34.31*/ {_display_(Seq[Any](format.raw/*34.33*/("""
                    """),format.raw/*35.21*/("""<table class="table table-striped">
                        <thead><tr>
                            <th>Glycan</th>
                            <th>Name</th>
                            <th>Mono Mass</th>
                            <th>HPLC</th>
                            <th>UPLC (2-AB)</th>
                            <th>UPLC (Proc)</th>
                            <th>RPUPLC</th>
                            <th>CE</th>
                        </tr></thead>
                        <tbody>
                        """),_display_(/*47.26*/for((key, value) <- result) yield /*47.53*/ {_display_(Seq[Any](format.raw/*47.55*/("""
                            """),format.raw/*48.29*/("""<tr>
                                """),_display_(/*49.34*/if(session.get("notation"))/*49.61*/ {_display_(Seq[Any](format.raw/*49.63*/("""
                                    """),format.raw/*50.37*/("""<td><img src=""""),_display_(/*50.52*/routes/*50.58*/.Assets.versioned("images/sm-" + session.get("notation") + "/sm-" + session.get("notation") + "-" + key + ".png")),format.raw/*50.171*/(""""><br/>
                                """)))}/*51.35*/else/*51.40*/{_display_(Seq[Any](format.raw/*51.41*/("""
                                    """),format.raw/*52.37*/("""<td><img src=""""),_display_(/*52.52*/routes/*52.58*/.Assets.versioned("images/sm-cfg-uoxf/sm-cfg-uoxf-" + key + ".png")),format.raw/*52.125*/("""">
                                    """)))}),format.raw/*53.38*/("""
                            """),format.raw/*54.29*/("""</td>
                            <td><a href="""),_display_(/*55.42*/routes/*55.48*/.Application.showGlycan(key)),format.raw/*55.76*/(""">"""),_display_(/*55.78*/value/*55.83*/.split("\t")/*55.95*/(1)),format.raw/*55.98*/("""</a></td>
                                """),_display_(/*56.34*/if(value.split("\t")(5) == "null")/*56.68*/ {_display_(Seq[Any](format.raw/*56.70*/("""
                                    """),format.raw/*57.37*/("""<td></td>
                                """)))}/*58.35*/else/*58.40*/{_display_(Seq[Any](format.raw/*58.41*/("""
                                    """),format.raw/*59.37*/("""<td>"""),_display_(/*59.42*/BigDecimal(value.split("\t")(5).toDouble)/*59.83*/.setScale(4, BigDecimal.RoundingMode.HALF_UP).toDouble),format.raw/*59.137*/("""</td>
                                """)))}),format.raw/*60.34*/("""
                                """),_display_(/*61.34*/if(value.split("\t")(2) == "null")/*61.68*/ {_display_(Seq[Any](format.raw/*61.70*/("""
                                    """),format.raw/*62.37*/("""<td></td>
                                """)))}/*63.35*/else/*63.40*/{_display_(Seq[Any](format.raw/*63.41*/("""
                                    """),format.raw/*64.37*/("""<td>"""),_display_(/*64.42*/BigDecimal(value.split("\t")(2).toDouble)/*64.83*/.setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble),format.raw/*64.137*/("""</td>
                                """)))}),format.raw/*65.34*/("""
                                """),_display_(/*66.34*/if(value.split("\t")(4) == "null")/*66.68*/ {_display_(Seq[Any](format.raw/*66.70*/("""
                                    """),format.raw/*67.37*/("""<td></td>
                                """)))}/*68.35*/else/*68.40*/{_display_(Seq[Any](format.raw/*68.41*/("""
                                    """),format.raw/*69.37*/("""<td>"""),_display_(/*69.42*/BigDecimal(value.split("\t")(4).toDouble)/*69.83*/.setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble),format.raw/*69.137*/("""</td>
                                """)))}),format.raw/*70.34*/("""
                                """),_display_(/*71.34*/if(value.split("\t")(6) == "null")/*71.68*/ {_display_(Seq[Any](format.raw/*71.70*/("""
                                    """),format.raw/*72.37*/("""<td></td>
                                """)))}/*73.35*/else/*73.40*/{_display_(Seq[Any](format.raw/*73.41*/("""
                                    """),format.raw/*74.37*/("""<td>"""),_display_(/*74.42*/BigDecimal(value.split("\t")(6).toDouble)/*74.83*/.setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble),format.raw/*74.137*/("""</td>
                                """)))}),format.raw/*75.34*/("""
                                """),_display_(/*76.34*/if(value.split("\t")(3) == "null")/*76.68*/ {_display_(Seq[Any](format.raw/*76.70*/("""
                                    """),format.raw/*77.37*/("""<td></td>
                                """)))}/*78.35*/else/*78.40*/{_display_(Seq[Any](format.raw/*78.41*/("""
                                    """),format.raw/*79.37*/("""<td>"""),_display_(/*79.42*/BigDecimal(value.split("\t")(3).toDouble)/*79.83*/.setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble),format.raw/*79.137*/("""</td>
                                """)))}),format.raw/*80.34*/("""
                                """),_display_(/*81.34*/if(value.split("\t")(0) == "null")/*81.68*/ {_display_(Seq[Any](format.raw/*81.70*/("""
                                    """),format.raw/*82.37*/("""<td></td>
                                """)))}/*83.35*/else/*83.40*/{_display_(Seq[Any](format.raw/*83.41*/("""
                                    """),format.raw/*84.37*/("""<td>"""),_display_(/*84.42*/BigDecimal(value.split("\t")(0).toDouble)/*84.83*/.setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble),format.raw/*84.137*/("""</td>
                                """)))}),format.raw/*85.34*/("""
                            """),format.raw/*86.29*/("""</tr>
                        """)))}),format.raw/*87.26*/("""
                        """),format.raw/*88.25*/("""</tbody>

                    </table>
                """)))}),format.raw/*91.18*/("""
                """),format.raw/*92.17*/("""</div>
                <div class="col-md-3">
                    """),_display_(/*94.22*/views/*94.27*/.html.format.format()),format.raw/*94.48*/("""
                    """),_display_(/*95.22*/views/*95.27*/.html.search.filterUoxfName()),format.raw/*95.56*/("""
                    """),_display_(/*96.22*/views/*96.27*/.html.general.allsamples(sample)),format.raw/*96.59*/("""
                """),format.raw/*97.17*/("""</div>
            </div>
        </div>
    </div>
""")))}))
      }
    }
  }

  def render(count:Int,sname:String,result:Map[String, String],sample:List[String]): play.twirl.api.HtmlFormat.Appendable = apply(count,sname,result,sample)

  def f:((Int,String,Map[String, String],List[String]) => play.twirl.api.HtmlFormat.Appendable) = (count,sname,result,sample) => apply(count,sname,result,sample)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Mon Feb 26 14:39:20 AEST 2018
                  SOURCE: /Users/s2980369/IdeaProjects/glycobase/glycobase/app/views/search/searchSample.scala.html
                  HASH: add6f09b0326ef938b32cb34dc0f95a34ae5af23
                  MATRIX: 658->1|1022->19|1194->96|1223->100|1234->104|1272->106|1306->331|1338->336|1671->642|1697->647|1781->704|1807->709|1836->710|2271->1118|2293->1131|2333->1133|2382->1154|2933->1678|2976->1705|3016->1707|3073->1736|3138->1774|3174->1801|3214->1803|3279->1840|3321->1855|3336->1861|3471->1974|3531->2016|3544->2021|3583->2022|3648->2059|3690->2074|3705->2080|3794->2147|3865->2187|3922->2216|3996->2263|4011->2269|4060->2297|4089->2299|4103->2304|4124->2316|4148->2319|4218->2362|4261->2396|4301->2398|4366->2435|4428->2479|4441->2484|4480->2485|4545->2522|4577->2527|4627->2568|4703->2622|4773->2661|4834->2695|4877->2729|4917->2731|4982->2768|5044->2812|5057->2817|5096->2818|5161->2855|5193->2860|5243->2901|5319->2955|5389->2994|5450->3028|5493->3062|5533->3064|5598->3101|5660->3145|5673->3150|5712->3151|5777->3188|5809->3193|5859->3234|5935->3288|6005->3327|6066->3361|6109->3395|6149->3397|6214->3434|6276->3478|6289->3483|6328->3484|6393->3521|6425->3526|6475->3567|6551->3621|6621->3660|6682->3694|6725->3728|6765->3730|6830->3767|6892->3811|6905->3816|6944->3817|7009->3854|7041->3859|7091->3900|7167->3954|7237->3993|7298->4027|7341->4061|7381->4063|7446->4100|7508->4144|7521->4149|7560->4150|7625->4187|7657->4192|7707->4233|7783->4287|7853->4326|7910->4355|7972->4386|8025->4411|8112->4467|8157->4484|8251->4551|8265->4556|8307->4577|8356->4599|8370->4604|8420->4633|8469->4655|8483->4660|8536->4692|8581->4709
                  LINES: 24->1|29->3|34->3|37->6|37->6|37->6|39->15|40->16|46->22|46->22|47->23|47->23|47->23|58->34|58->34|58->34|59->35|71->47|71->47|71->47|72->48|73->49|73->49|73->49|74->50|74->50|74->50|74->50|75->51|75->51|75->51|76->52|76->52|76->52|76->52|77->53|78->54|79->55|79->55|79->55|79->55|79->55|79->55|79->55|80->56|80->56|80->56|81->57|82->58|82->58|82->58|83->59|83->59|83->59|83->59|84->60|85->61|85->61|85->61|86->62|87->63|87->63|87->63|88->64|88->64|88->64|88->64|89->65|90->66|90->66|90->66|91->67|92->68|92->68|92->68|93->69|93->69|93->69|93->69|94->70|95->71|95->71|95->71|96->72|97->73|97->73|97->73|98->74|98->74|98->74|98->74|99->75|100->76|100->76|100->76|101->77|102->78|102->78|102->78|103->79|103->79|103->79|103->79|104->80|105->81|105->81|105->81|106->82|107->83|107->83|107->83|108->84|108->84|108->84|108->84|109->85|110->86|111->87|112->88|115->91|116->92|118->94|118->94|118->94|119->95|119->95|119->95|120->96|120->96|120->96|121->97
                  -- GENERATED --
              */
          