
package views.html.search

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._
/*1.2*/import sparql._

object searchTissueResult extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template3[Int,String,Map[String, String],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*3.2*/(count: Int, tissueName: String, result: Map[String, String]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*3.63*/("""


"""),_display_(/*6.2*/main/*6.6*/ {_display_(Seq[Any](format.raw/*6.8*/("""

    """),format.raw/*15.13*/("""
    """),format.raw/*16.5*/("""<div class="section">
        <div class="container" >
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header">
                        <div class="pull-left">
                            <h1 class="info-header-grey text-primary">Tissue Result - """),_display_(/*22.88*/tissueName),format.raw/*22.98*/("""</h1>
                            <div class="minitext">"""),_display_(/*23.52*/count),format.raw/*23.57*/(""" """),format.raw/*23.58*/("""glycan structures</div>
                        </div>
                        <div class="pull-right">
                            <div class="text-right brand"></div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-9">
                """),_display_(/*34.18*/if(count > 0)/*34.31*/ {_display_(Seq[Any](format.raw/*34.33*/("""
                    """),format.raw/*35.21*/("""<table class="table table-striped">
                        <thead><tr>
                            <th>Glycan</th>
                            <th>Name</th>
                            <th>Mono Mass</th>
                            <th>HPLC</th>
                            <th>UPLC (2-AB)</th>
                            <th>UPLC (Proc)</th>
                            <th>RPUPLC</th>
                            <th>CE</th>
                        </tr></thead>
                        <tbody>
                        """),_display_(/*47.26*/for((key, value) <- result) yield /*47.53*/ {_display_(Seq[Any](format.raw/*47.55*/("""
                            """),format.raw/*48.29*/("""<tr>
                                """),_display_(/*49.34*/if(session.get("notation"))/*49.61*/ {_display_(Seq[Any](format.raw/*49.63*/("""
                                    """),format.raw/*50.37*/("""<td><img src=""""),_display_(/*50.52*/routes/*50.58*/.Assets.versioned("images/sm-" + session.get("notation") + "/sm-" + session.get("notation") + "-" + key + ".png")),format.raw/*50.171*/(""""><br/>
                                """)))}/*51.35*/else/*51.40*/{_display_(Seq[Any](format.raw/*51.41*/("""
                                    """),format.raw/*52.37*/("""<td><img src=""""),_display_(/*52.52*/routes/*52.58*/.Assets.versioned("images/sm-cfg-uoxf/sm-cfg-uoxf-" + key + ".png")),format.raw/*52.125*/("""">
                                    """)))}),format.raw/*53.38*/("""
                            """),format.raw/*54.29*/("""</td>
                            <td><a href="""),_display_(/*55.42*/routes/*55.48*/.Application.showGlycan(key)),format.raw/*55.76*/(""">"""),_display_(/*55.78*/value/*55.83*/.split("\t")/*55.95*/(1)),format.raw/*55.98*/("""</a></td>
                                """),_display_(/*56.34*/if(value.split("\t")(5) == "null")/*56.68*/ {_display_(Seq[Any](format.raw/*56.70*/("""
                                    """),format.raw/*57.37*/("""<td></td>
                                """)))}/*58.35*/else/*58.40*/{_display_(Seq[Any](format.raw/*58.41*/("""
                                    """),format.raw/*59.37*/("""<td>"""),_display_(/*59.42*/BigDecimal(value.split("\t")(5).toDouble)/*59.83*/.setScale(4, BigDecimal.RoundingMode.HALF_UP).toDouble),format.raw/*59.137*/("""</td>
                                """)))}),format.raw/*60.34*/("""
                                """),_display_(/*61.34*/if(value.split("\t")(2) == "null")/*61.68*/ {_display_(Seq[Any](format.raw/*61.70*/("""
                                    """),format.raw/*62.37*/("""<td></td>
                                """)))}/*63.35*/else/*63.40*/{_display_(Seq[Any](format.raw/*63.41*/("""
                                    """),format.raw/*64.37*/("""<td>"""),_display_(/*64.42*/BigDecimal(value.split("\t")(2).toDouble)/*64.83*/.setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble),format.raw/*64.137*/("""</td>
                                """)))}),format.raw/*65.34*/("""
                                """),_display_(/*66.34*/if(value.split("\t")(4) == "null")/*66.68*/ {_display_(Seq[Any](format.raw/*66.70*/("""
                                    """),format.raw/*67.37*/("""<td></td>
                                """)))}/*68.35*/else/*68.40*/{_display_(Seq[Any](format.raw/*68.41*/("""
                                    """),format.raw/*69.37*/("""<td>"""),_display_(/*69.42*/BigDecimal(value.split("\t")(4).toDouble)/*69.83*/.setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble),format.raw/*69.137*/("""</td>
                                """)))}),format.raw/*70.34*/("""
                                """),_display_(/*71.34*/if(value.split("\t")(6) == "null")/*71.68*/ {_display_(Seq[Any](format.raw/*71.70*/("""
                                    """),format.raw/*72.37*/("""<td></td>
                                """)))}/*73.35*/else/*73.40*/{_display_(Seq[Any](format.raw/*73.41*/("""
                                    """),format.raw/*74.37*/("""<td>"""),_display_(/*74.42*/BigDecimal(value.split("\t")(6).toDouble)/*74.83*/.setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble),format.raw/*74.137*/("""</td>
                                """)))}),format.raw/*75.34*/("""
                                """),_display_(/*76.34*/if(value.split("\t")(3) == "null")/*76.68*/ {_display_(Seq[Any](format.raw/*76.70*/("""
                                    """),format.raw/*77.37*/("""<td></td>
                                """)))}/*78.35*/else/*78.40*/{_display_(Seq[Any](format.raw/*78.41*/("""
                                    """),format.raw/*79.37*/("""<td>"""),_display_(/*79.42*/BigDecimal(value.split("\t")(3).toDouble)/*79.83*/.setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble),format.raw/*79.137*/("""</td>
                                """)))}),format.raw/*80.34*/("""
                                """),_display_(/*81.34*/if(value.split("\t")(0) == "null")/*81.68*/ {_display_(Seq[Any](format.raw/*81.70*/("""
                                    """),format.raw/*82.37*/("""<td></td>
                                """)))}/*83.35*/else/*83.40*/{_display_(Seq[Any](format.raw/*83.41*/("""
                                    """),format.raw/*84.37*/("""<td>"""),_display_(/*84.42*/BigDecimal(value.split("\t")(0).toDouble)/*84.83*/.setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble),format.raw/*84.137*/("""</td>
                                """)))}),format.raw/*85.34*/("""
                            """),format.raw/*86.29*/("""</tr>
                        """)))}),format.raw/*87.26*/("""
                        """),format.raw/*88.25*/("""</tbody>

                    </table>
                """)))}),format.raw/*91.18*/("""
                """),format.raw/*92.17*/("""</div>
                <div class="col-md-3">
                """),_display_(/*94.18*/views/*94.23*/.html.format.format()),format.raw/*94.44*/("""
                """),format.raw/*95.17*/("""</div>
            </div>
        </div>
    </div>
""")))}))
      }
    }
  }

  def render(count:Int,tissueName:String,result:Map[String, String]): play.twirl.api.HtmlFormat.Appendable = apply(count,tissueName,result)

  def f:((Int,String,Map[String, String]) => play.twirl.api.HtmlFormat.Appendable) = (count,tissueName,result) => apply(count,tissueName,result)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Mon Feb 26 14:39:20 AEST 2018
                  SOURCE: /Users/s2980369/IdeaProjects/glycobase/glycobase/app/views/search/searchTissueResult.scala.html
                  HASH: 29cd945419599ffc3034e30b40b5c476f90338b8
                  MATRIX: 658->1|1015->19|1171->80|1200->84|1211->88|1249->90|1283->321|1315->326|1648->632|1679->642|1763->699|1789->704|1818->705|2253->1113|2275->1126|2315->1128|2364->1149|2915->1673|2958->1700|2998->1702|3055->1731|3120->1769|3156->1796|3196->1798|3261->1835|3303->1850|3318->1856|3453->1969|3513->2011|3526->2016|3565->2017|3630->2054|3672->2069|3687->2075|3776->2142|3847->2182|3904->2211|3978->2258|3993->2264|4042->2292|4071->2294|4085->2299|4106->2311|4130->2314|4200->2357|4243->2391|4283->2393|4348->2430|4410->2474|4423->2479|4462->2480|4527->2517|4559->2522|4609->2563|4685->2617|4755->2656|4816->2690|4859->2724|4899->2726|4964->2763|5026->2807|5039->2812|5078->2813|5143->2850|5175->2855|5225->2896|5301->2950|5371->2989|5432->3023|5475->3057|5515->3059|5580->3096|5642->3140|5655->3145|5694->3146|5759->3183|5791->3188|5841->3229|5917->3283|5987->3322|6048->3356|6091->3390|6131->3392|6196->3429|6258->3473|6271->3478|6310->3479|6375->3516|6407->3521|6457->3562|6533->3616|6603->3655|6664->3689|6707->3723|6747->3725|6812->3762|6874->3806|6887->3811|6926->3812|6991->3849|7023->3854|7073->3895|7149->3949|7219->3988|7280->4022|7323->4056|7363->4058|7428->4095|7490->4139|7503->4144|7542->4145|7607->4182|7639->4187|7689->4228|7765->4282|7835->4321|7892->4350|7954->4381|8007->4406|8094->4462|8139->4479|8229->4542|8243->4547|8285->4568|8330->4585
                  LINES: 24->1|29->3|34->3|37->6|37->6|37->6|39->15|40->16|46->22|46->22|47->23|47->23|47->23|58->34|58->34|58->34|59->35|71->47|71->47|71->47|72->48|73->49|73->49|73->49|74->50|74->50|74->50|74->50|75->51|75->51|75->51|76->52|76->52|76->52|76->52|77->53|78->54|79->55|79->55|79->55|79->55|79->55|79->55|79->55|80->56|80->56|80->56|81->57|82->58|82->58|82->58|83->59|83->59|83->59|83->59|84->60|85->61|85->61|85->61|86->62|87->63|87->63|87->63|88->64|88->64|88->64|88->64|89->65|90->66|90->66|90->66|91->67|92->68|92->68|92->68|93->69|93->69|93->69|93->69|94->70|95->71|95->71|95->71|96->72|97->73|97->73|97->73|98->74|98->74|98->74|98->74|99->75|100->76|100->76|100->76|101->77|102->78|102->78|102->78|103->79|103->79|103->79|103->79|104->80|105->81|105->81|105->81|106->82|107->83|107->83|107->83|108->84|108->84|108->84|108->84|109->85|110->86|111->87|112->88|115->91|116->92|118->94|118->94|118->94|119->95
                  -- GENERATED --
              */
          