
package views.html.search

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._
/*1.2*/import sparql._

object searchIdResult extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template3[Int,String,String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*3.2*/(count: Int, glycobaseId: String, uoxf: String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*3.49*/("""


"""),_display_(/*6.2*/main/*6.6*/ {_display_(Seq[Any](format.raw/*6.8*/("""

    """),format.raw/*8.5*/("""<div class="section">
        <div class="container" >
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header text-primary">
                        <h1 class="info-header-grey">"""),_display_(/*13.55*/count),format.raw/*13.60*/(""" """),format.raw/*13.61*/("""glycan structures found for GlycoBase id: """),_display_(/*13.104*/glycobaseId),format.raw/*13.115*/("""</h1><br />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-9">
                """),_display_(/*19.18*/if(count > 0)/*19.31*/ {_display_(Seq[Any](format.raw/*19.33*/("""
                    """),format.raw/*20.21*/("""<table class="table table-striped">
                        <thead><tr>
                            <th>Glycan</th>
                            <th>HPLC</th>
                            <th>UPLC</th>
                            <th>RPUPLC</th>
                            <th>CE</th>
                        </tr></thead>
                        <tbody>
                            <tr>
                                """),_display_(/*30.34*/if(session.get("notation"))/*30.61*/ {_display_(Seq[Any](format.raw/*30.63*/("""
                                    """),format.raw/*31.37*/("""<td><img src=""""),_display_(/*31.52*/routes/*31.58*/.Assets.versioned("images/sm-" + session.get("notation") + "/sm-" + session.get("notation") + "-" + glycobaseId + ".png")),format.raw/*31.179*/(""""><br/>
                                """)))}/*32.35*/else/*32.40*/{_display_(Seq[Any](format.raw/*32.41*/("""
                                    """),format.raw/*33.37*/("""<td><img src=""""),_display_(/*33.52*/routes/*33.58*/.Assets.versioned("images/sm-cfg-uoxf/sm-cfg-uoxf-" + glycobaseId + ".png")),format.raw/*33.133*/("""">
                                        """)))}),format.raw/*34.42*/("""
                                """),format.raw/*35.33*/("""<a href="""),_display_(/*35.42*/routes/*35.48*/.Application.showGlycan(glycobaseId)),format.raw/*35.84*/(""">"""),_display_(/*35.86*/uoxf),format.raw/*35.90*/("""</a></td>
                                """),_display_(/*36.34*/if(controllers.Application.avgHp(glycobaseId))/*36.80*/ {_display_(Seq[Any](format.raw/*36.82*/("""
                                    """),format.raw/*37.37*/("""<td>"""),_display_(/*37.42*/BigDecimal(controllers.Application.avgHp(glycobaseId))/*37.96*/.setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble),format.raw/*37.150*/("""</td>
                                """)))}/*38.35*/else/*38.40*/{_display_(Seq[Any](format.raw/*38.41*/("""
                                    """),format.raw/*39.37*/("""<td></td>
                                """)))}),format.raw/*40.34*/("""
                                """),_display_(/*41.34*/if(controllers.Application.avg2ABUp(glycobaseId))/*41.83*/ {_display_(Seq[Any](format.raw/*41.85*/("""
                                    """),format.raw/*42.37*/("""<td>"""),_display_(/*42.42*/BigDecimal(controllers.Application.avg2ABUp(glycobaseId))/*42.99*/.setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble),format.raw/*42.153*/("""</td>
                                """)))}/*43.35*/else/*43.40*/{_display_(Seq[Any](format.raw/*43.41*/("""
                                    """),format.raw/*44.37*/("""<td></td>
                                """)))}),format.raw/*45.34*/("""
                                """),_display_(/*46.34*/if(controllers.Application.avgRp(glycobaseId))/*46.80*/ {_display_(Seq[Any](format.raw/*46.82*/("""
                                    """),format.raw/*47.37*/("""<td>"""),_display_(/*47.42*/BigDecimal(controllers.Application.avgRp(glycobaseId))/*47.96*/.setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble),format.raw/*47.150*/("""</td>
                                """)))}/*48.35*/else/*48.40*/{_display_(Seq[Any](format.raw/*48.41*/("""
                                    """),format.raw/*49.37*/("""<td></td>
                                """)))}),format.raw/*50.34*/("""
                                """),_display_(/*51.34*/if(controllers.Application.avgCe(glycobaseId))/*51.80*/ {_display_(Seq[Any](format.raw/*51.82*/("""
                                    """),format.raw/*52.37*/("""<td>"""),_display_(/*52.42*/BigDecimal(controllers.Application.avgCe(glycobaseId))/*52.96*/.setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble),format.raw/*52.150*/("""</td>
                                """)))}/*53.35*/else/*53.40*/{_display_(Seq[Any](format.raw/*53.41*/("""
                                    """),format.raw/*54.37*/("""<td></td>
                                """)))}),format.raw/*55.34*/("""
                            """),format.raw/*56.29*/("""</tr>
                        </tbody>
                    </table>
                """)))}/*59.19*/else/*59.24*/{_display_(Seq[Any](format.raw/*59.25*/("""
                    """),format.raw/*60.21*/("""<h1 >No structures found for GlycoBase id: """),_display_(/*60.65*/glycobaseId),format.raw/*60.76*/("""</h1>
                """)))}),format.raw/*61.18*/("""
                """),format.raw/*62.17*/("""</div>
                <div class="col-md-3">
                """),_display_(/*64.18*/views/*64.23*/.html.format.format()),format.raw/*64.44*/("""
                """),format.raw/*65.17*/("""</div>
            </div>
        </div>
""")))}))
      }
    }
  }

  def render(count:Int,glycobaseId:String,uoxf:String): play.twirl.api.HtmlFormat.Appendable = apply(count,glycobaseId,uoxf)

  def f:((Int,String,String) => play.twirl.api.HtmlFormat.Appendable) = (count,glycobaseId,uoxf) => apply(count,glycobaseId,uoxf)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Mon Feb 26 14:39:20 AEST 2018
                  SOURCE: /Users/s2980369/IdeaProjects/glycobase/glycobase/app/views/search/searchIdResult.scala.html
                  HASH: 8935b1a31e13170151437a476bbe5539b2c91ca2
                  MATRIX: 658->1|998->19|1140->66|1169->70|1180->74|1218->76|1250->82|1515->320|1541->325|1570->326|1641->369|1674->380|1868->547|1890->560|1930->562|1979->583|2426->1003|2462->1030|2502->1032|2567->1069|2609->1084|2624->1090|2767->1211|2827->1253|2840->1258|2879->1259|2944->1296|2986->1311|3001->1317|3098->1392|3173->1436|3234->1469|3270->1478|3285->1484|3342->1520|3371->1522|3396->1526|3466->1569|3521->1615|3561->1617|3626->1654|3658->1659|3721->1713|3797->1767|3855->1807|3868->1812|3907->1813|3972->1850|4046->1893|4107->1927|4165->1976|4205->1978|4270->2015|4302->2020|4368->2077|4444->2131|4502->2171|4515->2176|4554->2177|4619->2214|4693->2257|4754->2291|4809->2337|4849->2339|4914->2376|4946->2381|5009->2435|5085->2489|5143->2529|5156->2534|5195->2535|5260->2572|5334->2615|5395->2649|5450->2695|5490->2697|5555->2734|5587->2739|5650->2793|5726->2847|5784->2887|5797->2892|5836->2893|5901->2930|5975->2973|6032->3002|6136->3088|6149->3093|6188->3094|6237->3115|6308->3159|6340->3170|6394->3193|6439->3210|6529->3273|6543->3278|6585->3299|6630->3316
                  LINES: 24->1|29->3|34->3|37->6|37->6|37->6|39->8|44->13|44->13|44->13|44->13|44->13|50->19|50->19|50->19|51->20|61->30|61->30|61->30|62->31|62->31|62->31|62->31|63->32|63->32|63->32|64->33|64->33|64->33|64->33|65->34|66->35|66->35|66->35|66->35|66->35|66->35|67->36|67->36|67->36|68->37|68->37|68->37|68->37|69->38|69->38|69->38|70->39|71->40|72->41|72->41|72->41|73->42|73->42|73->42|73->42|74->43|74->43|74->43|75->44|76->45|77->46|77->46|77->46|78->47|78->47|78->47|78->47|79->48|79->48|79->48|80->49|81->50|82->51|82->51|82->51|83->52|83->52|83->52|83->52|84->53|84->53|84->53|85->54|86->55|87->56|90->59|90->59|90->59|91->60|91->60|91->60|92->61|93->62|95->64|95->64|95->64|96->65
                  -- GENERATED --
              */
          