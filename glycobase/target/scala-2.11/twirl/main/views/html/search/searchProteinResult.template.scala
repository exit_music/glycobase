
package views.html.search

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._
/*1.2*/import sparql._

object searchProteinResult extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template3[Int,String,Map[String, String],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*3.2*/(count: Int, proteinName: String, result: Map[String, String]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*3.64*/("""

"""),_display_(/*5.2*/main/*5.6*/ {_display_(Seq[Any](format.raw/*5.8*/("""
    """),format.raw/*13.13*/("""
    """),format.raw/*14.5*/("""<div class="section">
        <div class="container" >
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header">
                        <div class="pull-left">
                            <h1 class="info-header-grey text-primary">Glycoprotein Selection - """),_display_(/*20.97*/proteinName),format.raw/*20.108*/("""</h1>
                            <h4 class="text-primary">"""),_display_(/*21.55*/count),format.raw/*21.60*/(""" """),format.raw/*21.61*/("""glycan structures</h4>
                        </div>
                        <div class="pull-right">
                            <div class="text-right brand"></div>
                        </div>
                        <div class="clearfix"></div>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-9">
                """),_display_(/*33.18*/if(count > 0)/*33.31*/ {_display_(Seq[Any](format.raw/*33.33*/("""
                    """),format.raw/*34.21*/("""<table class="table table-striped">
                        <thead><tr>
                            <th>Glycan</th>
                            <th>Name</th>
                            <th>Mono Mass</th>
                            <th>HPLC</th>
                            <th>UPLC (2-AB)</th>
                            <th>UPLC (Proc)</th>
                            <th>RPLC</th>
                            <th>CE</th>
                            <th>PGC</th>
                        </tr></thead>
                        <tbody>
                        """),_display_(/*47.26*/for((key, value) <- result) yield /*47.53*/ {_display_(Seq[Any](format.raw/*47.55*/("""
                            """),format.raw/*48.29*/("""<tr>
                                """),_display_(/*49.34*/if(session.get("notation"))/*49.61*/ {_display_(Seq[Any](format.raw/*49.63*/("""
                                    """),format.raw/*50.37*/("""<td><img src=""""),_display_(/*50.52*/routes/*50.58*/.Assets.versioned("images/sm-" + session.get("notation") + "/sm-" + session.get("notation") + "-" + key + ".png")),format.raw/*50.171*/(""""><br/>
                                """)))}/*51.35*/else/*51.40*/{_display_(Seq[Any](format.raw/*51.41*/("""
                                    """),format.raw/*52.37*/("""<td><img src=""""),_display_(/*52.52*/routes/*52.58*/.Assets.versioned("images/sm-cfg-uoxf/sm-cfg-uoxf-" + key + ".png")),format.raw/*52.125*/("""">
                                    """)))}),format.raw/*53.38*/("""
                            """),format.raw/*54.29*/("""</td>
                            <td><a href="""),_display_(/*55.42*/routes/*55.48*/.Application.showGlycan(key)),format.raw/*55.76*/(""">"""),_display_(/*55.78*/value/*55.83*/.split("\t")/*55.95*/(1)),format.raw/*55.98*/("""</a></td>
                                """),_display_(/*56.34*/if(value.split("\t")(5) == "null")/*56.68*/ {_display_(Seq[Any](format.raw/*56.70*/("""
                                    """),format.raw/*57.37*/("""<td></td>
                                """)))}/*58.35*/else/*58.40*/{_display_(Seq[Any](format.raw/*58.41*/("""
                                    """),format.raw/*59.37*/("""<td>"""),_display_(/*59.42*/BigDecimal(value.split("\t")(5).toDouble)/*59.83*/.setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble),format.raw/*59.137*/("""</td>
                                """)))}),format.raw/*60.34*/("""
                                """),_display_(/*61.34*/if(value.split("\t")(2) == "null")/*61.68*/ {_display_(Seq[Any](format.raw/*61.70*/("""
                                    """),format.raw/*62.37*/("""<td></td>
                                """)))}/*63.35*/else/*63.40*/{_display_(Seq[Any](format.raw/*63.41*/("""
                                    """),format.raw/*64.37*/("""<td>"""),_display_(/*64.42*/BigDecimal(value.split("\t")(2).toDouble)/*64.83*/.setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble),format.raw/*64.137*/("""</td>
                                """)))}),format.raw/*65.34*/("""
                                """),_display_(/*66.34*/if(value.split("\t")(4) == "null")/*66.68*/ {_display_(Seq[Any](format.raw/*66.70*/("""
                                    """),format.raw/*67.37*/("""<td></td>
                                """)))}/*68.35*/else/*68.40*/{_display_(Seq[Any](format.raw/*68.41*/("""
                                    """),format.raw/*69.37*/("""<td>"""),_display_(/*69.42*/BigDecimal(value.split("\t")(4).toDouble)/*69.83*/.setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble),format.raw/*69.137*/("""</td>
                                """)))}),format.raw/*70.34*/("""
                                """),_display_(/*71.34*/if(value.split("\t")(6) == "null")/*71.68*/ {_display_(Seq[Any](format.raw/*71.70*/("""
                                    """),format.raw/*72.37*/("""<td></td>
                                """)))}/*73.35*/else/*73.40*/{_display_(Seq[Any](format.raw/*73.41*/("""
                                    """),format.raw/*74.37*/("""<td>"""),_display_(/*74.42*/BigDecimal(value.split("\t")(6).toDouble)/*74.83*/.setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble),format.raw/*74.137*/("""</td>
                                """)))}),format.raw/*75.34*/("""
                                """),_display_(/*76.34*/if(value.split("\t")(3) == "null")/*76.68*/ {_display_(Seq[Any](format.raw/*76.70*/("""
                                    """),format.raw/*77.37*/("""<td></td>
                                """)))}/*78.35*/else/*78.40*/{_display_(Seq[Any](format.raw/*78.41*/("""
                                    """),format.raw/*79.37*/("""<td>"""),_display_(/*79.42*/BigDecimal(value.split("\t")(3).toDouble)/*79.83*/.setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble),format.raw/*79.137*/("""</td>
                                """)))}),format.raw/*80.34*/("""
                                """),_display_(/*81.34*/if(value.split("\t")(0) == "null")/*81.68*/ {_display_(Seq[Any](format.raw/*81.70*/("""
                                    """),format.raw/*82.37*/("""<td></td>
                                """)))}/*83.35*/else/*83.40*/{_display_(Seq[Any](format.raw/*83.41*/("""
                                    """),format.raw/*84.37*/("""<td>"""),_display_(/*84.42*/BigDecimal(value.split("\t")(0).toDouble)/*84.83*/.setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble),format.raw/*84.137*/("""</td>
                                """)))}),format.raw/*85.34*/("""
                                """),_display_(/*86.34*/if(value.split("\t")(7) == "null")/*86.68*/ {_display_(Seq[Any](format.raw/*86.70*/("""
                                    """),format.raw/*87.37*/("""<td></td>
                                """)))}/*88.35*/else/*88.40*/{_display_(Seq[Any](format.raw/*88.41*/("""
                                    """),format.raw/*89.37*/("""<td>"""),_display_(/*89.42*/BigDecimal(value.split("\t")(7).toDouble)/*89.83*/.setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble),format.raw/*89.137*/("""</td>
                                """)))}),format.raw/*90.34*/("""

                            """),format.raw/*92.29*/("""</tr>
                        """)))}),format.raw/*93.26*/("""
                        """),format.raw/*94.25*/("""</tbody>
                    </table>
                """)))}),format.raw/*96.18*/("""
                """),format.raw/*97.17*/("""</div>
                <div class="col-md-3">
                """),_display_(/*99.18*/views/*99.23*/.html.format.format()),format.raw/*99.44*/("""
                """),format.raw/*100.17*/("""</div>
            </div>
        </div>
    </div>
""")))}))
      }
    }
  }

  def render(count:Int,proteinName:String,result:Map[String, String]): play.twirl.api.HtmlFormat.Appendable = apply(count,proteinName,result)

  def f:((Int,String,Map[String, String]) => play.twirl.api.HtmlFormat.Appendable) = (count,proteinName,result) => apply(count,proteinName,result)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Mon Feb 26 14:39:20 AEST 2018
                  SOURCE: /Users/s2980369/IdeaProjects/glycobase/glycobase/app/views/search/searchProteinResult.scala.html
                  HASH: 19f9e3c25a2f74859ebea260266024318c75cea7
                  MATRIX: 658->1|1016->19|1173->81|1201->84|1212->88|1250->90|1283->329|1315->334|1657->649|1690->660|1777->720|1803->725|1832->726|2267->1134|2289->1147|2329->1149|2378->1170|2968->1733|3011->1760|3051->1762|3108->1791|3173->1829|3209->1856|3249->1858|3314->1895|3356->1910|3371->1916|3506->2029|3566->2071|3579->2076|3618->2077|3683->2114|3725->2129|3740->2135|3829->2202|3900->2242|3957->2271|4031->2318|4046->2324|4095->2352|4124->2354|4138->2359|4159->2371|4183->2374|4253->2417|4296->2451|4336->2453|4401->2490|4463->2534|4476->2539|4515->2540|4580->2577|4612->2582|4662->2623|4738->2677|4808->2716|4869->2750|4912->2784|4952->2786|5017->2823|5079->2867|5092->2872|5131->2873|5196->2910|5228->2915|5278->2956|5354->3010|5424->3049|5485->3083|5528->3117|5568->3119|5633->3156|5695->3200|5708->3205|5747->3206|5812->3243|5844->3248|5894->3289|5970->3343|6040->3382|6101->3416|6144->3450|6184->3452|6249->3489|6311->3533|6324->3538|6363->3539|6428->3576|6460->3581|6510->3622|6586->3676|6656->3715|6717->3749|6760->3783|6800->3785|6865->3822|6927->3866|6940->3871|6979->3872|7044->3909|7076->3914|7126->3955|7202->4009|7272->4048|7333->4082|7376->4116|7416->4118|7481->4155|7543->4199|7556->4204|7595->4205|7660->4242|7692->4247|7742->4288|7818->4342|7888->4381|7949->4415|7992->4449|8032->4451|8097->4488|8159->4532|8172->4537|8211->4538|8276->4575|8308->4580|8358->4621|8434->4675|8504->4714|8562->4744|8624->4775|8677->4800|8763->4855|8808->4872|8898->4935|8912->4940|8954->4961|9000->4978
                  LINES: 24->1|29->3|34->3|36->5|36->5|36->5|37->13|38->14|44->20|44->20|45->21|45->21|45->21|57->33|57->33|57->33|58->34|71->47|71->47|71->47|72->48|73->49|73->49|73->49|74->50|74->50|74->50|74->50|75->51|75->51|75->51|76->52|76->52|76->52|76->52|77->53|78->54|79->55|79->55|79->55|79->55|79->55|79->55|79->55|80->56|80->56|80->56|81->57|82->58|82->58|82->58|83->59|83->59|83->59|83->59|84->60|85->61|85->61|85->61|86->62|87->63|87->63|87->63|88->64|88->64|88->64|88->64|89->65|90->66|90->66|90->66|91->67|92->68|92->68|92->68|93->69|93->69|93->69|93->69|94->70|95->71|95->71|95->71|96->72|97->73|97->73|97->73|98->74|98->74|98->74|98->74|99->75|100->76|100->76|100->76|101->77|102->78|102->78|102->78|103->79|103->79|103->79|103->79|104->80|105->81|105->81|105->81|106->82|107->83|107->83|107->83|108->84|108->84|108->84|108->84|109->85|110->86|110->86|110->86|111->87|112->88|112->88|112->88|113->89|113->89|113->89|113->89|114->90|116->92|117->93|118->94|120->96|121->97|123->99|123->99|123->99|124->100
                  -- GENERATED --
              */
          