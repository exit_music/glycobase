
package views.html.search

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._
/*1.2*/import sparql._

object searchPaperResult extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template5[Int,String,List[SparqlEntity],List[SparqlEntity],List[SparqlEntity],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*3.2*/(count: Int, paperTitle: String, x: List[SparqlEntity], paper: List[SparqlEntity], source: List[SparqlEntity]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*3.112*/("""


    """),_display_(/*6.6*/main/*6.10*/ {_display_(Seq[Any](format.raw/*6.12*/("""
        """),format.raw/*14.17*/("""
        """),format.raw/*15.9*/("""<div class="section">
            <div class="container" >
                <div class="row">
                    <div class="col-md-12">
                        <div class="page-header">
                            <div class="pull-left">
                                <h1 class="info-header-grey text-primary">"""),_display_(/*21.76*/paperTitle),format.raw/*21.86*/("""</h1>
                                <div class="minitext">
                                    """),_display_(/*23.38*/paper/*23.43*/.get(0).getValue("Author")),format.raw/*23.69*/("""
                                    """),format.raw/*24.37*/("""- """),_display_(/*24.40*/paper/*24.45*/.get(0).getValue("Journal")),format.raw/*24.72*/(""", """),_display_(/*24.75*/paper/*24.80*/.get(0).getValue("Volume")),format.raw/*24.106*/(""", """),_display_(/*24.109*/paper/*24.114*/.get(0).getValue("PublishYear")),format.raw/*24.145*/("""
                                """),format.raw/*25.33*/("""</div>
                        </div>
                        <div class="pull-right">
                            <div class="text-right brand"></div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-9">
                    <table class="table table-striped">
                        <thead><tr>
                            <th>Structure</th>
                            <th>Name</th>
                            <th>Label</th>
                            <th>Average GU</th>
                        </tr></thead>
                        <tbody>
                        """),_display_(/*44.26*/for(entry <- x) yield /*44.41*/ {_display_(Seq[Any](format.raw/*44.43*/("""
                            """),format.raw/*45.29*/("""<tr>
                                """),_display_(/*46.34*/if(session.get("notation"))/*46.61*/ {_display_(Seq[Any](format.raw/*46.63*/("""
                                    """),format.raw/*47.37*/("""<td><a href="""),_display_(/*47.50*/routes/*47.56*/.Application.showGlycan(entry.getValue("GlycoBaseId"))),format.raw/*47.110*/("""><img src=""""),_display_(/*47.122*/routes/*47.128*/.Assets.versioned("images/sm-" + session.get("notation") + "/sm-" + session.get("notation") + "-" + entry.getValue("GlycoBaseId") + ".png")),format.raw/*47.267*/(""""></a><br/>
                                """)))}/*48.35*/else/*48.40*/{_display_(Seq[Any](format.raw/*48.41*/("""
                                    """),format.raw/*49.37*/("""<td><a href="""),_display_(/*49.50*/routes/*49.56*/.Application.showGlycan(entry.getValue("GlycoBaseId"))),format.raw/*49.110*/("""><img src=""""),_display_(/*49.122*/routes/*49.128*/.Assets.versioned("images/sm-cfg-uoxf/sm-cfg-uoxf" + entry.getValue("GlycoBaseId") + ".png")),format.raw/*49.220*/(""""></a>
                                """)))}),format.raw/*50.34*/("""
                            """),format.raw/*51.29*/("""<td><a href="""),_display_(/*51.42*/routes/*51.48*/.Application.showGlycan(entry.getValue("GlycoBaseId"))),format.raw/*51.102*/(""">"""),_display_(/*51.104*/entry/*51.109*/.getData.get("Uoxf")),format.raw/*51.129*/("""</a></td>
                            <td>"""),_display_(/*52.34*/entry/*52.39*/.getValue("LabelType")),format.raw/*52.61*/("""</td>
                            <td>"""),_display_(/*53.34*/BigDecimal(entry.getValue("avgGu").toDouble)/*53.78*/.setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble),format.raw/*53.132*/("""</td>


                            </tr>

                        """)))}),format.raw/*58.26*/("""
                        """),format.raw/*59.25*/("""</tbody>
                    </table>
                </div>
                <div class="col-md-3 sidebar">
                    """),_display_(/*63.22*/views/*63.27*/.html.format.format()),format.raw/*63.48*/("""
                    """),format.raw/*64.21*/("""<h3 class="fh-desc" >Information</h3>

                    """),_display_(/*66.22*/for(s <- source) yield /*66.38*/ {_display_(Seq[Any](format.raw/*66.40*/("""
                        """),_display_(/*67.26*/if(s.getValue("ReportName") != null)/*67.62*/ {_display_(Seq[Any](format.raw/*67.64*/("""
                            """),format.raw/*68.29*/("""<a href="/searchReport/"""),_display_(/*68.53*/s/*68.54*/.getValue("ReportName")),format.raw/*68.77*/(""""><span class="badge">
                                Report: """),_display_(/*69.42*/s/*69.43*/.getValue("ReportName")),format.raw/*69.66*/("""</span></a>
                        """)))}),format.raw/*70.26*/("""
                        """),_display_(/*71.26*/if(s.getValue("SampleName") != null)/*71.62*/ {_display_(Seq[Any](format.raw/*71.64*/("""
                            """),format.raw/*72.29*/("""<a href="/searchSample/"""),_display_(/*72.53*/s/*72.54*/.getValue("SampleName")),format.raw/*72.77*/(""""><span class="badge">
                                Sample: """),_display_(/*73.42*/s/*73.43*/.getValue("SampleName")),format.raw/*73.66*/("""</span></a>
                        """)))}),format.raw/*74.26*/("""
                        """),_display_(/*75.26*/if(s.getValue("Taxon") != null)/*75.57*/ {_display_(Seq[Any](format.raw/*75.59*/("""
                            """),format.raw/*76.29*/("""<a href="/searchTaxon/"""),_display_(/*76.52*/s/*76.53*/.getValue("Taxon")),format.raw/*76.71*/(""""><span class="badge">
                                Taxon: """),_display_(/*77.41*/s/*77.42*/.getValue("Taxon")),format.raw/*77.60*/("""</span></a>
                        """)))}),format.raw/*78.26*/("""
                        """),_display_(/*79.26*/if(s.getValue("Tissue") != null)/*79.58*/ {_display_(Seq[Any](format.raw/*79.60*/("""
                            """),format.raw/*80.29*/("""<a href="/searchTissue/"""),_display_(/*80.53*/s/*80.54*/.getValue("Tissue")),format.raw/*80.73*/(""""><span class="badge">
                                Tissue: """),_display_(/*81.42*/s/*81.43*/.getValue("Tissue")),format.raw/*81.62*/("""</span></a>
                        """)))}),format.raw/*82.26*/("""
                    """)))}),format.raw/*83.22*/("""
                """),format.raw/*84.17*/("""</div>
            </div>
        </div>
        </div>
    """)))}))
      }
    }
  }

  def render(count:Int,paperTitle:String,x:List[SparqlEntity],paper:List[SparqlEntity],source:List[SparqlEntity]): play.twirl.api.HtmlFormat.Appendable = apply(count,paperTitle,x,paper,source)

  def f:((Int,String,List[SparqlEntity],List[SparqlEntity],List[SparqlEntity]) => play.twirl.api.HtmlFormat.Appendable) = (count,paperTitle,x,paper,source) => apply(count,paperTitle,x,paper,source)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Mon Feb 26 14:39:20 AEST 2018
                  SOURCE: /Users/s2980369/IdeaProjects/glycobase/glycobase/app/views/search/searchPaperResult.scala.html
                  HASH: d8e8f9e667d2faf1e2f23ff8ce97bbb041e0cb8d
                  MATRIX: 658->1|1051->19|1257->129|1290->137|1302->141|1341->143|1378->487|1414->496|1755->810|1786->820|1911->918|1925->923|1972->949|2037->986|2067->989|2081->994|2129->1021|2159->1024|2173->1029|2221->1055|2252->1058|2267->1063|2320->1094|2381->1127|3149->1868|3180->1883|3220->1885|3277->1914|3342->1952|3378->1979|3418->1981|3483->2018|3523->2031|3538->2037|3614->2091|3654->2103|3670->2109|3831->2248|3895->2294|3908->2299|3947->2300|4012->2337|4052->2350|4067->2356|4143->2410|4183->2422|4199->2428|4313->2520|4384->2560|4441->2589|4481->2602|4496->2608|4572->2662|4602->2664|4617->2669|4659->2689|4729->2732|4743->2737|4786->2759|4852->2798|4905->2842|4981->2896|5080->2964|5133->2989|5289->3118|5303->3123|5345->3144|5394->3165|5481->3225|5513->3241|5553->3243|5606->3269|5651->3305|5691->3307|5748->3336|5799->3360|5809->3361|5853->3384|5944->3448|5954->3449|5998->3472|6066->3509|6119->3535|6164->3571|6204->3573|6261->3602|6312->3626|6322->3627|6366->3650|6457->3714|6467->3715|6511->3738|6579->3775|6632->3801|6672->3832|6712->3834|6769->3863|6819->3886|6829->3887|6868->3905|6958->3968|6968->3969|7007->3987|7075->4024|7128->4050|7169->4082|7209->4084|7266->4113|7317->4137|7327->4138|7367->4157|7458->4221|7468->4222|7508->4241|7576->4278|7629->4300|7674->4317
                  LINES: 24->1|29->3|34->3|37->6|37->6|37->6|38->14|39->15|45->21|45->21|47->23|47->23|47->23|48->24|48->24|48->24|48->24|48->24|48->24|48->24|48->24|48->24|48->24|49->25|68->44|68->44|68->44|69->45|70->46|70->46|70->46|71->47|71->47|71->47|71->47|71->47|71->47|71->47|72->48|72->48|72->48|73->49|73->49|73->49|73->49|73->49|73->49|73->49|74->50|75->51|75->51|75->51|75->51|75->51|75->51|75->51|76->52|76->52|76->52|77->53|77->53|77->53|82->58|83->59|87->63|87->63|87->63|88->64|90->66|90->66|90->66|91->67|91->67|91->67|92->68|92->68|92->68|92->68|93->69|93->69|93->69|94->70|95->71|95->71|95->71|96->72|96->72|96->72|96->72|97->73|97->73|97->73|98->74|99->75|99->75|99->75|100->76|100->76|100->76|100->76|101->77|101->77|101->77|102->78|103->79|103->79|103->79|104->80|104->80|104->80|104->80|105->81|105->81|105->81|106->82|107->83|108->84
                  -- GENERATED --
              */
          