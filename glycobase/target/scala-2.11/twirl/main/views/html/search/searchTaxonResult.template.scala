
package views.html.search

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._
/*1.2*/import sparql._

object searchTaxonResult extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template4[Int,String,Map[String, String],List[String],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*3.2*/(count: Int, taxonName: String, result: Map[String, String], taxon:List[String]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*3.82*/("""


"""),_display_(/*6.2*/main/*6.6*/ {_display_(Seq[Any](format.raw/*6.8*/("""
    """),format.raw/*14.13*/("""
    """),format.raw/*15.5*/("""<div class="section">
        <div class="container" >
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header">
                        <div class="pull-left">
                            <h1 class="info-header-grey text-primary">Taxonomy Results - """),_display_(/*21.91*/taxonName),format.raw/*21.100*/("""</h1>
                            <div class="minitext">"""),_display_(/*22.52*/count),format.raw/*22.57*/(""" """),format.raw/*22.58*/("""glycan structures</div>
                        </div>
                        <div class="pull-right">
                            <div class="text-right brand"></div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-9">
                """),_display_(/*33.18*/if(count > 0)/*33.31*/ {_display_(Seq[Any](format.raw/*33.33*/("""
                    """),format.raw/*34.21*/("""<table class="table table-striped">
                        <thead><tr>
                            <th>Glycan</th>
                            <th>Name</th>
                            <th>Mono Mass</th>
                            <th>HPLC</th>
                            <th>UPLC (2-AB)</th>
                            <th>UPLC (Proc)</th>
                            <th>RPUPLC</th>
                            <th>CE</th>
                        </tr></thead>
                        <tbody>
                        """),_display_(/*46.26*/for((key, value) <- result) yield /*46.53*/ {_display_(Seq[Any](format.raw/*46.55*/("""
                            """),format.raw/*47.29*/("""<tr>
                                """),_display_(/*48.34*/if(session.get("notation"))/*48.61*/ {_display_(Seq[Any](format.raw/*48.63*/("""
                                    """),format.raw/*49.37*/("""<td><img src=""""),_display_(/*49.52*/routes/*49.58*/.Assets.versioned("images/sm-" + session.get("notation") + "/sm-" + session.get("notation") + "-" + key + ".png")),format.raw/*49.171*/(""""><br/>
                                """)))}/*50.35*/else/*50.40*/{_display_(Seq[Any](format.raw/*50.41*/("""
                                    """),format.raw/*51.37*/("""<td><img src=""""),_display_(/*51.52*/routes/*51.58*/.Assets.versioned("images/sm-cfg-uoxf/sm-cfg-uoxf-" + key + ".png")),format.raw/*51.125*/("""">
                                    """)))}),format.raw/*52.38*/("""
                            """),format.raw/*53.29*/("""</td>
                            <td><a href="""),_display_(/*54.42*/routes/*54.48*/.Application.showGlycan(key)),format.raw/*54.76*/(""">"""),_display_(/*54.78*/value/*54.83*/.split("\t")/*54.95*/(1)),format.raw/*54.98*/("""</a></td>
                                """),_display_(/*55.34*/if(value.split("\t")(5) == "null")/*55.68*/ {_display_(Seq[Any](format.raw/*55.70*/("""
                                    """),format.raw/*56.37*/("""<td></td>
                                """)))}/*57.35*/else/*57.40*/{_display_(Seq[Any](format.raw/*57.41*/("""
                                    """),format.raw/*58.37*/("""<td>"""),_display_(/*58.42*/BigDecimal(value.split("\t")(5).toDouble)/*58.83*/.setScale(4, BigDecimal.RoundingMode.HALF_UP).toDouble),format.raw/*58.137*/("""</td>
                                """)))}),format.raw/*59.34*/("""
                                """),_display_(/*60.34*/if(value.split("\t")(2) == "null")/*60.68*/ {_display_(Seq[Any](format.raw/*60.70*/("""
                                    """),format.raw/*61.37*/("""<td></td>
                                """)))}/*62.35*/else/*62.40*/{_display_(Seq[Any](format.raw/*62.41*/("""
                                    """),format.raw/*63.37*/("""<td>"""),_display_(/*63.42*/BigDecimal(value.split("\t")(2).toDouble)/*63.83*/.setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble),format.raw/*63.137*/("""</td>
                                """)))}),format.raw/*64.34*/("""
                                """),_display_(/*65.34*/if(value.split("\t")(4) == "null")/*65.68*/ {_display_(Seq[Any](format.raw/*65.70*/("""
                                    """),format.raw/*66.37*/("""<td></td>
                                """)))}/*67.35*/else/*67.40*/{_display_(Seq[Any](format.raw/*67.41*/("""
                                    """),format.raw/*68.37*/("""<td>"""),_display_(/*68.42*/BigDecimal(value.split("\t")(4).toDouble)/*68.83*/.setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble),format.raw/*68.137*/("""</td>
                                """)))}),format.raw/*69.34*/("""
                                """),_display_(/*70.34*/if(value.split("\t")(6) == "null")/*70.68*/ {_display_(Seq[Any](format.raw/*70.70*/("""
                                    """),format.raw/*71.37*/("""<td></td>
                                """)))}/*72.35*/else/*72.40*/{_display_(Seq[Any](format.raw/*72.41*/("""
                                    """),format.raw/*73.37*/("""<td>"""),_display_(/*73.42*/BigDecimal(value.split("\t")(6).toDouble)/*73.83*/.setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble),format.raw/*73.137*/("""</td>
                                """)))}),format.raw/*74.34*/("""
                                """),_display_(/*75.34*/if(value.split("\t")(3) == "null")/*75.68*/ {_display_(Seq[Any](format.raw/*75.70*/("""
                                    """),format.raw/*76.37*/("""<td></td>
                                """)))}/*77.35*/else/*77.40*/{_display_(Seq[Any](format.raw/*77.41*/("""
                                    """),format.raw/*78.37*/("""<td>"""),_display_(/*78.42*/BigDecimal(value.split("\t")(3).toDouble)/*78.83*/.setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble),format.raw/*78.137*/("""</td>
                                """)))}),format.raw/*79.34*/("""
                                """),_display_(/*80.34*/if(value.split("\t")(0) == "null")/*80.68*/ {_display_(Seq[Any](format.raw/*80.70*/("""
                                    """),format.raw/*81.37*/("""<td></td>
                                """)))}/*82.35*/else/*82.40*/{_display_(Seq[Any](format.raw/*82.41*/("""
                                    """),format.raw/*83.37*/("""<td>"""),_display_(/*83.42*/BigDecimal(value.split("\t")(0).toDouble)/*83.83*/.setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble),format.raw/*83.137*/("""</td>
                                """)))}),format.raw/*84.34*/("""
                            """),format.raw/*85.29*/("""</tr>
                        """)))}),format.raw/*86.26*/("""
                        """),format.raw/*87.25*/("""</tbody>
                    </table>
                """)))}),format.raw/*89.18*/("""
                """),format.raw/*90.17*/("""</div>
                <div class="col-md-3">
                    """),_display_(/*92.22*/views/*92.27*/.html.format.format()),format.raw/*92.48*/("""
                    """),_display_(/*93.22*/views/*93.27*/.html.general.alltaxons(taxon)),format.raw/*93.57*/("""
                """),format.raw/*94.17*/("""</div>
            </div>
        </div>
    </div>
""")))}))
      }
    }
  }

  def render(count:Int,taxonName:String,result:Map[String, String],taxon:List[String]): play.twirl.api.HtmlFormat.Appendable = apply(count,taxonName,result,taxon)

  def f:((Int,String,Map[String, String],List[String]) => play.twirl.api.HtmlFormat.Appendable) = (count,taxonName,result,taxon) => apply(count,taxonName,result,taxon)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Mon Feb 26 14:39:20 AEST 2018
                  SOURCE: /Users/s2980369/IdeaProjects/glycobase/glycobase/app/views/search/searchTaxonResult.scala.html
                  HASH: 87980e67cddc65d54b7803ee50679721229eb206
                  MATRIX: 658->1|1027->19|1202->99|1231->103|1242->107|1280->109|1313->340|1345->345|1681->654|1712->663|1796->720|1822->725|1851->726|2286->1134|2308->1147|2348->1149|2397->1170|2948->1694|2991->1721|3031->1723|3088->1752|3153->1790|3189->1817|3229->1819|3294->1856|3336->1871|3351->1877|3486->1990|3546->2032|3559->2037|3598->2038|3663->2075|3705->2090|3720->2096|3809->2163|3880->2203|3937->2232|4011->2279|4026->2285|4075->2313|4104->2315|4118->2320|4139->2332|4163->2335|4233->2378|4276->2412|4316->2414|4381->2451|4443->2495|4456->2500|4495->2501|4560->2538|4592->2543|4642->2584|4718->2638|4788->2677|4849->2711|4892->2745|4932->2747|4997->2784|5059->2828|5072->2833|5111->2834|5176->2871|5208->2876|5258->2917|5334->2971|5404->3010|5465->3044|5508->3078|5548->3080|5613->3117|5675->3161|5688->3166|5727->3167|5792->3204|5824->3209|5874->3250|5950->3304|6020->3343|6081->3377|6124->3411|6164->3413|6229->3450|6291->3494|6304->3499|6343->3500|6408->3537|6440->3542|6490->3583|6566->3637|6636->3676|6697->3710|6740->3744|6780->3746|6845->3783|6907->3827|6920->3832|6959->3833|7024->3870|7056->3875|7106->3916|7182->3970|7252->4009|7313->4043|7356->4077|7396->4079|7461->4116|7523->4160|7536->4165|7575->4166|7640->4203|7672->4208|7722->4249|7798->4303|7868->4342|7925->4371|7987->4402|8040->4427|8126->4482|8171->4499|8265->4566|8279->4571|8321->4592|8370->4614|8384->4619|8435->4649|8480->4666
                  LINES: 24->1|29->3|34->3|37->6|37->6|37->6|38->14|39->15|45->21|45->21|46->22|46->22|46->22|57->33|57->33|57->33|58->34|70->46|70->46|70->46|71->47|72->48|72->48|72->48|73->49|73->49|73->49|73->49|74->50|74->50|74->50|75->51|75->51|75->51|75->51|76->52|77->53|78->54|78->54|78->54|78->54|78->54|78->54|78->54|79->55|79->55|79->55|80->56|81->57|81->57|81->57|82->58|82->58|82->58|82->58|83->59|84->60|84->60|84->60|85->61|86->62|86->62|86->62|87->63|87->63|87->63|87->63|88->64|89->65|89->65|89->65|90->66|91->67|91->67|91->67|92->68|92->68|92->68|92->68|93->69|94->70|94->70|94->70|95->71|96->72|96->72|96->72|97->73|97->73|97->73|97->73|98->74|99->75|99->75|99->75|100->76|101->77|101->77|101->77|102->78|102->78|102->78|102->78|103->79|104->80|104->80|104->80|105->81|106->82|106->82|106->82|107->83|107->83|107->83|107->83|108->84|109->85|110->86|111->87|113->89|114->90|116->92|116->92|116->92|117->93|117->93|117->93|118->94
                  -- GENERATED --
              */
          