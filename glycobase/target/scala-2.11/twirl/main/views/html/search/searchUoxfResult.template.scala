
package views.html.search

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._
/*1.2*/import sparql._

object searchUoxfResult extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template3[Int,String,String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*3.2*/(count: Int, glycobaseid: String, uoxf: String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*3.49*/("""


"""),_display_(/*6.2*/main/*6.6*/ {_display_(Seq[Any](format.raw/*6.8*/("""
    """),format.raw/*14.13*/("""
    """),format.raw/*15.5*/("""<div class="section">
        <div class="container" >
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header">
                        <div class="pull-left">
                            <h1 class="info-header-grey text-primary">Structure Name - """),_display_(/*21.89*/uoxf),format.raw/*21.93*/("""</h1>
                        </div>
                        <div class="pull-right">
                            <div class="text-right brand"></div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-9">
                """),_display_(/*32.18*/if(count > 0)/*32.31*/ {_display_(Seq[Any](format.raw/*32.33*/("""

                    """),format.raw/*34.21*/("""<table class="table table-striped">
                        <thead><tr>
                            <th>Glycan</th>
                            <th>Name</th>
                            <th>Mono Mass</th>
                            <th>HPLC</th>
                            <th>UPLC (2-AB)</th>
                            <th>UPLC (Proc)</th>
                            <th>RPUPLC</th>
                            <th>CE</th>
                        </tr></thead>
                        <tbody>
                            <tr>
                                """),_display_(/*47.34*/if(session.get("notation"))/*47.61*/ {_display_(Seq[Any](format.raw/*47.63*/("""
                                    """),format.raw/*48.37*/("""<td><img src=""""),_display_(/*48.52*/routes/*48.58*/.Assets.versioned("images/sm-" + session.get("notation") + "/sm-" + session.get("notation") + "-" + glycobaseid + ".png")),format.raw/*48.179*/(""""><br/>
                                """)))}/*49.35*/else/*49.40*/{_display_(Seq[Any](format.raw/*49.41*/("""
                                    """),format.raw/*50.37*/("""<td><img src=""""),_display_(/*50.52*/routes/*50.58*/.Assets.versioned("images/sm-cfg-uoxf/sm-cfg-uoxf-" + glycobaseid + ".png")),format.raw/*50.133*/("""">
                                    """)))}),format.raw/*51.38*/("""
                            """),format.raw/*52.29*/("""</td>
                                <td><a href="""),_display_(/*53.46*/routes/*53.52*/.Application.showGlycan(glycobaseid)),format.raw/*53.88*/(""">"""),_display_(/*53.90*/uoxf),format.raw/*53.94*/("""</a></td>
                                """),_display_(/*54.34*/if(controllers.Application.monoMass(glycobaseid))/*54.83*/ {_display_(Seq[Any](format.raw/*54.85*/("""
                                    """),format.raw/*55.37*/("""<td>"""),_display_(/*55.42*/BigDecimal(controllers.Application.monoMass(glycobaseid))/*55.99*/.setScale(4, BigDecimal.RoundingMode.HALF_UP).toDouble),format.raw/*55.153*/("""</td>
                                """)))}/*56.35*/else/*56.40*/{_display_(Seq[Any](format.raw/*56.41*/("""
                                    """),format.raw/*57.37*/("""<td></td>
                                """)))}),format.raw/*58.34*/("""
                                """),_display_(/*59.34*/if(controllers.Application.avgHp(glycobaseid))/*59.80*/ {_display_(Seq[Any](format.raw/*59.82*/("""
                                    """),format.raw/*60.37*/("""<td>"""),_display_(/*60.42*/BigDecimal(controllers.Application.avgHp(glycobaseid))/*60.96*/.setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble),format.raw/*60.150*/("""</td>
                                """)))}/*61.35*/else/*61.40*/{_display_(Seq[Any](format.raw/*61.41*/("""
                                    """),format.raw/*62.37*/("""<td></td>
                                """)))}),format.raw/*63.34*/("""
                                """),_display_(/*64.34*/if(controllers.Application.avg2ABUp(glycobaseid))/*64.83*/ {_display_(Seq[Any](format.raw/*64.85*/("""
                                    """),format.raw/*65.37*/("""<td>"""),_display_(/*65.42*/BigDecimal(controllers.Application.avg2ABUp(glycobaseid))/*65.99*/.setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble),format.raw/*65.153*/("""</td>
                                """)))}/*66.35*/else/*66.40*/{_display_(Seq[Any](format.raw/*66.41*/("""
                                    """),format.raw/*67.37*/("""<td></td>
                                """)))}),format.raw/*68.34*/("""
                                """),_display_(/*69.34*/if(controllers.Application.avgProcUp(glycobaseid))/*69.84*/ {_display_(Seq[Any](format.raw/*69.86*/("""
                                    """),format.raw/*70.37*/("""<td>"""),_display_(/*70.42*/BigDecimal(controllers.Application.avgProcUp(glycobaseid))/*70.100*/.setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble),format.raw/*70.154*/("""</td>
                                """)))}/*71.35*/else/*71.40*/{_display_(Seq[Any](format.raw/*71.41*/("""
                                    """),format.raw/*72.37*/("""<td></td>
                                """)))}),format.raw/*73.34*/("""
                                """),_display_(/*74.34*/if(controllers.Application.avgRp(glycobaseid))/*74.80*/ {_display_(Seq[Any](format.raw/*74.82*/("""
                                    """),format.raw/*75.37*/("""<td>"""),_display_(/*75.42*/BigDecimal(controllers.Application.avgRp(glycobaseid))/*75.96*/.setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble),format.raw/*75.150*/("""</td>
                                """)))}/*76.35*/else/*76.40*/{_display_(Seq[Any](format.raw/*76.41*/("""
                                    """),format.raw/*77.37*/("""<td></td>
                                """)))}),format.raw/*78.34*/("""
                                """),_display_(/*79.34*/if(controllers.Application.avgCe(glycobaseid))/*79.80*/ {_display_(Seq[Any](format.raw/*79.82*/("""
                                    """),format.raw/*80.37*/("""<td>"""),_display_(/*80.42*/BigDecimal(controllers.Application.avgCe(glycobaseid))/*80.96*/.setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble),format.raw/*80.150*/("""</td>
                                """)))}/*81.35*/else/*81.40*/{_display_(Seq[Any](format.raw/*81.41*/("""
                                    """),format.raw/*82.37*/("""<td></td>
                                """)))}),format.raw/*83.34*/("""
                            """),format.raw/*84.29*/("""</tr>
                        </tbody>
                    </table>
                """)))}/*87.19*/else/*87.24*/{_display_(Seq[Any](format.raw/*87.25*/("""
                    """),format.raw/*88.21*/("""<h1>"""),_display_(/*88.26*/uoxf),format.raw/*88.30*/(""" """),format.raw/*88.31*/("""is not found in GlycoBase</h1>
                """)))}),format.raw/*89.18*/("""
                """),format.raw/*90.17*/("""</div>
                <div class="col-md-3">
                """),_display_(/*92.18*/views/*92.23*/.html.format.format()),format.raw/*92.44*/("""
                """),format.raw/*93.17*/("""</div>
            </div>
        </div>
    </div>
""")))}))
      }
    }
  }

  def render(count:Int,glycobaseid:String,uoxf:String): play.twirl.api.HtmlFormat.Appendable = apply(count,glycobaseid,uoxf)

  def f:((Int,String,String) => play.twirl.api.HtmlFormat.Appendable) = (count,glycobaseid,uoxf) => apply(count,glycobaseid,uoxf)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Mon Feb 26 14:39:20 AEST 2018
                  SOURCE: /Users/s2980369/IdeaProjects/glycobase/glycobase/app/views/search/searchUoxfResult.scala.html
                  HASH: 42515551da7fab0a68ca135c59bbad612b1ea30d
                  MATRIX: 658->1|1000->19|1142->66|1171->70|1182->74|1220->76|1253->300|1285->305|1619->612|1644->616|2061->1006|2083->1019|2123->1021|2173->1043|2765->1608|2801->1635|2841->1637|2906->1674|2948->1689|2963->1695|3106->1816|3166->1858|3179->1863|3218->1864|3283->1901|3325->1916|3340->1922|3437->1997|3508->2037|3565->2066|3643->2117|3658->2123|3715->2159|3744->2161|3769->2165|3839->2208|3897->2257|3937->2259|4002->2296|4034->2301|4100->2358|4176->2412|4234->2452|4247->2457|4286->2458|4351->2495|4425->2538|4486->2572|4541->2618|4581->2620|4646->2657|4678->2662|4741->2716|4817->2770|4875->2810|4888->2815|4927->2816|4992->2853|5066->2896|5127->2930|5185->2979|5225->2981|5290->3018|5322->3023|5388->3080|5464->3134|5522->3174|5535->3179|5574->3180|5639->3217|5713->3260|5774->3294|5833->3344|5873->3346|5938->3383|5970->3388|6038->3446|6114->3500|6172->3540|6185->3545|6224->3546|6289->3583|6363->3626|6424->3660|6479->3706|6519->3708|6584->3745|6616->3750|6679->3804|6755->3858|6813->3898|6826->3903|6865->3904|6930->3941|7004->3984|7065->4018|7120->4064|7160->4066|7225->4103|7257->4108|7320->4162|7396->4216|7454->4256|7467->4261|7506->4262|7571->4299|7645->4342|7702->4371|7806->4457|7819->4462|7858->4463|7907->4484|7939->4489|7964->4493|7993->4494|8072->4542|8117->4559|8207->4622|8221->4627|8263->4648|8308->4665
                  LINES: 24->1|29->3|34->3|37->6|37->6|37->6|38->14|39->15|45->21|45->21|56->32|56->32|56->32|58->34|71->47|71->47|71->47|72->48|72->48|72->48|72->48|73->49|73->49|73->49|74->50|74->50|74->50|74->50|75->51|76->52|77->53|77->53|77->53|77->53|77->53|78->54|78->54|78->54|79->55|79->55|79->55|79->55|80->56|80->56|80->56|81->57|82->58|83->59|83->59|83->59|84->60|84->60|84->60|84->60|85->61|85->61|85->61|86->62|87->63|88->64|88->64|88->64|89->65|89->65|89->65|89->65|90->66|90->66|90->66|91->67|92->68|93->69|93->69|93->69|94->70|94->70|94->70|94->70|95->71|95->71|95->71|96->72|97->73|98->74|98->74|98->74|99->75|99->75|99->75|99->75|100->76|100->76|100->76|101->77|102->78|103->79|103->79|103->79|104->80|104->80|104->80|104->80|105->81|105->81|105->81|106->82|107->83|108->84|111->87|111->87|111->87|112->88|112->88|112->88|112->88|113->89|114->90|116->92|116->92|116->92|117->93
                  -- GENERATED --
              */
          