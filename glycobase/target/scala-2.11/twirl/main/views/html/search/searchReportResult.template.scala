
package views.html.search

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._
/*1.2*/import sparql._

object searchReportResult extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template4[Int,String,Map[String, String],List[String],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*3.2*/(count: Int, reportName: String, result: Map[String, String], reportList:List[String]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*3.88*/("""


"""),_display_(/*6.2*/main/*6.6*/ {_display_(Seq[Any](format.raw/*6.8*/("""
    """),format.raw/*14.13*/("""

    """),format.raw/*16.5*/("""<div class="section">
        <div class="container" >
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header">
                        <div class="pull-left">
                            <h1 class="info-header-grey text-primary">Report - """),_display_(/*22.81*/reportName),format.raw/*22.91*/("""</h1>
                            <div class="minitext">"""),_display_(/*23.52*/count),format.raw/*23.57*/(""" """),format.raw/*23.58*/("""glycan structures</div>
                        </div>
                        <div class="pull-right">
                            <div class="text-right brand"></div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-9">
                """),_display_(/*34.18*/if(count > 0)/*34.31*/ {_display_(Seq[Any](format.raw/*34.33*/("""
                    """),format.raw/*35.21*/("""<table class="table table-striped">
                        <thead><tr>
                            <th>Glycan</th>
                            <th>Name</th>
                            <th>Mono Mass</th>
                            <th>UPLC (2-AB)</th>
                            <th>UPLC (Proc)</th>
                            """),_display_(/*42.30*/if(reportName.contains("GSL"))/*42.60*/{_display_(Seq[Any](format.raw/*42.61*/("""
                                """),format.raw/*43.33*/("""<th>Alternative Name</th>
                                <th>Code</th>
                                <th>Series</th>
                                <th>Category</th>
                            """)))}/*47.30*/else/*47.34*/{_display_(Seq[Any](format.raw/*47.35*/("""
                                """),format.raw/*48.33*/("""<th>HPLC</th>
                                <th>RPUPLC</th>
                                <th>CE</th>
                            """)))}),format.raw/*51.30*/("""

                        """),format.raw/*53.25*/("""</tr></thead>
                        <tbody>
                        """),_display_(/*55.26*/for((key, value) <- result) yield /*55.53*/ {_display_(Seq[Any](format.raw/*55.55*/("""
                            """),format.raw/*56.29*/("""<tr>
                                """),_display_(/*57.34*/if(session.get("notation"))/*57.61*/ {_display_(Seq[Any](format.raw/*57.63*/("""
                                    """),format.raw/*58.37*/("""<td><img src=""""),_display_(/*58.52*/routes/*58.58*/.Assets.versioned("images/sm-" + session.get("notation") + "/sm-" + session.get("notation") + "-" + key + ".png")),format.raw/*58.171*/(""""><br/>
                                """)))}/*59.35*/else/*59.40*/{_display_(Seq[Any](format.raw/*59.41*/("""
                                    """),format.raw/*60.37*/("""<td><img src=""""),_display_(/*60.52*/routes/*60.58*/.Assets.versioned("images/sm-cfg-uoxf/sm-cfg-uoxf-" + key + ".png")),format.raw/*60.125*/("""">
                                    """)))}),format.raw/*61.38*/("""
                            """),format.raw/*62.29*/("""</td>
                            <td><a href="""),_display_(/*63.42*/routes/*63.48*/.Application.showGlycan(key)),format.raw/*63.76*/(""">"""),_display_(/*63.78*/value/*63.83*/.split("\t")/*63.95*/(1)),format.raw/*63.98*/("""</a></td>
                                """),_display_(/*64.34*/if(value.split("\t")(5) == "null")/*64.68*/ {_display_(Seq[Any](format.raw/*64.70*/("""
                                    """),format.raw/*65.37*/("""<td></td>
                                """)))}/*66.35*/else/*66.40*/{_display_(Seq[Any](format.raw/*66.41*/("""
                                    """),format.raw/*67.37*/("""<td>"""),_display_(/*67.42*/BigDecimal(value.split("\t")(5).toDouble)/*67.83*/.setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble),format.raw/*67.137*/("""</td>
                                """)))}),format.raw/*68.34*/("""
                                """),_display_(/*69.34*/if(value.split("\t")(4) == "null")/*69.68*/ {_display_(Seq[Any](format.raw/*69.70*/("""
                                    """),format.raw/*70.37*/("""<td></td>
                                """)))}/*71.35*/else/*71.40*/{_display_(Seq[Any](format.raw/*71.41*/("""
                                    """),format.raw/*72.37*/("""<td>"""),_display_(/*72.42*/BigDecimal(value.split("\t")(4).toDouble)/*72.83*/.setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble),format.raw/*72.137*/("""</td>
                                """)))}),format.raw/*73.34*/("""
                                """),_display_(/*74.34*/if(value.split("\t")(6) == "null")/*74.68*/ {_display_(Seq[Any](format.raw/*74.70*/("""
                                    """),format.raw/*75.37*/("""<td></td>
                                """)))}/*76.35*/else/*76.40*/{_display_(Seq[Any](format.raw/*76.41*/("""
                                    """),format.raw/*77.37*/("""<td>"""),_display_(/*77.42*/BigDecimal(value.split("\t")(6).toDouble)/*77.83*/.setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble),format.raw/*77.137*/("""</td>
                                """)))}),format.raw/*78.34*/("""
                                """),_display_(/*79.34*/if(reportName.contains("GSL"))/*79.64*/{_display_(Seq[Any](format.raw/*79.65*/("""
                                    """),format.raw/*80.78*/("""
                                    """),format.raw/*85.39*/("""
                                """)))}/*86.34*/else/*86.38*/{_display_(Seq[Any](format.raw/*86.39*/("""
                                    """),_display_(/*87.38*/if(value.split("\t")(2) == "null")/*87.72*/ {_display_(Seq[Any](format.raw/*87.74*/("""
                                        """),format.raw/*88.41*/("""<td></td>
                                    """)))}/*89.39*/else/*89.44*/{_display_(Seq[Any](format.raw/*89.45*/("""
                                        """),format.raw/*90.41*/("""<td>"""),_display_(/*90.46*/BigDecimal(value.split("\t")(2).toDouble)/*90.87*/.setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble),format.raw/*90.141*/("""</td>
                                    """)))}),format.raw/*91.38*/("""
                                    """),_display_(/*92.38*/if(value.split("\t")(3) == "null")/*92.72*/ {_display_(Seq[Any](format.raw/*92.74*/("""
                                        """),format.raw/*93.41*/("""<td></td>
                                    """)))}/*94.39*/else/*94.44*/{_display_(Seq[Any](format.raw/*94.45*/("""
                                        """),format.raw/*95.41*/("""<td>"""),_display_(/*95.46*/BigDecimal(value.split("\t")(3).toDouble)/*95.87*/.setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble),format.raw/*95.141*/("""</td>
                                    """)))}),format.raw/*96.38*/("""
                                    """),_display_(/*97.38*/if(value.split("\t")(0) == "null")/*97.72*/ {_display_(Seq[Any](format.raw/*97.74*/("""
                                        """),format.raw/*98.41*/("""<td></td>
                                    """)))}/*99.39*/else/*99.44*/{_display_(Seq[Any](format.raw/*99.45*/("""
                                        """),format.raw/*100.41*/("""<td>"""),_display_(/*100.46*/BigDecimal(value.split("\t")(0).toDouble)/*100.87*/.setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble),format.raw/*100.141*/("""</td>
                                    """)))}),format.raw/*101.38*/("""
                                """)))}),format.raw/*102.34*/("""
                            """),format.raw/*103.29*/("""</tr>
                        """)))}),format.raw/*104.26*/("""
                        """),format.raw/*105.25*/("""</tbody>

                    </table>
                """)))}),format.raw/*108.18*/("""
                """),format.raw/*109.17*/("""</div>
                <div class="col-md-3">
                    """),_display_(/*111.22*/if(reportName.contains("Ludger"))/*111.55*/{_display_(Seq[Any](format.raw/*111.56*/("""
                        """),_display_(/*112.26*/views/*112.31*/.html.ludger.ludgerMaterial()),format.raw/*112.60*/("""
                    """)))}),format.raw/*113.22*/("""
                    """),_display_(/*114.22*/views/*114.27*/.html.format.format()),format.raw/*114.48*/("""
                    """),_display_(/*115.22*/views/*115.27*/.html.search.filterUoxfName()),format.raw/*115.56*/("""
                    """),_display_(/*116.22*/views/*116.27*/.html.general.allreports(reportList)),format.raw/*116.63*/("""
                """),format.raw/*117.17*/("""</div>
            </div>
        </div>
    </div>
""")))}))
      }
    }
  }

  def render(count:Int,reportName:String,result:Map[String, String],reportList:List[String]): play.twirl.api.HtmlFormat.Appendable = apply(count,reportName,result,reportList)

  def f:((Int,String,Map[String, String],List[String]) => play.twirl.api.HtmlFormat.Appendable) = (count,reportName,result,reportList) => apply(count,reportName,result,reportList)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Mon Feb 26 14:39:20 AEST 2018
                  SOURCE: /Users/s2980369/IdeaProjects/glycobase/glycobase/app/views/search/searchReportResult.scala.html
                  HASH: 575c9c81d0fe5151a2aa89d66e24a54ef589542f
                  MATRIX: 658->1|1028->19|1209->105|1238->109|1249->113|1287->115|1320->337|1353->343|1679->642|1710->652|1794->709|1820->714|1849->715|2284->1123|2306->1136|2346->1138|2395->1159|2754->1491|2793->1521|2832->1522|2893->1555|3111->1754|3124->1758|3163->1759|3224->1792|3390->1927|3444->1953|3542->2024|3585->2051|3625->2053|3682->2082|3747->2120|3783->2147|3823->2149|3888->2186|3930->2201|3945->2207|4080->2320|4140->2362|4153->2367|4192->2368|4257->2405|4299->2420|4314->2426|4403->2493|4474->2533|4531->2562|4605->2609|4620->2615|4669->2643|4698->2645|4712->2650|4733->2662|4757->2665|4827->2708|4870->2742|4910->2744|4975->2781|5037->2825|5050->2830|5089->2831|5154->2868|5186->2873|5236->2914|5312->2968|5382->3007|5443->3041|5486->3075|5526->3077|5591->3114|5653->3158|5666->3163|5705->3164|5770->3201|5802->3206|5852->3247|5928->3301|5998->3340|6059->3374|6102->3408|6142->3410|6207->3447|6269->3491|6282->3496|6321->3497|6386->3534|6418->3539|6468->3580|6544->3634|6614->3673|6675->3707|6714->3737|6753->3738|6818->3816|6883->4297|6936->4331|6949->4335|6988->4336|7053->4374|7096->4408|7136->4410|7205->4451|7271->4499|7284->4504|7323->4505|7392->4546|7424->4551|7474->4592|7550->4646|7624->4689|7689->4727|7732->4761|7772->4763|7841->4804|7907->4852|7920->4857|7959->4858|8028->4899|8060->4904|8110->4945|8186->4999|8260->5042|8325->5080|8368->5114|8408->5116|8477->5157|8543->5205|8556->5210|8595->5211|8665->5252|8698->5257|8749->5298|8826->5352|8901->5395|8967->5429|9025->5458|9088->5489|9142->5514|9230->5570|9276->5587|9371->5654|9414->5687|9454->5688|9508->5714|9523->5719|9574->5748|9628->5770|9678->5792|9693->5797|9736->5818|9786->5840|9801->5845|9852->5874|9902->5896|9917->5901|9975->5937|10021->5954
                  LINES: 24->1|29->3|34->3|37->6|37->6|37->6|38->14|40->16|46->22|46->22|47->23|47->23|47->23|58->34|58->34|58->34|59->35|66->42|66->42|66->42|67->43|71->47|71->47|71->47|72->48|75->51|77->53|79->55|79->55|79->55|80->56|81->57|81->57|81->57|82->58|82->58|82->58|82->58|83->59|83->59|83->59|84->60|84->60|84->60|84->60|85->61|86->62|87->63|87->63|87->63|87->63|87->63|87->63|87->63|88->64|88->64|88->64|89->65|90->66|90->66|90->66|91->67|91->67|91->67|91->67|92->68|93->69|93->69|93->69|94->70|95->71|95->71|95->71|96->72|96->72|96->72|96->72|97->73|98->74|98->74|98->74|99->75|100->76|100->76|100->76|101->77|101->77|101->77|101->77|102->78|103->79|103->79|103->79|104->80|105->85|106->86|106->86|106->86|107->87|107->87|107->87|108->88|109->89|109->89|109->89|110->90|110->90|110->90|110->90|111->91|112->92|112->92|112->92|113->93|114->94|114->94|114->94|115->95|115->95|115->95|115->95|116->96|117->97|117->97|117->97|118->98|119->99|119->99|119->99|120->100|120->100|120->100|120->100|121->101|122->102|123->103|124->104|125->105|128->108|129->109|131->111|131->111|131->111|132->112|132->112|132->112|133->113|134->114|134->114|134->114|135->115|135->115|135->115|136->116|136->116|136->116|137->117
                  -- GENERATED --
              */
          