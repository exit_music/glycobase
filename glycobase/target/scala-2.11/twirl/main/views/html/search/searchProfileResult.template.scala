
package views.html.search

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._
/*1.2*/import sparql._

object searchProfileResult extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template3[Int,String,List[SparqlEntity],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*3.2*/(count: Int, profile: String, x: List[SparqlEntity]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*3.54*/("""


"""),_display_(/*6.2*/main/*6.6*/ {_display_(Seq[Any](format.raw/*6.8*/("""
    """),format.raw/*14.13*/("""
    """),format.raw/*15.5*/("""<div class="section">
        <div class="container" >
            <div class="row">
                <div class="col-md-12">

                    <div class="page-header">
                        <div class="pull-left">
                            <h1 class="info-header-grey text-primary">Profile Selection</h1>
                            <div class="minitext">"""),_display_(/*23.52*/count),format.raw/*23.57*/(""" """),format.raw/*23.58*/("""glycan structures</div>
                        </div>
                        <div class="pull-right">
                            <div class="text-right brand"></div>
                        </div>
                        <div class="clearfix"></div>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-9">
                """),_display_(/*35.18*/if(count > 0)/*35.31*/ {_display_(Seq[Any](format.raw/*35.33*/("""
                    """),format.raw/*36.21*/("""<table class="table table-striped">
                        <thead><tr>
                            <th>Glycan</th>
                            <th>Name</th>
                            <th>Evidence Type</th>
                            <th>Gu</th>
                            <th>Peak Area</th>
                        </tr></thead>
                        <tbody>
                        """),_display_(/*45.26*/for(entry <- x) yield /*45.41*/ {_display_(Seq[Any](format.raw/*45.43*/("""
                            """),format.raw/*46.29*/("""<tr>"""),_display_(/*46.34*/if(session.get("notation"))/*46.61*/ {_display_(Seq[Any](format.raw/*46.63*/("""
                                """),format.raw/*47.33*/("""<td><img src=""""),_display_(/*47.48*/routes/*47.54*/.Assets.versioned("images/sm-" + session.get("notation") + "/sm-" + session.get("notation") + "-" + entry.getValue("GlycoBaseId") + ".png")),format.raw/*47.193*/(""""><br/>
                            """)))}/*48.31*/else/*48.36*/{_display_(Seq[Any](format.raw/*48.37*/("""
                                """),format.raw/*49.33*/("""<td><img src=""""),_display_(/*49.48*/routes/*49.54*/.Assets.versioned("images/sm-cfg-uoxf/sm-cfg-uoxf-" + entry.getValue("GlycoBaseId") + ".png")),format.raw/*49.147*/("""">
                                    """)))}),format.raw/*50.38*/("""
                            """),format.raw/*51.29*/("""<td><a href="""),_display_(/*51.42*/routes/*51.48*/.Application.showGlycan(entry.getValue("GlycoBaseId"))),format.raw/*51.102*/(""">"""),_display_(/*51.104*/entry/*51.109*/.getData.get("Uoxf")),format.raw/*51.129*/("""</a></td>
                            <td>"""),_display_(/*52.34*/entry/*52.39*/.getValue("EvidenceType").split("_")/*52.75*/(1)),format.raw/*52.78*/("""</td>
                            <td>"""),_display_(/*53.34*/entry/*53.39*/.getData.get("Gu")),format.raw/*53.57*/("""</td>
                            <td>"""),_display_(/*54.34*/entry/*54.39*/.getData.get("PeakArea")),format.raw/*54.63*/("""</td>

                            </tr>
                        """)))}),format.raw/*57.26*/("""
                        """),format.raw/*58.25*/("""</tbody>

                    </table>
                """)))}/*61.19*/else/*61.24*/{_display_(Seq[Any](format.raw/*61.25*/("""
                    """),format.raw/*62.21*/("""<h1>Profile """),_display_(/*62.34*/profile),format.raw/*62.41*/(""" """),format.raw/*62.42*/("""doesn't exist</h1>
                """)))}),format.raw/*63.18*/("""
                """),format.raw/*64.17*/("""</div>
                <div class="col-md-3">
                """),_display_(/*66.18*/views/*66.23*/.html.format.format()),format.raw/*66.44*/("""
                """),format.raw/*67.17*/("""</div>
            </div>
        </div>
    </div>

""")))}),format.raw/*72.2*/("""
"""))
      }
    }
  }

  def render(count:Int,profile:String,x:List[SparqlEntity]): play.twirl.api.HtmlFormat.Appendable = apply(count,profile,x)

  def f:((Int,String,List[SparqlEntity]) => play.twirl.api.HtmlFormat.Appendable) = (count,profile,x) => apply(count,profile,x)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Mon Feb 26 14:39:20 AEST 2018
                  SOURCE: /Users/s2980369/IdeaProjects/glycobase/glycobase/app/views/search/searchProfileResult.scala.html
                  HASH: 60c20acc72ff8e9f8da5bc2cbaee4fcb168e7437
                  MATRIX: 658->1|1015->19|1162->71|1191->75|1202->79|1240->81|1273->311|1305->316|1696->680|1722->685|1751->686|2187->1095|2209->1108|2249->1110|2298->1131|2716->1522|2747->1537|2787->1539|2844->1568|2876->1573|2912->1600|2952->1602|3013->1635|3055->1650|3070->1656|3231->1795|3287->1833|3300->1838|3339->1839|3400->1872|3442->1887|3457->1893|3572->1986|3643->2026|3700->2055|3740->2068|3755->2074|3831->2128|3861->2130|3876->2135|3918->2155|3988->2198|4002->2203|4047->2239|4071->2242|4137->2281|4151->2286|4190->2304|4256->2343|4270->2348|4315->2372|4412->2438|4465->2463|4540->2520|4553->2525|4592->2526|4641->2547|4681->2560|4709->2567|4738->2568|4805->2604|4850->2621|4940->2684|4954->2689|4996->2710|5041->2727|5125->2781
                  LINES: 24->1|29->3|34->3|37->6|37->6|37->6|38->14|39->15|47->23|47->23|47->23|59->35|59->35|59->35|60->36|69->45|69->45|69->45|70->46|70->46|70->46|70->46|71->47|71->47|71->47|71->47|72->48|72->48|72->48|73->49|73->49|73->49|73->49|74->50|75->51|75->51|75->51|75->51|75->51|75->51|75->51|76->52|76->52|76->52|76->52|77->53|77->53|77->53|78->54|78->54|78->54|81->57|82->58|85->61|85->61|85->61|86->62|86->62|86->62|86->62|87->63|88->64|90->66|90->66|90->66|91->67|96->72
                  -- GENERATED --
              */
          