
package views.html.search

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._
/*1.2*/import sparql._

object searchGuResult extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template11[Int,String,String,String,List[SparqlEntity],Int,Int,Int,Int,Int,String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*3.2*/(count: Int, min: String, max: String, expType: String, x: List[SparqlEntity], core: Int, bisect: Int, outer: Int, hybrid: Int, mannose: Int, olinkResult: String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*3.164*/("""

"""),_display_(/*5.2*/main/*5.6*/ {_display_(Seq[Any](format.raw/*5.8*/("""

    """),format.raw/*7.5*/("""<div class="section">
        <div class="container" >
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header">
                        <div class="pull-left">
                            <h1 class="info-header-grey text-primary">Retention Results</h1>
                            <div class="minitext">
                                """),_display_(/*15.34*/if(expType == "rpuplc")/*15.57*/{_display_(Seq[Any](format.raw/*15.58*/("""
                                    """),format.raw/*16.37*/("""<h4 class="text-primary">"""),_display_(/*16.63*/count),format.raw/*16.68*/(""" """),format.raw/*16.69*/("""glycan structures found in """),_display_(/*16.97*/expType/*16.104*/.toUpperCase()),format.raw/*16.118*/(""" """),format.raw/*16.119*/("""data within AU range """),_display_(/*16.141*/min),format.raw/*16.144*/(""" """),format.raw/*16.145*/("""to """),_display_(/*16.149*/max),format.raw/*16.152*/("""</h4><br />
                                """)))}/*17.34*/else/*17.38*/{_display_(Seq[Any](format.raw/*17.39*/("""
                                    """),format.raw/*18.37*/("""<h4 class="text-primary">"""),_display_(/*18.63*/count),format.raw/*18.68*/(""" """),format.raw/*18.69*/("""glycan structures found in """),_display_(/*18.97*/expType/*18.104*/.toUpperCase()),format.raw/*18.118*/(""" """),format.raw/*18.119*/("""data within GU range """),_display_(/*18.141*/min),format.raw/*18.144*/(""" """),format.raw/*18.145*/("""to """),_display_(/*18.149*/max),format.raw/*18.152*/("""
                                    """),_display_(/*19.38*/if(core ==1 || bisect == 1 || core == 1 || outer == 1 || hybrid == 1 || mannose == 1 )/*19.124*/ {_display_(Seq[Any](format.raw/*19.126*/("""
                                        """),format.raw/*20.41*/("""with """),_display_(/*20.47*/if(core == 1)/*20.60*/ {_display_(Seq[Any](format.raw/*20.62*/("""
                                            """),format.raw/*21.45*/("""Core Fucose""")))}),format.raw/*21.57*/(""" """),_display_(/*21.59*/if(bisect == 1)/*21.74*/ {_display_(Seq[Any](format.raw/*21.76*/(""" """),format.raw/*21.77*/("""Bisect """)))}),format.raw/*21.85*/(""" """),_display_(/*21.87*/if(outer == 1)/*21.101*/ {_display_(Seq[Any](format.raw/*21.103*/(""" """),format.raw/*21.104*/("""Outer Arm Fucose """)))}),format.raw/*21.122*/(""" """),_display_(/*21.124*/if(hybrid == 1)/*21.139*/ {_display_(Seq[Any](format.raw/*21.141*/(""" """),format.raw/*21.142*/("""Hybrid """)))}),format.raw/*21.150*/(""" """),_display_(/*21.152*/if(mannose == 1)/*21.168*/ {_display_(Seq[Any](format.raw/*21.170*/(""" """),format.raw/*21.171*/("""Mannose """)))}),format.raw/*21.180*/("""
                                    """)))}),format.raw/*22.38*/("""
                                    """),format.raw/*23.37*/("""</h4><br />

                                """)))}),format.raw/*25.34*/("""
                            """),format.raw/*26.29*/("""</div>
                        </div>
                        <div class="pull-right">
                            <div class="text-right brand"></div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-9">
                """),_display_(/*37.18*/if(count>0)/*37.29*/{_display_(Seq[Any](format.raw/*37.30*/("""
                    """),format.raw/*38.21*/("""<table class="table table-striped">
                        <thead><tr>
                            <th>Glycan</th>
                            <th>Name</th>
                            <th><font size="2">Mono Mass</font></th>
                            """),_display_(/*43.30*/if(expType == "rpuplc")/*43.53*/{_display_(Seq[Any](format.raw/*43.54*/("""
                                """),format.raw/*44.33*/("""<th>Au</th>
                            """)))}/*45.30*/else/*45.34*/{_display_(Seq[Any](format.raw/*45.35*/("""<th>Gu</th>""")))}),format.raw/*45.47*/("""
                        """),format.raw/*46.25*/("""</tr></thead>

                        <tbody>
                        """),_display_(/*49.26*/for(entry <-x) yield /*49.40*/{_display_(Seq[Any](format.raw/*49.41*/("""
                            """),format.raw/*50.29*/("""<tr>"""),_display_(/*50.34*/if(expType == "pgc")/*50.54*/{_display_(Seq[Any](format.raw/*50.55*/("""
                                """),format.raw/*51.33*/("""<td>"""),_display_(/*51.38*/if( entry.getValue("GlycoBaseId").toInt < 100000 )/*51.88*/ {_display_(Seq[Any](format.raw/*51.90*/("""
                                    """),format.raw/*52.37*/("""<img src="""),_display_(/*52.47*/{routes.Image.showImageReaction(entry.getValue("GlycoBaseId"), session.get("notation"), "normal")}),format.raw/*52.145*/(""" """),format.raw/*52.146*/("""alt=""/><br/>
                                """)))}/*53.35*/else/*53.40*/{_display_(Seq[Any](format.raw/*53.41*/("""
                                    """),format.raw/*54.37*/("""<img src="""),_display_(/*54.47*/{routes.Image.showImagePgc(entry.getValue("GlycoStoreId"), session.get("notation"), "normal")}),format.raw/*54.141*/(""" """),format.raw/*54.142*/("""alt=""/><br/>
                                """)))}),format.raw/*55.34*/("""
                                """),format.raw/*56.33*/("""</td>
                                <td><a href="""),_display_(/*57.46*/routes/*57.52*/.Application.showGlycan(entry.getValue("GlycoBaseId"))),format.raw/*57.106*/(""">"""),_display_(/*57.108*/entry/*57.113*/.getData.get("Uoxf")),format.raw/*57.133*/("""</a></td>
                                """),_display_(/*58.34*/if(controllers.Application.monoMass(entry.getValue("GlycoBaseId")) == "null")/*58.111*/ {_display_(Seq[Any](format.raw/*58.113*/("""
                                    """),format.raw/*59.37*/("""<td></td>
                                """)))}/*60.35*/else/*60.40*/{_display_(Seq[Any](format.raw/*60.41*/("""
                                    """),format.raw/*61.37*/("""<td>"""),_display_(/*61.42*/BigDecimal(controllers.Application.monoMass(entry.getValue("GlycoBaseId")))/*61.117*/.setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble),format.raw/*61.171*/("""</td>
                                """)))}),format.raw/*62.34*/("""
                                """),format.raw/*63.33*/("""<td>"""),_display_(/*63.38*/BigDecimal(controllers.Application.avgPgc(entry.getValue("GlycoBaseId")))/*63.111*/.setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble),format.raw/*63.165*/("""</td>
                            """)))}),format.raw/*64.30*/("""

                                """),_display_(/*66.34*/if(session.get("notation"))/*66.61*/{_display_(Seq[Any](format.raw/*66.62*/("""
                                    """),format.raw/*67.37*/("""<td><img src=""""),_display_(/*67.52*/routes/*67.58*/.Assets.versioned("images/sm-"+session.get("notation")+"/sm-"+session.get("notation")+"-" +entry.getValue("GlycoBaseId")+".png")),format.raw/*67.186*/(""""><br/>
                                """)))}/*68.35*/else/*68.39*/{_display_(Seq[Any](format.raw/*68.40*/("""<td><img src=""""),_display_(/*68.55*/routes/*68.61*/.Assets.versioned("images/sm-cfg-uoxf/sm-cfg-uoxf-" +entry.getValue("GlycoBaseId")+".png")),format.raw/*68.151*/("""">""")))}),format.raw/*68.154*/("""
                            """),format.raw/*69.29*/("""<td><a href="""),_display_(/*69.42*/routes/*69.48*/.Application.showGlycan(entry.getValue("GlycoBaseId"))),format.raw/*69.102*/(""">"""),_display_(/*69.104*/entry/*69.109*/.getData.get("Uoxf")),format.raw/*69.129*/("""</a></td>


                                """),_display_(/*72.34*/if(controllers.Application.monoMass(entry.getValue("GlycoBaseId")) == "null" || controllers.Application.monoMass(entry.getValue("GlycoBaseId")) == null)/*72.186*/ {_display_(Seq[Any](format.raw/*72.188*/("""                                    """),format.raw/*72.224*/("""<td></td>
                                """)))}/*73.35*/else/*73.40*/{_display_(Seq[Any](format.raw/*73.41*/("""
                                    """),format.raw/*74.37*/("""<td>"""),_display_(/*74.42*/BigDecimal(controllers.Application.monoMass(entry.getValue("GlycoBaseId")))/*74.117*/.setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble),format.raw/*74.171*/("""</td>
                                """)))}),format.raw/*75.34*/("""

                                """),_display_(/*77.34*/if(expType == "hplc")/*77.55*/{_display_(Seq[Any](format.raw/*77.56*/("""
                                    """),format.raw/*78.37*/("""<td>"""),_display_(/*78.42*/BigDecimal(controllers.Application.avgHp(entry.getValue("GlycoBaseId")))/*78.114*/.setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble),format.raw/*78.168*/("""</td>
                                """)))}),_display_(/*79.35*/if(expType == "uplc_2-ab")/*79.61*/{_display_(Seq[Any](format.raw/*79.62*/("""
                                    """),format.raw/*80.37*/("""<td>"""),_display_(/*80.42*/BigDecimal(controllers.Application.avg2ABUp(entry.getValue("GlycoBaseId")))/*80.117*/.setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble),format.raw/*80.171*/("""</td>
                                """)))}),_display_(/*81.35*/if(expType == "uplc_procainamide")/*81.69*/{_display_(Seq[Any](format.raw/*81.70*/("""
                                    """),format.raw/*82.37*/("""<td>"""),_display_(/*82.42*/BigDecimal(controllers.Application.avgProcUp(entry.getValue("GlycoBaseId")))/*82.118*/.setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble),format.raw/*82.172*/("""</td>
                                """)))}),_display_(/*83.35*/if(expType == "rpuplc")/*83.58*/{_display_(Seq[Any](format.raw/*83.59*/("""
                                    """),format.raw/*84.37*/("""<td>"""),_display_(/*84.42*/BigDecimal(controllers.Application.avgRp(entry.getValue("GlycoBaseId")))/*84.114*/.setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble),format.raw/*84.168*/("""</td>
                                """)))}),_display_(/*85.35*/if(expType == "ce")/*85.54*/{_display_(Seq[Any](format.raw/*85.55*/("""
                                    """),format.raw/*86.37*/("""<td>"""),_display_(/*86.42*/BigDecimal(controllers.Application.avgCe(entry.getValue("GlycoBaseId")))/*86.114*/.setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble),format.raw/*86.168*/("""</td>
                                """)))}),format.raw/*87.34*/("""

                            """),format.raw/*89.29*/("""</tr>
                        """)))}),format.raw/*90.26*/("""
                        """),format.raw/*91.25*/("""</tbody>
                    </table>
                """)))}),format.raw/*93.18*/("""
                """),format.raw/*94.17*/("""</div>
                <div class="col-md-3">
                    """),_display_(/*96.22*/views/*96.27*/.html.format.format()),format.raw/*96.48*/("""
                    """),_display_(/*97.22*/views/*97.27*/.html.search.filterUoxfGuRange(min, max, expType)),format.raw/*97.76*/("""

                    """),format.raw/*99.92*/("""
                    """),_display_(/*100.22*/if(olinkResult.matches("yes"))/*100.52*/ {_display_(Seq[Any](format.raw/*100.54*/(""" """),_display_(/*100.56*/views/*100.61*/.html.search.olinkMotifs(min, max, expType))))}),format.raw/*100.105*/("""



                """),format.raw/*104.17*/("""</div>
            </div>
        </div>
    </div>

""")))}))
      }
    }
  }

  def render(count:Int,min:String,max:String,expType:String,x:List[SparqlEntity],core:Int,bisect:Int,outer:Int,hybrid:Int,mannose:Int,olinkResult:String): play.twirl.api.HtmlFormat.Appendable = apply(count,min,max,expType,x,core,bisect,outer,hybrid,mannose,olinkResult)

  def f:((Int,String,String,String,List[SparqlEntity],Int,Int,Int,Int,Int,String) => play.twirl.api.HtmlFormat.Appendable) = (count,min,max,expType,x,core,bisect,outer,hybrid,mannose,olinkResult) => apply(count,min,max,expType,x,core,bisect,outer,hybrid,mannose,olinkResult)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Mon Feb 26 14:39:20 AEST 2018
                  SOURCE: /Users/s2980369/IdeaProjects/glycobase/glycobase/app/views/search/searchGuResult.scala.html
                  HASH: 34f9f96d0450fcbd317a83df9c3fc4705c0fd846
                  MATRIX: 658->1|1052->19|1310->181|1338->184|1349->188|1387->190|1419->196|1842->592|1874->615|1913->616|1978->653|2031->679|2057->684|2086->685|2141->713|2158->720|2194->734|2224->735|2274->757|2299->760|2329->761|2361->765|2386->768|2450->813|2463->817|2502->818|2567->855|2620->881|2646->886|2675->887|2730->915|2747->922|2783->936|2813->937|2863->959|2888->962|2918->963|2950->967|2975->970|3040->1008|3136->1094|3177->1096|3246->1137|3279->1143|3301->1156|3341->1158|3414->1203|3457->1215|3486->1217|3510->1232|3550->1234|3579->1235|3618->1243|3647->1245|3671->1259|3712->1261|3742->1262|3792->1280|3822->1282|3847->1297|3888->1299|3918->1300|3958->1308|3988->1310|4014->1326|4055->1328|4085->1329|4126->1338|4195->1376|4260->1413|4337->1459|4394->1488|4812->1879|4832->1890|4871->1891|4920->1912|5203->2168|5235->2191|5274->2192|5335->2225|5395->2266|5408->2270|5447->2271|5490->2283|5543->2308|5642->2380|5672->2394|5711->2395|5768->2424|5800->2429|5829->2449|5868->2450|5929->2483|5961->2488|6020->2538|6060->2540|6125->2577|6162->2587|6282->2685|6312->2686|6378->2734|6391->2739|6430->2740|6495->2777|6532->2787|6648->2881|6678->2882|6756->2929|6817->2962|6895->3013|6910->3019|6986->3073|7016->3075|7031->3080|7073->3100|7143->3143|7230->3220|7271->3222|7336->3259|7398->3303|7411->3308|7450->3309|7515->3346|7547->3351|7632->3426|7708->3480|7778->3519|7839->3552|7871->3557|7954->3630|8030->3684|8096->3719|8158->3754|8194->3781|8233->3782|8298->3819|8340->3834|8355->3840|8505->3968|8565->4010|8578->4014|8617->4015|8659->4030|8674->4036|8786->4126|8821->4129|8878->4158|8918->4171|8933->4177|9009->4231|9039->4233|9054->4238|9096->4258|9168->4303|9330->4455|9371->4457|9436->4493|9498->4537|9511->4542|9550->4543|9615->4580|9647->4585|9732->4660|9808->4714|9878->4753|9940->4788|9970->4809|10009->4810|10074->4847|10106->4852|10188->4924|10264->4978|10334->5018|10369->5044|10408->5045|10473->5082|10505->5087|10590->5162|10666->5216|10736->5256|10779->5290|10818->5291|10883->5328|10915->5333|11001->5409|11077->5463|11147->5503|11179->5526|11218->5527|11283->5564|11315->5569|11397->5641|11473->5695|11543->5735|11571->5754|11610->5755|11675->5792|11707->5797|11789->5869|11865->5923|11935->5962|11993->5992|12055->6023|12108->6048|12194->6103|12239->6120|12333->6187|12347->6192|12389->6213|12438->6235|12452->6240|12522->6289|12572->6382|12622->6404|12662->6434|12703->6436|12733->6438|12748->6443|12818->6487|12867->6507
                  LINES: 24->1|29->3|34->3|36->5|36->5|36->5|38->7|46->15|46->15|46->15|47->16|47->16|47->16|47->16|47->16|47->16|47->16|47->16|47->16|47->16|47->16|47->16|47->16|48->17|48->17|48->17|49->18|49->18|49->18|49->18|49->18|49->18|49->18|49->18|49->18|49->18|49->18|49->18|49->18|50->19|50->19|50->19|51->20|51->20|51->20|51->20|52->21|52->21|52->21|52->21|52->21|52->21|52->21|52->21|52->21|52->21|52->21|52->21|52->21|52->21|52->21|52->21|52->21|52->21|52->21|52->21|52->21|52->21|53->22|54->23|56->25|57->26|68->37|68->37|68->37|69->38|74->43|74->43|74->43|75->44|76->45|76->45|76->45|76->45|77->46|80->49|80->49|80->49|81->50|81->50|81->50|81->50|82->51|82->51|82->51|82->51|83->52|83->52|83->52|83->52|84->53|84->53|84->53|85->54|85->54|85->54|85->54|86->55|87->56|88->57|88->57|88->57|88->57|88->57|88->57|89->58|89->58|89->58|90->59|91->60|91->60|91->60|92->61|92->61|92->61|92->61|93->62|94->63|94->63|94->63|94->63|95->64|97->66|97->66|97->66|98->67|98->67|98->67|98->67|99->68|99->68|99->68|99->68|99->68|99->68|99->68|100->69|100->69|100->69|100->69|100->69|100->69|100->69|103->72|103->72|103->72|103->72|104->73|104->73|104->73|105->74|105->74|105->74|105->74|106->75|108->77|108->77|108->77|109->78|109->78|109->78|109->78|110->79|110->79|110->79|111->80|111->80|111->80|111->80|112->81|112->81|112->81|113->82|113->82|113->82|113->82|114->83|114->83|114->83|115->84|115->84|115->84|115->84|116->85|116->85|116->85|117->86|117->86|117->86|117->86|118->87|120->89|121->90|122->91|124->93|125->94|127->96|127->96|127->96|128->97|128->97|128->97|130->99|131->100|131->100|131->100|131->100|131->100|131->100|135->104
                  -- GENERATED --
              */
          