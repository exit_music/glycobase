
package views.html

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._

object index extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template0[play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply():play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](_display_(/*1.2*/main/*1.6*/ {_display_(Seq[Any](format.raw/*1.8*/("""
    """),format.raw/*2.5*/("""<div class="cover">
        <div class="cover-image">
            <img src=""""),_display_(/*4.24*/routes/*4.30*/.Assets.versioned("images/htludger.jpg")),format.raw/*4.70*/("""" class="cover-image">
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h1 class="text-inverse">Relating Structure to Separation</h1>
                    <p class="text-inverse">Integrated Platform of Glycan Retention Properties</p>
                    <br>
                    <br>
                    <a href="/collections" class="btn btn-lg btn-primary">Explore</a>
                </div>
            </div>
        </div>
    </div>
    <div class="section">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <img src=""""),_display_(/*22.32*/routes/*22.38*/.Assets.versioned("images/collaborators/glycomics.jpg")),format.raw/*22.93*/("""" class="center-block img-responsive">
                </div>
                <div class="col-md-4">
                    <img src=""""),_display_(/*25.32*/routes/*25.38*/.Assets.versioned("images/collaborators/bti.png")),format.raw/*25.87*/("""" class="center-block img-responsive">
                </div>
                <div class="col-md-4">
                    <img src=""""),_display_(/*28.32*/routes/*28.38*/.Assets.versioned("images/collaborators/mq.png")),format.raw/*28.86*/("""" class="center-block img-responsive">
                </div>
            </div>
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-3">
                    <img src=""""),_display_(/*34.32*/routes/*34.38*/.Assets.versioned("images/ludger_logo.jpg")),format.raw/*34.81*/("""" class="center-block img-responsive">
                </div>
                <div class="col-md-3">
                    <img src=""""),_display_(/*37.32*/routes/*37.38*/.Assets.versioned("images/collaborators/NIBRT.png")),format.raw/*37.89*/("""" class="center-block img-responsive">
                </div>
                <div class="col-md-3"></div>
            </div>
        </div>
    </div>


    <script>
  (function(i,s,o,g,r,a,m)"""),format.raw/*46.27*/("""{"""),format.raw/*46.28*/("""i['GoogleAnalyticsObject']=r;i[r]=i[r]||function()"""),format.raw/*46.78*/("""{"""),format.raw/*46.79*/("""
              """),format.raw/*47.15*/("""(i[r].q=i[r].q||[]).push(arguments)"""),format.raw/*47.50*/("""}"""),format.raw/*47.51*/(""",i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  """),format.raw/*49.3*/("""}"""),format.raw/*49.4*/(""")(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-74498737-1', 'auto');
  ga('send', 'pageview');

</script>

""")))}))
      }
    }
  }

  def render(): play.twirl.api.HtmlFormat.Appendable = apply()

  def f:(() => play.twirl.api.HtmlFormat.Appendable) = () => apply()

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Mon Feb 26 14:39:19 AEST 2018
                  SOURCE: /Users/s2980369/IdeaProjects/glycobase/glycobase/app/views/index.scala.html
                  HASH: 564c197e0599e8db39a053f69ea2ebab61fe0fa9
                  MATRIX: 1030->1|1041->5|1079->7|1110->12|1213->89|1227->95|1287->135|2010->831|2025->837|2101->892|2260->1024|2275->1030|2345->1079|2504->1211|2519->1217|2588->1265|2841->1491|2856->1497|2920->1540|3079->1672|3094->1678|3166->1729|3387->1922|3416->1923|3494->1973|3523->1974|3566->1989|3629->2024|3658->2025|3820->2160|3848->2161
                  LINES: 33->1|33->1|33->1|34->2|36->4|36->4|36->4|54->22|54->22|54->22|57->25|57->25|57->25|60->28|60->28|60->28|66->34|66->34|66->34|69->37|69->37|69->37|78->46|78->46|78->46|78->46|79->47|79->47|79->47|81->49|81->49
                  -- GENERATED --
              */
          