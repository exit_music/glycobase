
package views.html.format

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._

object format extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template0[play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply():play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*3.1*/("""<div class="info-grey">
    <h3>Structure Format</h3>
    <a href="/format/cfg"><span class='label label-dark'><span class='icon-adjust icon-white'></span> CFG/Essentials</span></a>
    <a href="/format/iupac"><span class='label label-dark'><span class='icon-font icon-white'></span> Text</span></a>
    <a href="/format/uoxf"><span class='label label-dark'><span class='icon-stop icon-white'></span> Oxford</span></a>
    <a href="/format/uoxf-color"><span class='label label-dark'><span class='icon-stop icon-white'></span> Oxford Color</span></a>
    <a href="/format/cfg-uoxf"><span class='label label-dark'><span class='icon-stop icon-white'></span> CFG Oxford Hybrid</span></a>
</div>"""))
      }
    }
  }

  def render(): play.twirl.api.HtmlFormat.Appendable = apply()

  def f:(() => play.twirl.api.HtmlFormat.Appendable) = () => apply()

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Mon Feb 26 14:39:19 AEST 2018
                  SOURCE: /Users/s2980369/IdeaProjects/glycobase/glycobase/app/views/format/format.scala.html
                  HASH: e336fe1cae6fe9e797e4f1f8dc6dc795bf1a471f
                  MATRIX: 1038->2
                  LINES: 33->3
                  -- GENERATED --
              */
          