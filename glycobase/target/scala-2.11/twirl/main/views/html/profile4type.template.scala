
package views.html

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._
/*1.2*/import sparql._

object profile4type extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template3[Int,String,List[SparqlEntity],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*3.2*/(count: Int, expType: String, x: List[SparqlEntity] ):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*3.55*/("""


"""),_display_(/*6.2*/main/*6.6*/ {_display_(Seq[Any](format.raw/*6.8*/("""

"""),format.raw/*8.1*/("""<h1>"""),_display_(/*8.6*/count),format.raw/*8.11*/(""" """),format.raw/*8.12*/("""profiles found under: """),_display_(/*8.35*/expType),format.raw/*8.42*/("""</h1>



"""),_display_(/*12.2*/if(count>0)/*12.13*/{_display_(Seq[Any](format.raw/*12.14*/("""
"""),format.raw/*13.1*/("""<table class="table table-striped">
    <thead><tr>
        <th>Profile</th>
    </tr></thead>
    <tbody>
    """),_display_(/*18.6*/for(entry <-x) yield /*18.20*/{_display_(Seq[Any](format.raw/*18.21*/("""
    """),format.raw/*19.5*/("""<tr><td>
        <a href="""),_display_(/*20.18*/routes/*20.24*/.Application.searchProfile(entry.getValue("ProfileURI").split("/")(4))),format.raw/*20.94*/(""">"""),_display_(/*20.96*/entry/*20.101*/.getValue("ProfileURI").split("/")/*20.135*/(4)),format.raw/*20.138*/("""</a>
    </td></tr>""")))}),format.raw/*21.16*/("""
    """),format.raw/*22.5*/("""</tbody>

</table>

""")))}),format.raw/*26.2*/("""


""")))}))
      }
    }
  }

  def render(count:Int,expType:String,x:List[SparqlEntity]): play.twirl.api.HtmlFormat.Appendable = apply(count,expType,x)

  def f:((Int,String,List[SparqlEntity]) => play.twirl.api.HtmlFormat.Appendable) = (count,expType,x) => apply(count,expType,x)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Mon Feb 26 14:39:19 AEST 2018
                  SOURCE: /Users/s2980369/IdeaProjects/glycobase/glycobase/app/views/profile4type.scala.html
                  HASH: 49711bc69f55b14ee49daa2564a54a7c7af4db59
                  MATRIX: 651->1|1001->19|1149->72|1178->76|1189->80|1227->82|1255->84|1285->89|1310->94|1338->95|1387->118|1414->125|1450->135|1470->146|1509->147|1537->148|1675->260|1705->274|1744->275|1776->280|1829->306|1844->312|1935->382|1964->384|1979->389|2023->423|2048->426|2099->446|2131->451|2182->472
                  LINES: 24->1|29->3|34->3|37->6|37->6|37->6|39->8|39->8|39->8|39->8|39->8|39->8|43->12|43->12|43->12|44->13|49->18|49->18|49->18|50->19|51->20|51->20|51->20|51->20|51->20|51->20|51->20|52->21|53->22|57->26
                  -- GENERATED --
              */
          