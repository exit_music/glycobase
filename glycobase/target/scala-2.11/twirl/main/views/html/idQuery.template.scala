
package views.html

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._
/*2.2*/import views.html.helper._

object idQuery extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template0[play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply():play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](_display_(/*5.2*/main/*5.6*/ {_display_(Seq[Any](format.raw/*5.8*/("""

"""),format.raw/*7.1*/("""<h1>Type GlycoBase id to search: </h1>

"""),_display_(/*9.2*/helper/*9.8*/.form(action = routes.Application.getID())/*9.50*/ {_display_(Seq[Any](format.raw/*9.52*/("""

"""),format.raw/*11.1*/("""<label for="id">ID: </label>
<input type="text" name="glycobaseID" id="id"/><br/>

<input type="submit" value="OK" />
""")))}),format.raw/*15.2*/("""

""")))}))
      }
    }
  }

  def render(): play.twirl.api.HtmlFormat.Appendable = apply()

  def f:(() => play.twirl.api.HtmlFormat.Appendable) = () => apply()

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Mon Feb 26 14:39:19 AEST 2018
                  SOURCE: /Users/s2980369/IdeaProjects/glycobase/glycobase/app/views/idQuery.scala.html
                  HASH: 93a5c3b1dcdf1ea179e10d70b35093c75517712a
                  MATRIX: 651->2|1066->32|1077->36|1115->38|1143->40|1209->81|1222->87|1272->129|1311->131|1340->133|1489->252
                  LINES: 24->2|34->5|34->5|34->5|36->7|38->9|38->9|38->9|38->9|40->11|44->15
                  -- GENERATED --
              */
          