
package views.html

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._
/*1.2*/import sparql._

object query extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template5[List[SparqlEntity],List[SparqlEntity],List[SparqlEntity],List[SparqlEntity],List[SparqlEntity],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*3.2*/(sampleList: List[SparqlEntity], proteinList: List[SparqlEntity], reportList: List[SparqlEntity], tissueList: List[SparqlEntity], taxonList: List[SparqlEntity]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*3.162*/("""

    """),_display_(/*5.6*/main/*5.10*/ {_display_(Seq[Any](format.raw/*5.12*/("""
        """),format.raw/*12.17*/("""
        """),format.raw/*13.9*/("""<div class="section">
            <div class="container" >
                <div class="row">
                    <div class="col-md-12">
                        <div class="page-header">
                            <div class="pull-left">
                                <h1 class="info-header-grey text-primary">Search GlycoStore</h1>
                            </div>
                            <div class="pull-right">
                                <div class="text-right brand"></div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                        <div class="row">
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#rt" aria-controls="home" role="tab" data-toggle="tab">
                                    Technology Platform</a></li>
                                <li role="presentation"><a href="#sample" aria-controls="data" role="tab" data-toggle="tab">
                                    Sample Name</a></li>
                                <li role="presentation"><a href="#glycoprotein" aria-controls="data" role="tab" data-toggle="tab">
                                    Glycoprotein</a></li>
                                <li role="presentation"><a href="#report" aria-controls="profile" role="tab" data-toggle="tab">
                                    Report Name</a></li>
                                <li role="presentation"><a href="#tissue" aria-controls="profile" role="tab" data-toggle="tab">
                                    Tissue</a></li>
                                <li role="presentation"><a href="#taxonomy" aria-controls="profile" role="tab" data-toggle="tab">
                                    Taxonomy</a></li>
                            </ul>
                            <div class="tab-content">

                                <div role="tabpanel" class="tab-pane active" id="rt">

                                """),_display_(/*46.34*/helper/*46.40*/.form(action = routes.Application.getGuRange(), 'class -> "form-inline")/*46.112*/ {_display_(Seq[Any](format.raw/*46.114*/("""
                                    """),format.raw/*47.37*/("""<h4>Search for structures with a matching retention range</h4>
                                    <p>Data collections available include:</p>
                                    <ul>
                                        <li>HPLC and UPLC with 2-aminobenzamide label</li>
                                        <li>UPLC with 2-AB or Procainamide label</li>
                                        <li>RPUPLC using 2-AB</li>
                                        <li>Capillary electrophoresis, samples labeled with APTS</li>
                                        <li>Reduced glycans run on Porous graphitic carbon</li>
                                    </ul>
                                    <br/>
                                    <div class="form-group">
                                        <label for="name">Experiment Type: </label>
                                        <select name="type" id="name">
                                            <option>HPLC</option>
                                            <option>UPLC_2-AB</option>
                                            <option>UPLC_Procainamide</option>
                                            <option>RPUPLC</option>
                                            <option>CE</option>
                                            <option>PGC</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="min">Minimum value: </label>
                                        <input type="text" name="Minimum" id="min"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="max">Maximum value: </label>
                                        <input type="text" name="Maximum" id="max"/>
                                    </div>

                                    <button type="submit" class="btn-sm btn-eblue">Go</button>

                                """)))}),format.raw/*79.34*/("""
                                """),format.raw/*80.33*/("""</div>

                                <div role="tabpanel" class="tab-pane" id="sample">

                                    <h4>Search By Sample</h4>
                                    """),_display_(/*85.38*/helper/*85.44*/.form(action = routes.Application.getSampleName(), 'class -> "form-inline")/*85.119*/ {_display_(Seq[Any](format.raw/*85.121*/("""

                                        """),format.raw/*87.41*/("""<label for="name">Sample Name: </label>
                                        <select name="SampleName" id="name" >
                                        """),_display_(/*89.42*/for(sample <- sampleList) yield /*89.67*/ {_display_(Seq[Any](format.raw/*89.69*/("""
                                            """),format.raw/*90.45*/("""<option>"""),_display_(/*90.54*/sample/*90.60*/.getValue("SampleName")),format.raw/*90.83*/("""</option>>
                                        """)))}),format.raw/*91.42*/("""</select>

                                        <input type="submit" value="OK" />
                                    """)))}),format.raw/*94.38*/("""

                                """),format.raw/*96.33*/("""</div>

                                <div role="tabpanel" class="tab-pane" id="glycoprotein">

                                    <h4>Search By Protein</h4>
                                    """),_display_(/*101.38*/helper/*101.44*/.form(action = routes.Application.getProtein(), 'class -> "form-inline")/*101.116*/ {_display_(Seq[Any](format.raw/*101.118*/("""

                                        """),format.raw/*103.41*/("""<label for="name">Protein Name: </label>
                                        <select name="ProteinName" id="name" >
                                        """),_display_(/*105.42*/for(protein <- proteinList) yield /*105.69*/ {_display_(Seq[Any](format.raw/*105.71*/("""
                                            """),format.raw/*106.45*/("""<option>"""),_display_(/*106.54*/protein/*106.61*/.getValue("ProteinName")),format.raw/*106.85*/("""</option>>
                                        """)))}),format.raw/*107.42*/("""</select>

                                        <input type="submit" value="OK" />
                                    """)))}),format.raw/*110.38*/("""

                                """),format.raw/*112.33*/("""</div>


                                """),format.raw/*138.35*/("""

                                """),format.raw/*140.33*/("""<div role="tabpanel" class="tab-pane" id="report">

                                    <h4>Choose Report name to search: </h4>

                                    """),_display_(/*144.38*/helper/*144.44*/.form(action = routes.Application.getReportName(), 'class -> "form-inline")/*144.119*/ {_display_(Seq[Any](format.raw/*144.121*/("""

                                        """),format.raw/*146.41*/("""<label for="name">Report: </label>
                                        <select name="report" id="name">
                                        """),_display_(/*148.42*/for(report <- reportList) yield /*148.67*/ {_display_(Seq[Any](format.raw/*148.69*/("""
                                            """),format.raw/*149.45*/("""<option>"""),_display_(/*149.54*/report/*149.60*/.getValue("ReportName")),format.raw/*149.83*/("""</option>
                                        """)))}),format.raw/*150.42*/("""</select>

                                        <input type="submit" value="OK" />
                                    """)))}),format.raw/*153.38*/("""


                                """),format.raw/*156.33*/("""</div>

                                <div role="tabpanel" class="tab-pane" id="tissue">

                                    <h4>Search By Tissue</h4>

                                    """),_display_(/*162.38*/helper/*162.44*/.form(action = routes.Application.getTissueName(), 'class -> "form-inline")/*162.119*/ {_display_(Seq[Any](format.raw/*162.121*/("""

                                        """),format.raw/*164.41*/("""<label for="name">Tissue: </label>
                                        <select name="tissue" id="name">
                                        """),_display_(/*166.42*/for(tissue <- tissueList) yield /*166.67*/ {_display_(Seq[Any](format.raw/*166.69*/("""
                                            """),format.raw/*167.45*/("""<option>"""),_display_(/*167.54*/tissue/*167.60*/.getValue("Tissue")),format.raw/*167.79*/("""</option>
                                        """)))}),format.raw/*168.42*/("""</select>

                                        <input type="submit" value="OK" />
                                    """)))}),format.raw/*171.38*/("""

                                """),format.raw/*173.33*/("""</div>


                                <div role="tabpanel" class="tab-pane" id="taxonomy">

                                    <h4>Search By Taxonomy</h4>

                                    """),_display_(/*180.38*/helper/*180.44*/.form(action = routes.Application.getTaxonName(), 'class -> "form-inline")/*180.118*/ {_display_(Seq[Any](format.raw/*180.120*/("""

                                        """),format.raw/*182.41*/("""<label for="name">Taxonomy: </label>
                                        <select name="TaxonName" id="name">
                                        """),_display_(/*184.42*/for(taxon <- taxonList) yield /*184.65*/ {_display_(Seq[Any](format.raw/*184.67*/("""
                                            """),format.raw/*185.45*/("""<option>"""),_display_(/*185.54*/taxon/*185.59*/.getValue("Taxon").toLowerCase.capitalize),format.raw/*185.100*/("""</option>
                                        """)))}),format.raw/*186.42*/("""</select>

                                        <input type="submit" value="OK" />
                                    """)))}),format.raw/*189.38*/("""
                                """),format.raw/*190.33*/("""</div>

                                <div role="tabpanel" class="tab-pane" id="name">

                                    <h4>Write glycan name in uoxf format: </h4>

                                    """),_display_(/*196.38*/helper/*196.44*/.form(action = routes.Application.getUoxf(), 'class -> "form-inline")/*196.113*/ {_display_(Seq[Any](format.raw/*196.115*/("""

                                        """),format.raw/*198.41*/("""<label for="name">UOXF: </label>
                                        <input type="text" name="uoxf" id="name"/>
                                        <br/>

                                        <input type="submit" value="OK" />
                                    """)))}),format.raw/*203.38*/("""
                                """),format.raw/*204.33*/("""</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    """)))}))
      }
    }
  }

  def render(sampleList:List[SparqlEntity],proteinList:List[SparqlEntity],reportList:List[SparqlEntity],tissueList:List[SparqlEntity],taxonList:List[SparqlEntity]): play.twirl.api.HtmlFormat.Appendable = apply(sampleList,proteinList,reportList,tissueList,taxonList)

  def f:((List[SparqlEntity],List[SparqlEntity],List[SparqlEntity],List[SparqlEntity],List[SparqlEntity]) => play.twirl.api.HtmlFormat.Appendable) = (sampleList,proteinList,reportList,tissueList,taxonList) => apply(sampleList,proteinList,reportList,tissueList,taxonList)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Mon Feb 26 14:39:19 AEST 2018
                  SOURCE: /Users/s2980369/IdeaProjects/glycobase/glycobase/app/views/query.scala.html
                  HASH: 31faf99f0aefac7f0fd7707f10084ce6a874a2bf
                  MATRIX: 651->1|1059->19|1315->179|1347->186|1359->190|1398->192|1435->365|1471->374|3582->2458|3597->2464|3679->2536|3720->2538|3785->2575|5917->4676|5978->4709|6196->4900|6211->4906|6296->4981|6337->4983|6407->5025|6593->5184|6634->5209|6674->5211|6747->5256|6783->5265|6798->5271|6842->5294|6925->5346|7079->5469|7141->5503|7367->5701|7383->5707|7466->5779|7508->5781|7579->5823|7768->5984|7812->6011|7853->6013|7927->6058|7964->6067|7981->6074|8027->6098|8111->6150|8266->6273|8329->6307|8399->7537|8462->7571|8656->7737|8672->7743|8758->7818|8800->7820|8871->7862|9048->8011|9090->8036|9131->8038|9205->8083|9242->8092|9258->8098|9303->8121|9386->8172|9541->8295|9605->8330|9825->8522|9841->8528|9927->8603|9969->8605|10040->8647|10217->8796|10259->8821|10300->8823|10374->8868|10411->8877|10427->8883|10468->8902|10551->8953|10706->9076|10769->9110|10994->9307|11010->9313|11095->9387|11137->9389|11208->9431|11390->9585|11430->9608|11471->9610|11545->9655|11582->9664|11597->9669|11661->9710|11744->9761|11899->9884|11961->9917|12197->10125|12213->10131|12293->10200|12335->10202|12406->10244|12713->10519|12775->10552
                  LINES: 24->1|29->3|34->3|36->5|36->5|36->5|37->12|38->13|71->46|71->46|71->46|71->46|72->47|104->79|105->80|110->85|110->85|110->85|110->85|112->87|114->89|114->89|114->89|115->90|115->90|115->90|115->90|116->91|119->94|121->96|126->101|126->101|126->101|126->101|128->103|130->105|130->105|130->105|131->106|131->106|131->106|131->106|132->107|135->110|137->112|140->138|142->140|146->144|146->144|146->144|146->144|148->146|150->148|150->148|150->148|151->149|151->149|151->149|151->149|152->150|155->153|158->156|164->162|164->162|164->162|164->162|166->164|168->166|168->166|168->166|169->167|169->167|169->167|169->167|170->168|173->171|175->173|182->180|182->180|182->180|182->180|184->182|186->184|186->184|186->184|187->185|187->185|187->185|187->185|188->186|191->189|192->190|198->196|198->196|198->196|198->196|200->198|205->203|206->204
                  -- GENERATED --
              */
          