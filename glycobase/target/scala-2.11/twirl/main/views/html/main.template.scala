
package views.html

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._

object main extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[Html,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(content: Html):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.17*/("""
"""),format.raw/*2.1*/("""<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
        <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
        <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" media="screen" href=""""),_display_(/*9.70*/routes/*9.76*/.Assets.versioned("stylesheets/glycostore.css")),format.raw/*9.123*/("""">

        """),format.raw/*18.11*/("""

    """),format.raw/*20.5*/("""</head>
    <body>
        <div class="navbar-default navbar-static-top navbar" id="menu">
            <div class="container">
                """),format.raw/*24.83*/("""

                """),format.raw/*26.17*/("""<div class="collapse navbar-collapse" id="navbar-ex-collapse">
                    <a class="navbar-brand" href="/">GlycoStore</a>
                    """),format.raw/*28.78*/("""

                    """),format.raw/*30.21*/("""<form class="navbar-form navbar-right animated" action="/searchFilter" method="get">
                        <div class="input-group">
                            <span class="input-group-btn">
                                <select class="form-control" name="type" id="name">
                                    <option>Name</option>
                                    <option>HPLC</option>
                                    <option>UPLC</option>
                                    <option>UPLC_2-AB</option>
                                    <option>UPLC_Procainamide</option>
                                    <option>CE</option>
                                    <option>RP</option>
                                    <option>PGC</option>
                                </select></span>
                            <input type="text" class="form-control" placeholder="Quick Search" id="action234" name="search">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="submit">
                                    <span class="fa fa-search"></span>
                                </button></span>
                        </div>
                    </form>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                            <a href="#" data-toggle="dropdown" class="dropdown-toggle">Show All <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                """),format.raw/*56.78*/("""
                                """),format.raw/*57.33*/("""<li><a href="/references">References</a></li>
                                <li><a href="/collections">Collections</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" data-toggle="dropdown" class="dropdown-toggle">Search <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="/search">Retention Times/Units</a></li>
                                <li><a href="/glys3">GlycoCT Structure</a></li>
                                <li><a href="http://115.146.88.26">Structure Motif and Mass</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" data-toggle="dropdown" class="dropdown-toggle">About <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="https://unicarbkb.freshdesk.com/support/home">Help</a></li>
                                <li><a href="/about">Statistics</a></li>
                                <li><a href="/about">Team & Citing GlycoStore</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>


        """),_display_(/*83.10*/content),format.raw/*83.17*/("""

        """),format.raw/*85.9*/("""<footer class="navbar-fixed-bottom section section-primary">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <h3>GlycoStore</h3>
                        <p>This project would not have been possible with out funding from the Institute for Glycomics,
                            A*STAR’s Joint Council Office (JCO) through Visiting Investigator
                            Programme (“HighGlycoART”, A*STAR’s Biomedical Research Council
                            through the Strategic Positioning Fund (“GlycoSing”), NeCTAR,
                            ARC LIEF, and Macquarie-Ludger Pilot Scheme. <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-nd/4.0/88x31.png" /></a><br /></p>
                    </div>
                    <div class="col-sm-6"> <p class="text-info text-right"> <br><br></p>
                        <div class="row">
                        </div>
                        <div class="row">
                            <div class="col-md-12 hidden-xs text-right">
                                <a href="#"><i class="fa fa-2x fa-fw fa-twitter text-inverse"></i></a>
                                <a href="#"><i class="fa fa-2x fa-fw fa-github text-inverse"></i></a>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>



    </body>

    <script>
            (function (i, s, o, g, r, a, m) """),format.raw/*116.45*/("""{"""),format.raw/*116.46*/("""
                """),format.raw/*117.17*/("""i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () """),format.raw/*118.44*/("""{"""),format.raw/*118.45*/("""
                            """),format.raw/*119.29*/("""(i[r].q = i[r].q || []).push(arguments)
                        """),format.raw/*120.25*/("""}"""),format.raw/*120.26*/(""", i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            """),format.raw/*126.13*/("""}"""),format.raw/*126.14*/(""")(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-74498737-1', 'auto');
            ga('send', 'pageview');

    </script>

</html>


"""))
      }
    }
  }

  def render(content:Html): play.twirl.api.HtmlFormat.Appendable = apply(content)

  def f:((Html) => play.twirl.api.HtmlFormat.Appendable) = (content) => apply(content)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Mon Feb 26 14:39:19 AEST 2018
                  SOURCE: /Users/s2980369/IdeaProjects/glycobase/glycobase/app/views/main.scala.html
                  HASH: c2f8834ebcd5695990d89b24d937e628286e94eb
                  MATRIX: 945->1|1055->16|1082->17|1682->591|1696->597|1764->644|1804->902|1837->908|2008->1117|2054->1135|2233->1343|2283->1365|3874->3048|3935->3081|5346->4465|5374->4472|5411->4482|7080->6122|7110->6123|7156->6140|7260->6215|7290->6216|7348->6245|7441->6309|7471->6310|7741->6551|7771->6552
                  LINES: 28->1|33->1|34->2|41->9|41->9|41->9|43->18|45->20|49->24|51->26|53->28|55->30|80->56|81->57|107->83|107->83|109->85|140->116|140->116|141->117|142->118|142->118|143->119|144->120|144->120|150->126|150->126
                  -- GENERATED --
              */
          