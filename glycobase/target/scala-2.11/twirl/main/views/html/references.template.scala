
package views.html

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._
/*1.2*/import sparql._

object references extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template2[Int,List[SparqlEntity],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*3.2*/(count: Int, x: List[SparqlEntity]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*3.37*/("""


    """),_display_(/*6.6*/main/*6.10*/ {_display_(Seq[Any](format.raw/*6.12*/("""
        """),format.raw/*13.17*/("""
        """),format.raw/*14.9*/("""<div class="section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="page-header">
                            <div class="pull-left">
                                <h1 class="info-header-grey text-primary">References</h1>
                            </div>
                            <div class="pull-right">
                                <div class="text-right brand"></div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    """),_display_(/*28.22*/if(count > 0)/*28.35*/ {_display_(Seq[Any](format.raw/*28.37*/("""

                        """),format.raw/*30.25*/("""<table class="table table-striped">
                            <thead><tr>
                                <th>Title</th>
                                <th>Author</th>
                                <th>Year</th>
                                <th>Journal</th>
                            </tr></thead>
                            <tbody>
                            """),_display_(/*38.30*/for(entry <- x) yield /*38.45*/ {_display_(Seq[Any](format.raw/*38.47*/("""
                                """),format.raw/*39.33*/("""<tr>
                                    <td><a href='"""),_display_(/*40.51*/routes/*40.57*/.Application.searchReference(entry.getValue("PaperTitle"))),format.raw/*40.115*/("""'>"""),_display_(/*40.118*/entry/*40.123*/.getValue("PaperTitle")),format.raw/*40.146*/("""</a></td>
                                    <td>"""),_display_(/*41.42*/entry/*41.47*/.getValue("PaperAuthor")),format.raw/*41.71*/("""</td>
                                    <td>"""),_display_(/*42.42*/entry/*42.47*/.getValue("PaperYear")),format.raw/*42.69*/("""</td>
                                    <td>"""),_display_(/*43.42*/entry/*43.47*/.getValue("JournalName")),format.raw/*43.71*/("""</td>
                                </tr>
                            """)))}),format.raw/*45.30*/("""</tbody>

                        </table>

                    """)))}),format.raw/*49.22*/("""
                """),format.raw/*50.17*/("""</div>
            </div>
        </div>


    """)))}))
      }
    }
  }

  def render(count:Int,x:List[SparqlEntity]): play.twirl.api.HtmlFormat.Appendable = apply(count,x)

  def f:((Int,List[SparqlEntity]) => play.twirl.api.HtmlFormat.Appendable) = (count,x) => apply(count,x)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Mon Feb 26 14:39:19 AEST 2018
                  SOURCE: /Users/s2980369/IdeaProjects/glycobase/glycobase/app/views/references.scala.html
                  HASH: cd2c10e4abb0b930d6d821b9064193dcd13abf57
                  MATRIX: 651->1|992->19|1122->54|1155->62|1167->66|1206->68|1243->234|1279->243|1962->899|1984->912|2024->914|2078->940|2478->1313|2509->1328|2549->1330|2610->1363|2692->1418|2707->1424|2787->1482|2818->1485|2833->1490|2878->1513|2956->1564|2970->1569|3015->1593|3089->1640|3103->1645|3146->1667|3220->1714|3234->1719|3279->1743|3383->1816|3479->1881|3524->1898
                  LINES: 24->1|29->3|34->3|37->6|37->6|37->6|38->13|39->14|53->28|53->28|53->28|55->30|63->38|63->38|63->38|64->39|65->40|65->40|65->40|65->40|65->40|65->40|66->41|66->41|66->41|67->42|67->42|67->42|68->43|68->43|68->43|70->45|74->49|75->50
                  -- GENERATED --
              */
          