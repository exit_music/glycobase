
package views.html

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._

object guQuery extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[Form[GuRangeData],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(formGu: Form[GuRangeData]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {
/*2.2*/import views.html.helper._


Seq[Any](format.raw/*1.29*/("""
"""),format.raw/*3.1*/("""
"""),_display_(/*4.2*/main/*4.6*/ {_display_(Seq[Any](format.raw/*4.8*/("""

    """),format.raw/*6.5*/("""<h1>Write desired Gu/Au Range</h1>

    """),_display_(/*8.6*/helper/*8.12*/.form(action = routes.Application.getGuRange())/*8.59*/ {_display_(Seq[Any](format.raw/*8.61*/("""

        """),format.raw/*10.9*/("""<label for="name">Experiment Type: </label>
        <select name="type" id="name">
            <option>HPLC</option>
            <option>UPLC_2-AB</option>
            <option>UPLC_Procainamide</option>
            <option>RPUPLC</option>
            <option>CE</option>
        </select><br/>
        <label for="min">Minimum value: </label>
        <input type="text" name="Minimum" id="min"/><br/>
        <label for="max">Maximum value: </label>
        <input type="text" name="Maximum" id="max"/><br/>

        """),format.raw/*25.25*/("""

        """),format.raw/*27.9*/("""<input type="submit" value="OK" />
    """)))}),format.raw/*28.6*/("""

""")))}))
      }
    }
  }

  def render(formGu:Form[GuRangeData]): play.twirl.api.HtmlFormat.Appendable = apply(formGu)

  def f:((Form[GuRangeData]) => play.twirl.api.HtmlFormat.Appendable) = (formGu) => apply(formGu)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Mon Feb 26 14:39:19 AEST 2018
                  SOURCE: /Users/s2980369/IdeaProjects/glycobase/glycobase/app/views/guQuery.scala.html
                  HASH: 72282c3f5705b525872ed6f5c8607c5e79ded292
                  MATRIX: 961->1|1061->30|1117->28|1144->57|1171->59|1182->63|1220->65|1252->71|1318->112|1332->118|1387->165|1426->167|1463->177|2008->805|2045->815|2115->855
                  LINES: 28->1|31->2|34->1|35->3|36->4|36->4|36->4|38->6|40->8|40->8|40->8|40->8|42->10|55->25|57->27|58->28
                  -- GENERATED --
              */
          