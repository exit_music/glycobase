
package views.html

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._
/*1.2*/import helper._
/*2.2*/import sparql._

object sampleQuery extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[List[SparqlEntity],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*4.2*/(sampleList:List[SparqlEntity]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*4.33*/("""

"""),_display_(/*6.2*/main/*6.6*/ {_display_(Seq[Any](format.raw/*6.8*/("""

    """),format.raw/*8.5*/("""<h1>Search By Sample</h1>
    """),_display_(/*9.6*/helper/*9.12*/.form(action = routes.Application.getSampleName())/*9.62*/ {_display_(Seq[Any](format.raw/*9.64*/("""

    """),format.raw/*11.5*/("""<label for="name">Sample Name: </label>
    <select name="SampleName" id="name" class="selectpicker">
        """),_display_(/*13.10*/for(sample <- sampleList) yield /*13.35*/{_display_(Seq[Any](format.raw/*13.36*/("""
        """),format.raw/*14.9*/("""<option>"""),_display_(/*14.18*/sample/*14.24*/.getValue("SampleName")),format.raw/*14.47*/("""</option>>
    """)))}),format.raw/*15.6*/("""</select>

    <input type="submit" value="OK" />
    """)))}),format.raw/*18.6*/("""

""")))}))
      }
    }
  }

  def render(sampleList:List[SparqlEntity]): play.twirl.api.HtmlFormat.Appendable = apply(sampleList)

  def f:((List[SparqlEntity]) => play.twirl.api.HtmlFormat.Appendable) = (sampleList) => apply(sampleList)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Mon Feb 26 14:39:19 AEST 2018
                  SOURCE: /Users/s2980369/IdeaProjects/glycobase/glycobase/app/views/sampleQuery.scala.html
                  HASH: 2b4ff5fafdca235e23a71c8d3e9238a7017a08db
                  MATRIX: 651->1|674->18|1012->36|1138->67|1166->70|1177->74|1215->76|1247->82|1303->113|1317->119|1375->169|1414->171|1447->177|1585->288|1626->313|1665->314|1701->323|1737->332|1752->338|1796->361|1842->377|1927->432
                  LINES: 24->1|25->2|30->4|35->4|37->6|37->6|37->6|39->8|40->9|40->9|40->9|40->9|42->11|44->13|44->13|44->13|45->14|45->14|45->14|45->14|46->15|49->18
                  -- GENERATED --
              */
          