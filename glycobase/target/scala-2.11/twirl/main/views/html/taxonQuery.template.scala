
package views.html

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._
/*1.2*/import sparql._

object taxonQuery extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[List[SparqlEntity],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*2.2*/(taxonList:List[SparqlEntity]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.32*/("""

"""),_display_(/*4.2*/main/*4.6*/ {_display_(Seq[Any](format.raw/*4.8*/("""

    """),format.raw/*6.5*/("""<h1>Search By Taxonomy</h1>

    """),_display_(/*8.6*/helper/*8.12*/.form(action = routes.Application.getTaxonName())/*8.61*/ {_display_(Seq[Any](format.raw/*8.63*/("""

        """),format.raw/*10.9*/("""<label for="name">Taxonomy: </label>
        <select name="TaxonName" id="name">
        """),_display_(/*12.10*/for(taxon <- taxonList) yield /*12.33*/{_display_(Seq[Any](format.raw/*12.34*/("""
            """),format.raw/*13.13*/("""<option>"""),_display_(/*13.22*/taxon/*13.27*/.getValue("Taxon").toLowerCase.capitalize),format.raw/*13.68*/("""</option>
        """)))}),format.raw/*14.10*/("""</select>

        <input type="submit" value="OK" />
    """)))}),format.raw/*17.6*/("""
""")))}))
      }
    }
  }

  def render(taxonList:List[SparqlEntity]): play.twirl.api.HtmlFormat.Appendable = apply(taxonList)

  def f:((List[SparqlEntity]) => play.twirl.api.HtmlFormat.Appendable) = (taxonList) => apply(taxonList)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Mon Feb 26 14:39:19 AEST 2018
                  SOURCE: /Users/s2980369/IdeaProjects/glycobase/glycobase/app/views/taxonQuery.scala.html
                  HASH: bd646fe9b609913ebd115c3472b8744692403d68
                  MATRIX: 651->1|988->18|1113->48|1141->51|1152->55|1190->57|1222->63|1281->97|1295->103|1352->152|1391->154|1428->164|1545->254|1584->277|1623->278|1664->291|1700->300|1714->305|1776->346|1826->365|1915->424
                  LINES: 24->1|29->2|34->2|36->4|36->4|36->4|38->6|40->8|40->8|40->8|40->8|42->10|44->12|44->12|44->12|45->13|45->13|45->13|45->13|46->14|49->17
                  -- GENERATED --
              */
          