
package views.html

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._
/*1.2*/import sparql._

object tissueQuery extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[List[SparqlEntity],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*2.2*/(tissueList:List[SparqlEntity]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.33*/("""

"""),_display_(/*4.2*/main/*4.6*/ {_display_(Seq[Any](format.raw/*4.8*/("""

"""),format.raw/*6.1*/("""<h1>Search By Tissue</h1>

"""),_display_(/*8.2*/helper/*8.8*/.form(action = routes.Application.getTissueName())/*8.58*/ {_display_(Seq[Any](format.raw/*8.60*/("""

"""),format.raw/*10.1*/("""<label for="name">Tissue: </label>
<select name="tissue" id="name">
    """),_display_(/*12.6*/for(tissue <- tissueList) yield /*12.31*/{_display_(Seq[Any](format.raw/*12.32*/("""
    """),format.raw/*13.5*/("""<option>"""),_display_(/*13.14*/tissue/*13.20*/.getValue("Tissue")),format.raw/*13.39*/("""</option>
""")))}),format.raw/*14.2*/("""</select>

<input type="submit" value="OK" />
""")))}),format.raw/*17.2*/("""
""")))}))
      }
    }
  }

  def render(tissueList:List[SparqlEntity]): play.twirl.api.HtmlFormat.Appendable = apply(tissueList)

  def f:((List[SparqlEntity]) => play.twirl.api.HtmlFormat.Appendable) = (tissueList) => apply(tissueList)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Mon Feb 26 14:39:19 AEST 2018
                  SOURCE: /Users/s2980369/IdeaProjects/glycobase/glycobase/app/views/tissueQuery.scala.html
                  HASH: 4a848fc8c9cb201a50f596b51834ee62d339970a
                  MATRIX: 651->1|989->18|1115->49|1143->52|1154->56|1192->58|1220->60|1273->88|1286->94|1344->144|1383->146|1412->148|1511->221|1552->246|1591->247|1623->252|1659->261|1674->267|1714->286|1755->297|1832->344
                  LINES: 24->1|29->2|34->2|36->4|36->4|36->4|38->6|40->8|40->8|40->8|40->8|42->10|44->12|44->12|44->12|45->13|45->13|45->13|45->13|46->14|49->17
                  -- GENERATED --
              */
          