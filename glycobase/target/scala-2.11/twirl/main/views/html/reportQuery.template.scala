
package views.html

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._
/*1.2*/import sparql._
/*2.2*/import views.html.helper._

object reportQuery extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[List[SparqlEntity],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*4.2*/(ReportList:List[SparqlEntity]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*4.33*/("""
"""),_display_(/*5.2*/main/*5.6*/ {_display_(Seq[Any](format.raw/*5.8*/("""

"""),format.raw/*7.1*/("""<h1>Choose Report name to search: </h1>

"""),_display_(/*9.2*/helper/*9.8*/.form(action = routes.Application.getReportName())/*9.58*/ {_display_(Seq[Any](format.raw/*9.60*/("""

"""),format.raw/*11.1*/("""<label for="name">Report: </label>
<select name="report" id="name">
    """),_display_(/*13.6*/for(report <- ReportList) yield /*13.31*/{_display_(Seq[Any](format.raw/*13.32*/("""
    """),format.raw/*14.5*/("""<option>"""),_display_(/*14.14*/report/*14.20*/.getValue("ReportName")),format.raw/*14.43*/("""</option>
""")))}),format.raw/*15.2*/("""</select>

<input type="submit" value="OK" />
""")))}),format.raw/*18.2*/("""

""")))}))
      }
    }
  }

  def render(ReportList:List[SparqlEntity]): play.twirl.api.HtmlFormat.Appendable = apply(ReportList)

  def f:((List[SparqlEntity]) => play.twirl.api.HtmlFormat.Appendable) = (ReportList) => apply(ReportList)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Mon Feb 26 14:39:19 AEST 2018
                  SOURCE: /Users/s2980369/IdeaProjects/glycobase/glycobase/app/views/reportQuery.scala.html
                  HASH: 5eaa4793eb7ab6d8cc76566f6c54a156b70eea43
                  MATRIX: 651->1|674->18|1023->47|1149->78|1176->80|1187->84|1225->86|1253->88|1320->130|1333->136|1391->186|1430->188|1459->190|1558->263|1599->288|1638->289|1670->294|1706->303|1721->309|1765->332|1806->343|1883->390
                  LINES: 24->1|25->2|30->4|35->4|36->5|36->5|36->5|38->7|40->9|40->9|40->9|40->9|42->11|44->13|44->13|44->13|45->14|45->14|45->14|45->14|46->15|49->18
                  -- GENERATED --
              */
          