
package views.html

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._
/*1.2*/import sparql._

object glycoprotein extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[List[SparqlEntity],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*3.2*/(x: List[SparqlEntity] ):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*3.26*/("""


"""),_display_(/*6.2*/main/*6.6*/ {_display_(Seq[Any](format.raw/*6.8*/("""

    """),format.raw/*8.5*/("""<div class="hero">
        <div class="wrap">
            <div class="text">
                <strong>Glycoprotein Summary</strong>
            </div>
            <div class="minitext">Structures associated</div>
        </div>
    </div>

    <div class="section">
        <div class="container">
            """),format.raw/*25.21*/("""
            """),format.raw/*26.13*/("""<div class="row">
                <div class="col-md-9">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud </p>
                    <table class="table">
                        <thead><tr>
                            <th>Name (Oxford Style)</th>
                            <th>Structure</th>
                            <th>Protein</th>
                        </tr></thead>
                        <tbody>

                        """),_display_(/*37.26*/for(entry <-x) yield /*37.40*/{_display_(Seq[Any](format.raw/*37.41*/("""
                            """),format.raw/*38.29*/("""<tr>
                                <td><a href="""),_display_(/*39.46*/routes/*39.52*/.Application.showGlycan(entry.getValue("GlycoBaseId"))),format.raw/*39.106*/(""">"""),_display_(/*39.108*/entry/*39.113*/.getData.get("Uoxf")),format.raw/*39.133*/("""</a></td>
                                """),_display_(/*40.34*/if(session.get("notation"))/*40.61*/{_display_(Seq[Any](format.raw/*40.62*/("""
                                    """),format.raw/*41.37*/("""<td>
                                        <img src=""""),_display_(/*42.52*/routes/*42.58*/.Assets.versioned("images/sm-"+session.get("notation")+"/sm-"+session.get("notation")+"-" + entry.getValue("GlycoBaseId") +".png")),format.raw/*42.188*/(""""><br/>
                                """)))}/*43.35*/else/*43.39*/{_display_(Seq[Any](format.raw/*43.40*/("""
                                    """),format.raw/*44.37*/("""<td><img src=""""),_display_(/*44.52*/routes/*44.58*/.Assets.versioned("images/sm-cfg-uoxf/sm-cfg-uoxf-" + entry.getValue("GlycoBaseId") +".png")),format.raw/*44.150*/("""">
                                        """)))}),format.raw/*45.42*/("""
                                """),format.raw/*46.33*/("""</td>
                                <td>"""),_display_(/*47.38*/entry/*47.43*/.getValue("ProteinName")),format.raw/*47.67*/("""</td>

                            </tr>
                        """)))}),format.raw/*50.26*/("""</tbody>

                    </table>
                </div>
                <div class="col-md-3 sidebar">
                  """),_display_(/*55.20*/views/*55.25*/.html.format.format()),format.raw/*55.46*/("""
                """),format.raw/*56.17*/("""</div>
            </div>
        </div>
    </div>


""")))}))
      }
    }
  }

  def render(x:List[SparqlEntity]): play.twirl.api.HtmlFormat.Appendable = apply(x)

  def f:((List[SparqlEntity]) => play.twirl.api.HtmlFormat.Appendable) = (x) => apply(x)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Mon Feb 26 14:39:19 AEST 2018
                  SOURCE: /Users/s2980369/IdeaProjects/glycobase/glycobase/app/views/glycoprotein.scala.html
                  HASH: ca83495d2439b1313df491979735e4d623a94b46
                  MATRIX: 651->1|990->19|1109->43|1138->47|1149->51|1187->53|1219->59|1556->645|1597->658|2188->1222|2218->1236|2257->1237|2314->1266|2391->1316|2406->1322|2482->1376|2512->1378|2527->1383|2569->1403|2639->1446|2675->1473|2714->1474|2779->1511|2862->1567|2877->1573|3029->1703|3089->1745|3102->1749|3141->1750|3206->1787|3248->1802|3263->1808|3377->1900|3452->1944|3513->1977|3583->2020|3597->2025|3642->2049|3739->2115|3894->2243|3908->2248|3950->2269|3995->2286
                  LINES: 24->1|29->3|34->3|37->6|37->6|37->6|39->8|50->25|51->26|62->37|62->37|62->37|63->38|64->39|64->39|64->39|64->39|64->39|64->39|65->40|65->40|65->40|66->41|67->42|67->42|67->42|68->43|68->43|68->43|69->44|69->44|69->44|69->44|70->45|71->46|72->47|72->47|72->47|75->50|80->55|80->55|80->55|81->56
                  -- GENERATED --
              */
          