
package views.html

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._
/*2.2*/import views.html.helper._

object uoxfQuery extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template0[play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply():play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](_display_(/*5.2*/main/*5.6*/ {_display_(Seq[Any](format.raw/*5.8*/("""

"""),format.raw/*7.1*/("""<h1>Write glycan name in uoxf format: </h1>

"""),_display_(/*9.2*/helper/*9.8*/.form(action = routes.Application.getUoxf())/*9.52*/ {_display_(Seq[Any](format.raw/*9.54*/("""

"""),format.raw/*11.1*/("""<label for="name">UOXF: </label>
<input type="text" name="uoxf" id="name"/><br/>

<input type="submit" value="OK" />
""")))}),format.raw/*15.2*/("""

""")))}))
      }
    }
  }

  def render(): play.twirl.api.HtmlFormat.Appendable = apply()

  def f:(() => play.twirl.api.HtmlFormat.Appendable) = () => apply()

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Mon Feb 26 14:39:19 AEST 2018
                  SOURCE: /Users/s2980369/IdeaProjects/glycobase/glycobase/app/views/uoxfQuery.scala.html
                  HASH: 38d6a20a3a88b116e1e9f2efbf3c3fb8bc6411e0
                  MATRIX: 651->2|1068->32|1079->36|1117->38|1145->40|1216->86|1229->92|1281->136|1320->138|1349->140|1497->258
                  LINES: 24->2|34->5|34->5|34->5|36->7|38->9|38->9|38->9|38->9|40->11|44->15
                  -- GENERATED --
              */
          