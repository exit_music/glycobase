
// @GENERATOR:play-routes-compiler
// @SOURCE:/Users/s2980369/IdeaProjects/glycobase/glycobase/conf/routes
// @DATE:Mon Feb 26 14:39:19 AEST 2018

package router

import play.core.routing._
import play.core.routing.HandlerInvokerFactory._

import play.api.mvc._

import _root_.controllers.Assets.Asset
import _root_.play.libs.F

class Routes(
  override val errorHandler: play.api.http.HttpErrorHandler, 
  // @LINE:6
  Application_5: controllers.Application,
  // @LINE:12
  Collections_0: controllers.Collections,
  // @LINE:14
  BuildCollectionsJson_1: controllers.BuildCollectionsJson,
  // @LINE:16
  StructureSearch_3: controllers.StructureSearch,
  // @LINE:41
  Image_2: controllers.Image,
  // @LINE:75
  Assets_4: controllers.Assets,
  val prefix: String
) extends GeneratedRouter {

   @javax.inject.Inject()
   def this(errorHandler: play.api.http.HttpErrorHandler,
    // @LINE:6
    Application_5: controllers.Application,
    // @LINE:12
    Collections_0: controllers.Collections,
    // @LINE:14
    BuildCollectionsJson_1: controllers.BuildCollectionsJson,
    // @LINE:16
    StructureSearch_3: controllers.StructureSearch,
    // @LINE:41
    Image_2: controllers.Image,
    // @LINE:75
    Assets_4: controllers.Assets
  ) = this(errorHandler, Application_5, Collections_0, BuildCollectionsJson_1, StructureSearch_3, Image_2, Assets_4, "/")

  def withPrefix(prefix: String): Routes = {
    router.RoutesPrefix.setPrefix(prefix)
    new Routes(errorHandler, Application_5, Collections_0, BuildCollectionsJson_1, StructureSearch_3, Image_2, Assets_4, prefix)
  }

  private[this] val defaultPrefix: String = {
    if (this.prefix.endsWith("/")) "" else "/"
  }

  def documentation = List(
    ("""GET""", this.prefix, """controllers.Application.index()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """about""", """controllers.Application.about()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """references""", """controllers.Application.listReferences()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """structures""", """controllers.Application.listAllStructures()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """collections""", """controllers.Collections.listAllCollections()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """collectionsRef""", """controllers.Collections.getCollectionsRef()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """displayCollection/""" + "$" + """report<[^/]+>""", """controllers.BuildCollectionsJson.buildCollectionsJson(report:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """builder""", """controllers.StructureSearch.builder()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """searchGlycoCT""", """controllers.StructureSearch.searchGlycoct()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """searchGu/""" + "$" + """expType<[^/]+>""", """controllers.Application.searchGu(expType:String, minValue:String, maxValue:String, core:Integer, bisect:Integer, outer:Integer, hybrid:Integer, mannose:Integer)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """searchGuMotif/""" + "$" + """expType<[^/]+>""", """controllers.Application.searchGuMotif(expType:String, minValue:String, maxValue:String, core:Integer, bisect:Integer, outer:Integer, hybrid:Integer, mannose:Integer)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """searchGuMotifOlinked/""" + "$" + """expType<[^/]+>""", """controllers.Application.searchGuMotifOlinked(expType:String, minValue:String, maxValue:String, core1:Integer, core2:Integer, bloodgrouph:Integer, linearbloodgroupb:Integer, bloodgroupi:Integer, bloodgrouplewisa:Integer, bloodgrouplewisx:Integer, bloodgroupslewisx:Integer)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """glycan/""" + "$" + """uoxf<[^/]+>""", """controllers.Application.showGlycan(uoxf:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """searchReport/""" + "$" + """report<[^/]+>""", """controllers.Application.searchReport(report:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """searchTissue/""" + "$" + """tissue<[^/]+>""", """controllers.Application.searchTissue(tissue:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """searchSample/""" + "$" + """sample<[^/]+>""", """controllers.Application.searchSample(sample:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """searchTaxon/""" + "$" + """taxon<[^/]+>""", """controllers.Application.searchTaxon(taxon:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """searchProfile/""" + "$" + """profile<[^/]+>""", """controllers.Application.searchProfile(profile:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """searchID/""" + "$" + """id<[^/]+>""", """controllers.Application.searchID(id:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """searchUoxf/""" + "$" + """name<[^/]+>""", """controllers.Application.searchUoxf(name:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """profile/""" + "$" + """type<[^/]+>""", """controllers.Application.listProfie4Type(type:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """reference/""" + "$" + """title<[^/]+>""", """controllers.Application.searchReference(title:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """proteins/""" + "$" + """name<[^/]+>""", """controllers.Application.searchProtein(name:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """showImage""", """controllers.Image.showImage(structure:String, notation:String, extended:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """showImageReaction""", """controllers.Image.showImageReaction(glycobaseId:String, notation:String, extended:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """format/""" + "$" + """s<[^/]+>""", """controllers.Application.format(s:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """search""", """controllers.Application.queryoptions()"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """searchGU""", """controllers.Application.getGuRange()"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """searchSample""", """controllers.Application.getSampleName()"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """searchTaxon""", """controllers.Application.getTaxonName()"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """searchID""", """controllers.Application.getID()"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """searchUoxf""", """controllers.Application.getUoxf()"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """searchTissue""", """controllers.Application.getTissueName()"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """searchProfile""", """controllers.Application.getProfileID()"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """searchReport""", """controllers.Application.getReportName()"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """proteins""", """controllers.Application.getProtein()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """searchFilter""", """controllers.Application.searchFilter()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """showPgc""", """controllers.Application.showPgc()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """showPgcGlycan/""" + "$" + """id<[^/]+>""", """controllers.Application.showPgcGlycan(id:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """showImagePgc""", """controllers.Image.showImagePgc(glycostoreId:String, notation:String, extended:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """showImagePgcObjectStore""", """controllers.Image.showImagePgcObjectStore(glycostoreId:String, notation:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """showPgcData""", """controllers.Application.startShowPgcData()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """showPgcDataTemp""", """controllers.Application.showPgcDataTemp()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """glys3""", """controllers.StructureSearch.glys3()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """substructureSPARQLQuery""", """controllers.StructureSearch.SubstructureSPARQLQueryResult()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """images/""" + "$" + """file<.+>""", """controllers.Assets.at(path:String = "/public/images", file:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """assets/""" + "$" + """file<.+>""", """controllers.Assets.versioned(path:String = "/public", file:Asset)"""),
    Nil
  ).foldLeft(List.empty[(String,String,String)]) { (s,e) => e.asInstanceOf[Any] match {
    case r @ (_,_,_) => s :+ r.asInstanceOf[(String,String,String)]
    case l => s ++ l.asInstanceOf[List[(String,String,String)]]
  }}


  // @LINE:6
  private[this] lazy val controllers_Application_index0_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix)))
  )
  private[this] lazy val controllers_Application_index0_invoker = createInvoker(
    Application_5.index(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Application",
      "index",
      Nil,
      "GET",
      this.prefix + """""",
      """ Home page""",
      Seq()
    )
  )

  // @LINE:8
  private[this] lazy val controllers_Application_about1_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("about")))
  )
  private[this] lazy val controllers_Application_about1_invoker = createInvoker(
    Application_5.about(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Application",
      "about",
      Nil,
      "GET",
      this.prefix + """about""",
      """GET     /search                         controllers.Application.chooseQuery()""",
      Seq()
    )
  )

  // @LINE:9
  private[this] lazy val controllers_Application_listReferences2_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("references")))
  )
  private[this] lazy val controllers_Application_listReferences2_invoker = createInvoker(
    Application_5.listReferences(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Application",
      "listReferences",
      Nil,
      "GET",
      this.prefix + """references""",
      """""",
      Seq()
    )
  )

  // @LINE:11
  private[this] lazy val controllers_Application_listAllStructures3_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("structures")))
  )
  private[this] lazy val controllers_Application_listAllStructures3_invoker = createInvoker(
    Application_5.listAllStructures(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Application",
      "listAllStructures",
      Nil,
      "GET",
      this.prefix + """structures""",
      """GET     /proteins                       controllers.Application.listProteins()""",
      Seq()
    )
  )

  // @LINE:12
  private[this] lazy val controllers_Collections_listAllCollections4_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("collections")))
  )
  private[this] lazy val controllers_Collections_listAllCollections4_invoker = createInvoker(
    Collections_0.listAllCollections(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Collections",
      "listAllCollections",
      Nil,
      "GET",
      this.prefix + """collections""",
      """""",
      Seq()
    )
  )

  // @LINE:13
  private[this] lazy val controllers_Collections_getCollectionsRef5_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("collectionsRef")))
  )
  private[this] lazy val controllers_Collections_getCollectionsRef5_invoker = createInvoker(
    Collections_0.getCollectionsRef(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Collections",
      "getCollectionsRef",
      Nil,
      "GET",
      this.prefix + """collectionsRef""",
      """""",
      Seq()
    )
  )

  // @LINE:14
  private[this] lazy val controllers_BuildCollectionsJson_buildCollectionsJson6_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("displayCollection/"), DynamicPart("report", """[^/]+""",true)))
  )
  private[this] lazy val controllers_BuildCollectionsJson_buildCollectionsJson6_invoker = createInvoker(
    BuildCollectionsJson_1.buildCollectionsJson(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.BuildCollectionsJson",
      "buildCollectionsJson",
      Seq(classOf[String]),
      "GET",
      this.prefix + """displayCollection/""" + "$" + """report<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:16
  private[this] lazy val controllers_StructureSearch_builder7_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("builder")))
  )
  private[this] lazy val controllers_StructureSearch_builder7_invoker = createInvoker(
    StructureSearch_3.builder(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.StructureSearch",
      "builder",
      Nil,
      "GET",
      this.prefix + """builder""",
      """""",
      Seq()
    )
  )

  // @LINE:17
  private[this] lazy val controllers_StructureSearch_searchGlycoct8_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("searchGlycoCT")))
  )
  private[this] lazy val controllers_StructureSearch_searchGlycoct8_invoker = createInvoker(
    StructureSearch_3.searchGlycoct(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.StructureSearch",
      "searchGlycoct",
      Nil,
      "GET",
      this.prefix + """searchGlycoCT""",
      """""",
      Seq()
    )
  )

  // @LINE:21
  private[this] lazy val controllers_Application_searchGu9_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("searchGu/"), DynamicPart("expType", """[^/]+""",true)))
  )
  private[this] lazy val controllers_Application_searchGu9_invoker = createInvoker(
    Application_5.searchGu(fakeValue[String], fakeValue[String], fakeValue[String], fakeValue[Integer], fakeValue[Integer], fakeValue[Integer], fakeValue[Integer], fakeValue[Integer]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Application",
      "searchGu",
      Seq(classOf[String], classOf[String], classOf[String], classOf[Integer], classOf[Integer], classOf[Integer], classOf[Integer], classOf[Integer]),
      "GET",
      this.prefix + """searchGu/""" + "$" + """expType<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:22
  private[this] lazy val controllers_Application_searchGuMotif10_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("searchGuMotif/"), DynamicPart("expType", """[^/]+""",true)))
  )
  private[this] lazy val controllers_Application_searchGuMotif10_invoker = createInvoker(
    Application_5.searchGuMotif(fakeValue[String], fakeValue[String], fakeValue[String], fakeValue[Integer], fakeValue[Integer], fakeValue[Integer], fakeValue[Integer], fakeValue[Integer]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Application",
      "searchGuMotif",
      Seq(classOf[String], classOf[String], classOf[String], classOf[Integer], classOf[Integer], classOf[Integer], classOf[Integer], classOf[Integer]),
      "GET",
      this.prefix + """searchGuMotif/""" + "$" + """expType<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:24
  private[this] lazy val controllers_Application_searchGuMotifOlinked11_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("searchGuMotifOlinked/"), DynamicPart("expType", """[^/]+""",true)))
  )
  private[this] lazy val controllers_Application_searchGuMotifOlinked11_invoker = createInvoker(
    Application_5.searchGuMotifOlinked(fakeValue[String], fakeValue[String], fakeValue[String], fakeValue[Integer], fakeValue[Integer], fakeValue[Integer], fakeValue[Integer], fakeValue[Integer], fakeValue[Integer], fakeValue[Integer], fakeValue[Integer]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Application",
      "searchGuMotifOlinked",
      Seq(classOf[String], classOf[String], classOf[String], classOf[Integer], classOf[Integer], classOf[Integer], classOf[Integer], classOf[Integer], classOf[Integer], classOf[Integer], classOf[Integer]),
      "GET",
      this.prefix + """searchGuMotifOlinked/""" + "$" + """expType<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:29
  private[this] lazy val controllers_Application_showGlycan12_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("glycan/"), DynamicPart("uoxf", """[^/]+""",true)))
  )
  private[this] lazy val controllers_Application_showGlycan12_invoker = createInvoker(
    Application_5.showGlycan(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Application",
      "showGlycan",
      Seq(classOf[String]),
      "GET",
      this.prefix + """glycan/""" + "$" + """uoxf<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:30
  private[this] lazy val controllers_Application_searchReport13_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("searchReport/"), DynamicPart("report", """[^/]+""",true)))
  )
  private[this] lazy val controllers_Application_searchReport13_invoker = createInvoker(
    Application_5.searchReport(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Application",
      "searchReport",
      Seq(classOf[String]),
      "GET",
      this.prefix + """searchReport/""" + "$" + """report<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:31
  private[this] lazy val controllers_Application_searchTissue14_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("searchTissue/"), DynamicPart("tissue", """[^/]+""",true)))
  )
  private[this] lazy val controllers_Application_searchTissue14_invoker = createInvoker(
    Application_5.searchTissue(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Application",
      "searchTissue",
      Seq(classOf[String]),
      "GET",
      this.prefix + """searchTissue/""" + "$" + """tissue<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:32
  private[this] lazy val controllers_Application_searchSample15_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("searchSample/"), DynamicPart("sample", """[^/]+""",true)))
  )
  private[this] lazy val controllers_Application_searchSample15_invoker = createInvoker(
    Application_5.searchSample(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Application",
      "searchSample",
      Seq(classOf[String]),
      "GET",
      this.prefix + """searchSample/""" + "$" + """sample<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:33
  private[this] lazy val controllers_Application_searchTaxon16_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("searchTaxon/"), DynamicPart("taxon", """[^/]+""",true)))
  )
  private[this] lazy val controllers_Application_searchTaxon16_invoker = createInvoker(
    Application_5.searchTaxon(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Application",
      "searchTaxon",
      Seq(classOf[String]),
      "GET",
      this.prefix + """searchTaxon/""" + "$" + """taxon<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:34
  private[this] lazy val controllers_Application_searchProfile17_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("searchProfile/"), DynamicPart("profile", """[^/]+""",true)))
  )
  private[this] lazy val controllers_Application_searchProfile17_invoker = createInvoker(
    Application_5.searchProfile(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Application",
      "searchProfile",
      Seq(classOf[String]),
      "GET",
      this.prefix + """searchProfile/""" + "$" + """profile<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:35
  private[this] lazy val controllers_Application_searchID18_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("searchID/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_Application_searchID18_invoker = createInvoker(
    Application_5.searchID(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Application",
      "searchID",
      Seq(classOf[String]),
      "GET",
      this.prefix + """searchID/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:36
  private[this] lazy val controllers_Application_searchUoxf19_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("searchUoxf/"), DynamicPart("name", """[^/]+""",true)))
  )
  private[this] lazy val controllers_Application_searchUoxf19_invoker = createInvoker(
    Application_5.searchUoxf(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Application",
      "searchUoxf",
      Seq(classOf[String]),
      "GET",
      this.prefix + """searchUoxf/""" + "$" + """name<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:37
  private[this] lazy val controllers_Application_listProfie4Type20_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("profile/"), DynamicPart("type", """[^/]+""",true)))
  )
  private[this] lazy val controllers_Application_listProfie4Type20_invoker = createInvoker(
    Application_5.listProfie4Type(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Application",
      "listProfie4Type",
      Seq(classOf[String]),
      "GET",
      this.prefix + """profile/""" + "$" + """type<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:38
  private[this] lazy val controllers_Application_searchReference21_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("reference/"), DynamicPart("title", """[^/]+""",true)))
  )
  private[this] lazy val controllers_Application_searchReference21_invoker = createInvoker(
    Application_5.searchReference(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Application",
      "searchReference",
      Seq(classOf[String]),
      "GET",
      this.prefix + """reference/""" + "$" + """title<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:39
  private[this] lazy val controllers_Application_searchProtein22_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("proteins/"), DynamicPart("name", """[^/]+""",true)))
  )
  private[this] lazy val controllers_Application_searchProtein22_invoker = createInvoker(
    Application_5.searchProtein(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Application",
      "searchProtein",
      Seq(classOf[String]),
      "GET",
      this.prefix + """proteins/""" + "$" + """name<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:41
  private[this] lazy val controllers_Image_showImage23_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("showImage")))
  )
  private[this] lazy val controllers_Image_showImage23_invoker = createInvoker(
    Image_2.showImage(fakeValue[String], fakeValue[String], fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Image",
      "showImage",
      Seq(classOf[String], classOf[String], classOf[String]),
      "GET",
      this.prefix + """showImage""",
      """""",
      Seq()
    )
  )

  // @LINE:42
  private[this] lazy val controllers_Image_showImageReaction24_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("showImageReaction")))
  )
  private[this] lazy val controllers_Image_showImageReaction24_invoker = createInvoker(
    Image_2.showImageReaction(fakeValue[String], fakeValue[String], fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Image",
      "showImageReaction",
      Seq(classOf[String], classOf[String], classOf[String]),
      "GET",
      this.prefix + """showImageReaction""",
      """""",
      Seq()
    )
  )

  // @LINE:43
  private[this] lazy val controllers_Application_format25_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("format/"), DynamicPart("s", """[^/]+""",true)))
  )
  private[this] lazy val controllers_Application_format25_invoker = createInvoker(
    Application_5.format(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Application",
      "format",
      Seq(classOf[String]),
      "GET",
      this.prefix + """format/""" + "$" + """s<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:45
  private[this] lazy val controllers_Application_queryoptions26_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("search")))
  )
  private[this] lazy val controllers_Application_queryoptions26_invoker = createInvoker(
    Application_5.queryoptions(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Application",
      "queryoptions",
      Nil,
      "GET",
      this.prefix + """search""",
      """""",
      Seq()
    )
  )

  // @LINE:47
  private[this] lazy val controllers_Application_getGuRange27_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("searchGU")))
  )
  private[this] lazy val controllers_Application_getGuRange27_invoker = createInvoker(
    Application_5.getGuRange(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Application",
      "getGuRange",
      Nil,
      "POST",
      this.prefix + """searchGU""",
      """POST    /search                         controllers.Application.search()""",
      Seq()
    )
  )

  // @LINE:48
  private[this] lazy val controllers_Application_getSampleName28_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("searchSample")))
  )
  private[this] lazy val controllers_Application_getSampleName28_invoker = createInvoker(
    Application_5.getSampleName(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Application",
      "getSampleName",
      Nil,
      "POST",
      this.prefix + """searchSample""",
      """""",
      Seq()
    )
  )

  // @LINE:49
  private[this] lazy val controllers_Application_getTaxonName29_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("searchTaxon")))
  )
  private[this] lazy val controllers_Application_getTaxonName29_invoker = createInvoker(
    Application_5.getTaxonName(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Application",
      "getTaxonName",
      Nil,
      "POST",
      this.prefix + """searchTaxon""",
      """""",
      Seq()
    )
  )

  // @LINE:50
  private[this] lazy val controllers_Application_getID30_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("searchID")))
  )
  private[this] lazy val controllers_Application_getID30_invoker = createInvoker(
    Application_5.getID(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Application",
      "getID",
      Nil,
      "POST",
      this.prefix + """searchID""",
      """""",
      Seq()
    )
  )

  // @LINE:51
  private[this] lazy val controllers_Application_getUoxf31_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("searchUoxf")))
  )
  private[this] lazy val controllers_Application_getUoxf31_invoker = createInvoker(
    Application_5.getUoxf(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Application",
      "getUoxf",
      Nil,
      "POST",
      this.prefix + """searchUoxf""",
      """""",
      Seq()
    )
  )

  // @LINE:52
  private[this] lazy val controllers_Application_getTissueName32_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("searchTissue")))
  )
  private[this] lazy val controllers_Application_getTissueName32_invoker = createInvoker(
    Application_5.getTissueName(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Application",
      "getTissueName",
      Nil,
      "POST",
      this.prefix + """searchTissue""",
      """""",
      Seq()
    )
  )

  // @LINE:53
  private[this] lazy val controllers_Application_getProfileID33_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("searchProfile")))
  )
  private[this] lazy val controllers_Application_getProfileID33_invoker = createInvoker(
    Application_5.getProfileID(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Application",
      "getProfileID",
      Nil,
      "POST",
      this.prefix + """searchProfile""",
      """""",
      Seq()
    )
  )

  // @LINE:54
  private[this] lazy val controllers_Application_getReportName34_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("searchReport")))
  )
  private[this] lazy val controllers_Application_getReportName34_invoker = createInvoker(
    Application_5.getReportName(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Application",
      "getReportName",
      Nil,
      "POST",
      this.prefix + """searchReport""",
      """""",
      Seq()
    )
  )

  // @LINE:55
  private[this] lazy val controllers_Application_getProtein35_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("proteins")))
  )
  private[this] lazy val controllers_Application_getProtein35_invoker = createInvoker(
    Application_5.getProtein(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Application",
      "getProtein",
      Nil,
      "POST",
      this.prefix + """proteins""",
      """""",
      Seq()
    )
  )

  // @LINE:56
  private[this] lazy val controllers_Application_searchFilter36_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("searchFilter")))
  )
  private[this] lazy val controllers_Application_searchFilter36_invoker = createInvoker(
    Application_5.searchFilter(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Application",
      "searchFilter",
      Nil,
      "GET",
      this.prefix + """searchFilter""",
      """""",
      Seq()
    )
  )

  // @LINE:59
  private[this] lazy val controllers_Application_showPgc37_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("showPgc")))
  )
  private[this] lazy val controllers_Application_showPgc37_invoker = createInvoker(
    Application_5.showPgc(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Application",
      "showPgc",
      Nil,
      "GET",
      this.prefix + """showPgc""",
      """""",
      Seq()
    )
  )

  // @LINE:60
  private[this] lazy val controllers_Application_showPgcGlycan38_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("showPgcGlycan/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_Application_showPgcGlycan38_invoker = createInvoker(
    Application_5.showPgcGlycan(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Application",
      "showPgcGlycan",
      Seq(classOf[String]),
      "GET",
      this.prefix + """showPgcGlycan/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:61
  private[this] lazy val controllers_Image_showImagePgc39_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("showImagePgc")))
  )
  private[this] lazy val controllers_Image_showImagePgc39_invoker = createInvoker(
    Image_2.showImagePgc(fakeValue[String], fakeValue[String], fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Image",
      "showImagePgc",
      Seq(classOf[String], classOf[String], classOf[String]),
      "GET",
      this.prefix + """showImagePgc""",
      """""",
      Seq()
    )
  )

  // @LINE:62
  private[this] lazy val controllers_Image_showImagePgcObjectStore40_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("showImagePgcObjectStore")))
  )
  private[this] lazy val controllers_Image_showImagePgcObjectStore40_invoker = createInvoker(
    Image_2.showImagePgcObjectStore(fakeValue[String], fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Image",
      "showImagePgcObjectStore",
      Seq(classOf[String], classOf[String]),
      "GET",
      this.prefix + """showImagePgcObjectStore""",
      """""",
      Seq()
    )
  )

  // @LINE:66
  private[this] lazy val controllers_Application_startShowPgcData41_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("showPgcData")))
  )
  private[this] lazy val controllers_Application_startShowPgcData41_invoker = createInvoker(
    Application_5.startShowPgcData(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Application",
      "startShowPgcData",
      Nil,
      "GET",
      this.prefix + """showPgcData""",
      """""",
      Seq()
    )
  )

  // @LINE:67
  private[this] lazy val controllers_Application_showPgcDataTemp42_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("showPgcDataTemp")))
  )
  private[this] lazy val controllers_Application_showPgcDataTemp42_invoker = createInvoker(
    Application_5.showPgcDataTemp(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Application",
      "showPgcDataTemp",
      Nil,
      "GET",
      this.prefix + """showPgcDataTemp""",
      """""",
      Seq()
    )
  )

  // @LINE:71
  private[this] lazy val controllers_StructureSearch_glys343_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("glys3")))
  )
  private[this] lazy val controllers_StructureSearch_glys343_invoker = createInvoker(
    StructureSearch_3.glys3(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.StructureSearch",
      "glys3",
      Nil,
      "GET",
      this.prefix + """glys3""",
      """GET     /ontology glycordf/ontologyviewer
 GlyS3 structure search""",
      Seq()
    )
  )

  // @LINE:72
  private[this] lazy val controllers_StructureSearch_SubstructureSPARQLQueryResult44_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("substructureSPARQLQuery")))
  )
  private[this] lazy val controllers_StructureSearch_SubstructureSPARQLQueryResult44_invoker = createInvoker(
    StructureSearch_3.SubstructureSPARQLQueryResult(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.StructureSearch",
      "SubstructureSPARQLQueryResult",
      Nil,
      "GET",
      this.prefix + """substructureSPARQLQuery""",
      """""",
      Seq()
    )
  )

  // @LINE:75
  private[this] lazy val controllers_Assets_at45_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("images/"), DynamicPart("file", """.+""",false)))
  )
  private[this] lazy val controllers_Assets_at45_invoker = createInvoker(
    Assets_4.at(fakeValue[String], fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Assets",
      "at",
      Seq(classOf[String], classOf[String]),
      "GET",
      this.prefix + """images/""" + "$" + """file<.+>""",
      """ Map static resources from the /public folder to the /assets URL path""",
      Seq()
    )
  )

  // @LINE:76
  private[this] lazy val controllers_Assets_versioned46_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("assets/"), DynamicPart("file", """.+""",false)))
  )
  private[this] lazy val controllers_Assets_versioned46_invoker = createInvoker(
    Assets_4.versioned(fakeValue[String], fakeValue[Asset]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Assets",
      "versioned",
      Seq(classOf[String], classOf[Asset]),
      "GET",
      this.prefix + """assets/""" + "$" + """file<.+>""",
      """""",
      Seq()
    )
  )


  def routes: PartialFunction[RequestHeader, Handler] = {
  
    // @LINE:6
    case controllers_Application_index0_route(params) =>
      call { 
        controllers_Application_index0_invoker.call(Application_5.index())
      }
  
    // @LINE:8
    case controllers_Application_about1_route(params) =>
      call { 
        controllers_Application_about1_invoker.call(Application_5.about())
      }
  
    // @LINE:9
    case controllers_Application_listReferences2_route(params) =>
      call { 
        controllers_Application_listReferences2_invoker.call(Application_5.listReferences())
      }
  
    // @LINE:11
    case controllers_Application_listAllStructures3_route(params) =>
      call { 
        controllers_Application_listAllStructures3_invoker.call(Application_5.listAllStructures())
      }
  
    // @LINE:12
    case controllers_Collections_listAllCollections4_route(params) =>
      call { 
        controllers_Collections_listAllCollections4_invoker.call(Collections_0.listAllCollections())
      }
  
    // @LINE:13
    case controllers_Collections_getCollectionsRef5_route(params) =>
      call { 
        controllers_Collections_getCollectionsRef5_invoker.call(Collections_0.getCollectionsRef())
      }
  
    // @LINE:14
    case controllers_BuildCollectionsJson_buildCollectionsJson6_route(params) =>
      call(params.fromPath[String]("report", None)) { (report) =>
        controllers_BuildCollectionsJson_buildCollectionsJson6_invoker.call(BuildCollectionsJson_1.buildCollectionsJson(report))
      }
  
    // @LINE:16
    case controllers_StructureSearch_builder7_route(params) =>
      call { 
        controllers_StructureSearch_builder7_invoker.call(StructureSearch_3.builder())
      }
  
    // @LINE:17
    case controllers_StructureSearch_searchGlycoct8_route(params) =>
      call { 
        controllers_StructureSearch_searchGlycoct8_invoker.call(StructureSearch_3.searchGlycoct())
      }
  
    // @LINE:21
    case controllers_Application_searchGu9_route(params) =>
      call(params.fromPath[String]("expType", None), params.fromQuery[String]("minValue", None), params.fromQuery[String]("maxValue", None), params.fromQuery[Integer]("core", None), params.fromQuery[Integer]("bisect", None), params.fromQuery[Integer]("outer", None), params.fromQuery[Integer]("hybrid", None), params.fromQuery[Integer]("mannose", None)) { (expType, minValue, maxValue, core, bisect, outer, hybrid, mannose) =>
        controllers_Application_searchGu9_invoker.call(Application_5.searchGu(expType, minValue, maxValue, core, bisect, outer, hybrid, mannose))
      }
  
    // @LINE:22
    case controllers_Application_searchGuMotif10_route(params) =>
      call(params.fromPath[String]("expType", None), params.fromQuery[String]("minValue", None), params.fromQuery[String]("maxValue", None), params.fromQuery[Integer]("core", None), params.fromQuery[Integer]("bisect", None), params.fromQuery[Integer]("outer", None), params.fromQuery[Integer]("hybrid", None), params.fromQuery[Integer]("mannose", None)) { (expType, minValue, maxValue, core, bisect, outer, hybrid, mannose) =>
        controllers_Application_searchGuMotif10_invoker.call(Application_5.searchGuMotif(expType, minValue, maxValue, core, bisect, outer, hybrid, mannose))
      }
  
    // @LINE:24
    case controllers_Application_searchGuMotifOlinked11_route(params) =>
      call(params.fromPath[String]("expType", None), params.fromQuery[String]("minValue", None), params.fromQuery[String]("maxValue", None), params.fromQuery[Integer]("core1", None), params.fromQuery[Integer]("core2", None), params.fromQuery[Integer]("bloodgrouph", None), params.fromQuery[Integer]("linearbloodgroupb", None), params.fromQuery[Integer]("bloodgroupi", None), params.fromQuery[Integer]("bloodgrouplewisa", None), params.fromQuery[Integer]("bloodgrouplewisx", None), params.fromQuery[Integer]("bloodgroupslewisx", None)) { (expType, minValue, maxValue, core1, core2, bloodgrouph, linearbloodgroupb, bloodgroupi, bloodgrouplewisa, bloodgrouplewisx, bloodgroupslewisx) =>
        controllers_Application_searchGuMotifOlinked11_invoker.call(Application_5.searchGuMotifOlinked(expType, minValue, maxValue, core1, core2, bloodgrouph, linearbloodgroupb, bloodgroupi, bloodgrouplewisa, bloodgrouplewisx, bloodgroupslewisx))
      }
  
    // @LINE:29
    case controllers_Application_showGlycan12_route(params) =>
      call(params.fromPath[String]("uoxf", None)) { (uoxf) =>
        controllers_Application_showGlycan12_invoker.call(Application_5.showGlycan(uoxf))
      }
  
    // @LINE:30
    case controllers_Application_searchReport13_route(params) =>
      call(params.fromPath[String]("report", None)) { (report) =>
        controllers_Application_searchReport13_invoker.call(Application_5.searchReport(report))
      }
  
    // @LINE:31
    case controllers_Application_searchTissue14_route(params) =>
      call(params.fromPath[String]("tissue", None)) { (tissue) =>
        controllers_Application_searchTissue14_invoker.call(Application_5.searchTissue(tissue))
      }
  
    // @LINE:32
    case controllers_Application_searchSample15_route(params) =>
      call(params.fromPath[String]("sample", None)) { (sample) =>
        controllers_Application_searchSample15_invoker.call(Application_5.searchSample(sample))
      }
  
    // @LINE:33
    case controllers_Application_searchTaxon16_route(params) =>
      call(params.fromPath[String]("taxon", None)) { (taxon) =>
        controllers_Application_searchTaxon16_invoker.call(Application_5.searchTaxon(taxon))
      }
  
    // @LINE:34
    case controllers_Application_searchProfile17_route(params) =>
      call(params.fromPath[String]("profile", None)) { (profile) =>
        controllers_Application_searchProfile17_invoker.call(Application_5.searchProfile(profile))
      }
  
    // @LINE:35
    case controllers_Application_searchID18_route(params) =>
      call(params.fromPath[String]("id", None)) { (id) =>
        controllers_Application_searchID18_invoker.call(Application_5.searchID(id))
      }
  
    // @LINE:36
    case controllers_Application_searchUoxf19_route(params) =>
      call(params.fromPath[String]("name", None)) { (name) =>
        controllers_Application_searchUoxf19_invoker.call(Application_5.searchUoxf(name))
      }
  
    // @LINE:37
    case controllers_Application_listProfie4Type20_route(params) =>
      call(params.fromPath[String]("type", None)) { (_pf_escape_type) =>
        controllers_Application_listProfie4Type20_invoker.call(Application_5.listProfie4Type(_pf_escape_type))
      }
  
    // @LINE:38
    case controllers_Application_searchReference21_route(params) =>
      call(params.fromPath[String]("title", None)) { (title) =>
        controllers_Application_searchReference21_invoker.call(Application_5.searchReference(title))
      }
  
    // @LINE:39
    case controllers_Application_searchProtein22_route(params) =>
      call(params.fromPath[String]("name", None)) { (name) =>
        controllers_Application_searchProtein22_invoker.call(Application_5.searchProtein(name))
      }
  
    // @LINE:41
    case controllers_Image_showImage23_route(params) =>
      call(params.fromQuery[String]("structure", None), params.fromQuery[String]("notation", None), params.fromQuery[String]("extended", None)) { (structure, notation, extended) =>
        controllers_Image_showImage23_invoker.call(Image_2.showImage(structure, notation, extended))
      }
  
    // @LINE:42
    case controllers_Image_showImageReaction24_route(params) =>
      call(params.fromQuery[String]("glycobaseId", None), params.fromQuery[String]("notation", None), params.fromQuery[String]("extended", None)) { (glycobaseId, notation, extended) =>
        controllers_Image_showImageReaction24_invoker.call(Image_2.showImageReaction(glycobaseId, notation, extended))
      }
  
    // @LINE:43
    case controllers_Application_format25_route(params) =>
      call(params.fromPath[String]("s", None)) { (s) =>
        controllers_Application_format25_invoker.call(Application_5.format(s))
      }
  
    // @LINE:45
    case controllers_Application_queryoptions26_route(params) =>
      call { 
        controllers_Application_queryoptions26_invoker.call(Application_5.queryoptions())
      }
  
    // @LINE:47
    case controllers_Application_getGuRange27_route(params) =>
      call { 
        controllers_Application_getGuRange27_invoker.call(Application_5.getGuRange())
      }
  
    // @LINE:48
    case controllers_Application_getSampleName28_route(params) =>
      call { 
        controllers_Application_getSampleName28_invoker.call(Application_5.getSampleName())
      }
  
    // @LINE:49
    case controllers_Application_getTaxonName29_route(params) =>
      call { 
        controllers_Application_getTaxonName29_invoker.call(Application_5.getTaxonName())
      }
  
    // @LINE:50
    case controllers_Application_getID30_route(params) =>
      call { 
        controllers_Application_getID30_invoker.call(Application_5.getID())
      }
  
    // @LINE:51
    case controllers_Application_getUoxf31_route(params) =>
      call { 
        controllers_Application_getUoxf31_invoker.call(Application_5.getUoxf())
      }
  
    // @LINE:52
    case controllers_Application_getTissueName32_route(params) =>
      call { 
        controllers_Application_getTissueName32_invoker.call(Application_5.getTissueName())
      }
  
    // @LINE:53
    case controllers_Application_getProfileID33_route(params) =>
      call { 
        controllers_Application_getProfileID33_invoker.call(Application_5.getProfileID())
      }
  
    // @LINE:54
    case controllers_Application_getReportName34_route(params) =>
      call { 
        controllers_Application_getReportName34_invoker.call(Application_5.getReportName())
      }
  
    // @LINE:55
    case controllers_Application_getProtein35_route(params) =>
      call { 
        controllers_Application_getProtein35_invoker.call(Application_5.getProtein())
      }
  
    // @LINE:56
    case controllers_Application_searchFilter36_route(params) =>
      call { 
        controllers_Application_searchFilter36_invoker.call(Application_5.searchFilter())
      }
  
    // @LINE:59
    case controllers_Application_showPgc37_route(params) =>
      call { 
        controllers_Application_showPgc37_invoker.call(Application_5.showPgc())
      }
  
    // @LINE:60
    case controllers_Application_showPgcGlycan38_route(params) =>
      call(params.fromPath[String]("id", None)) { (id) =>
        controllers_Application_showPgcGlycan38_invoker.call(Application_5.showPgcGlycan(id))
      }
  
    // @LINE:61
    case controllers_Image_showImagePgc39_route(params) =>
      call(params.fromQuery[String]("glycostoreId", None), params.fromQuery[String]("notation", None), params.fromQuery[String]("extended", None)) { (glycostoreId, notation, extended) =>
        controllers_Image_showImagePgc39_invoker.call(Image_2.showImagePgc(glycostoreId, notation, extended))
      }
  
    // @LINE:62
    case controllers_Image_showImagePgcObjectStore40_route(params) =>
      call(params.fromQuery[String]("glycostoreId", None), params.fromQuery[String]("notation", None)) { (glycostoreId, notation) =>
        controllers_Image_showImagePgcObjectStore40_invoker.call(Image_2.showImagePgcObjectStore(glycostoreId, notation))
      }
  
    // @LINE:66
    case controllers_Application_startShowPgcData41_route(params) =>
      call { 
        controllers_Application_startShowPgcData41_invoker.call(Application_5.startShowPgcData())
      }
  
    // @LINE:67
    case controllers_Application_showPgcDataTemp42_route(params) =>
      call { 
        controllers_Application_showPgcDataTemp42_invoker.call(Application_5.showPgcDataTemp())
      }
  
    // @LINE:71
    case controllers_StructureSearch_glys343_route(params) =>
      call { 
        controllers_StructureSearch_glys343_invoker.call(StructureSearch_3.glys3())
      }
  
    // @LINE:72
    case controllers_StructureSearch_SubstructureSPARQLQueryResult44_route(params) =>
      call { 
        controllers_StructureSearch_SubstructureSPARQLQueryResult44_invoker.call(StructureSearch_3.SubstructureSPARQLQueryResult())
      }
  
    // @LINE:75
    case controllers_Assets_at45_route(params) =>
      call(Param[String]("path", Right("/public/images")), params.fromPath[String]("file", None)) { (path, file) =>
        controllers_Assets_at45_invoker.call(Assets_4.at(path, file))
      }
  
    // @LINE:76
    case controllers_Assets_versioned46_route(params) =>
      call(Param[String]("path", Right("/public")), params.fromPath[Asset]("file", None)) { (path, file) =>
        controllers_Assets_versioned46_invoker.call(Assets_4.versioned(path, file))
      }
  }
}
