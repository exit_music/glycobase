
// @GENERATOR:play-routes-compiler
// @SOURCE:/Users/s2980369/IdeaProjects/glycobase/glycobase/conf/routes
// @DATE:Mon Feb 26 14:39:19 AEST 2018


package router {
  object RoutesPrefix {
    private var _prefix: String = "/"
    def setPrefix(p: String): Unit = {
      _prefix = p
    }
    def prefix: String = _prefix
    val byNamePrefix: Function0[String] = { () => prefix }
  }
}
