
// @GENERATOR:play-routes-compiler
// @SOURCE:/Users/s2980369/IdeaProjects/glycobase/glycobase/conf/routes
// @DATE:Mon Feb 26 14:39:19 AEST 2018

import play.api.mvc.Call


import _root_.controllers.Assets.Asset
import _root_.play.libs.F

// @LINE:6
package controllers {

  // @LINE:75
  class ReverseAssets(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:75
    def at(file:String): Call = {
      implicit val _rrc = new play.core.routing.ReverseRouteContext(Map(("path", "/public/images")))
      Call("GET", _prefix + { _defaultPrefix } + "images/" + implicitly[play.api.mvc.PathBindable[String]].unbind("file", file))
    }
  
    // @LINE:76
    def versioned(file:Asset): Call = {
      implicit val _rrc = new play.core.routing.ReverseRouteContext(Map(("path", "/public")))
      Call("GET", _prefix + { _defaultPrefix } + "assets/" + implicitly[play.api.mvc.PathBindable[Asset]].unbind("file", file))
    }
  
  }

  // @LINE:12
  class ReverseCollections(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:12
    def listAllCollections(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "collections")
    }
  
    // @LINE:13
    def getCollectionsRef(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "collectionsRef")
    }
  
  }

  // @LINE:6
  class ReverseApplication(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:50
    def getID(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "searchID")
    }
  
    // @LINE:32
    def searchSample(sample:String): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "searchSample/" + implicitly[play.api.mvc.PathBindable[String]].unbind("sample", play.core.routing.dynamicString(sample)))
    }
  
    // @LINE:53
    def getProfileID(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "searchProfile")
    }
  
    // @LINE:51
    def getUoxf(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "searchUoxf")
    }
  
    // @LINE:35
    def searchID(id:String): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "searchID/" + implicitly[play.api.mvc.PathBindable[String]].unbind("id", play.core.routing.dynamicString(id)))
    }
  
    // @LINE:54
    def getReportName(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "searchReport")
    }
  
    // @LINE:36
    def searchUoxf(name:String): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "searchUoxf/" + implicitly[play.api.mvc.PathBindable[String]].unbind("name", play.core.routing.dynamicString(name)))
    }
  
    // @LINE:59
    def showPgc(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "showPgc")
    }
  
    // @LINE:38
    def searchReference(title:String): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "reference/" + implicitly[play.api.mvc.PathBindable[String]].unbind("title", play.core.routing.dynamicString(title)))
    }
  
    // @LINE:52
    def getTissueName(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "searchTissue")
    }
  
    // @LINE:24
    def searchGuMotifOlinked(expType:String, minValue:String, maxValue:String, core1:Integer, core2:Integer, bloodgrouph:Integer, linearbloodgroupb:Integer, bloodgroupi:Integer, bloodgrouplewisa:Integer, bloodgrouplewisx:Integer, bloodgroupslewisx:Integer): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "searchGuMotifOlinked/" + implicitly[play.api.mvc.PathBindable[String]].unbind("expType", play.core.routing.dynamicString(expType)) + play.core.routing.queryString(List(Some(implicitly[play.api.mvc.QueryStringBindable[String]].unbind("minValue", minValue)), Some(implicitly[play.api.mvc.QueryStringBindable[String]].unbind("maxValue", maxValue)), Some(implicitly[play.api.mvc.QueryStringBindable[Integer]].unbind("core1", core1)), Some(implicitly[play.api.mvc.QueryStringBindable[Integer]].unbind("core2", core2)), Some(implicitly[play.api.mvc.QueryStringBindable[Integer]].unbind("bloodgrouph", bloodgrouph)), Some(implicitly[play.api.mvc.QueryStringBindable[Integer]].unbind("linearbloodgroupb", linearbloodgroupb)), Some(implicitly[play.api.mvc.QueryStringBindable[Integer]].unbind("bloodgroupi", bloodgroupi)), Some(implicitly[play.api.mvc.QueryStringBindable[Integer]].unbind("bloodgrouplewisa", bloodgrouplewisa)), Some(implicitly[play.api.mvc.QueryStringBindable[Integer]].unbind("bloodgrouplewisx", bloodgrouplewisx)), Some(implicitly[play.api.mvc.QueryStringBindable[Integer]].unbind("bloodgroupslewisx", bloodgroupslewisx)))))
    }
  
    // @LINE:39
    def searchProtein(name:String): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "proteins/" + implicitly[play.api.mvc.PathBindable[String]].unbind("name", play.core.routing.dynamicString(name)))
    }
  
    // @LINE:66
    def startShowPgcData(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "showPgcData")
    }
  
    // @LINE:29
    def showGlycan(uoxf:String): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "glycan/" + implicitly[play.api.mvc.PathBindable[String]].unbind("uoxf", play.core.routing.dynamicString(uoxf)))
    }
  
    // @LINE:9
    def listReferences(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "references")
    }
  
    // @LINE:56
    def searchFilter(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "searchFilter")
    }
  
    // @LINE:49
    def getTaxonName(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "searchTaxon")
    }
  
    // @LINE:60
    def showPgcGlycan(id:String): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "showPgcGlycan/" + implicitly[play.api.mvc.PathBindable[String]].unbind("id", play.core.routing.dynamicString(id)))
    }
  
    // @LINE:55
    def getProtein(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "proteins")
    }
  
    // @LINE:67
    def showPgcDataTemp(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "showPgcDataTemp")
    }
  
    // @LINE:8
    def about(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "about")
    }
  
    // @LINE:11
    def listAllStructures(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "structures")
    }
  
    // @LINE:30
    def searchReport(report:String): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "searchReport/" + implicitly[play.api.mvc.PathBindable[String]].unbind("report", play.core.routing.dynamicString(report)))
    }
  
    // @LINE:48
    def getSampleName(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "searchSample")
    }
  
    // @LINE:47
    def getGuRange(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "searchGU")
    }
  
    // @LINE:21
    def searchGu(expType:String, minValue:String, maxValue:String, core:Integer, bisect:Integer, outer:Integer, hybrid:Integer, mannose:Integer): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "searchGu/" + implicitly[play.api.mvc.PathBindable[String]].unbind("expType", play.core.routing.dynamicString(expType)) + play.core.routing.queryString(List(Some(implicitly[play.api.mvc.QueryStringBindable[String]].unbind("minValue", minValue)), Some(implicitly[play.api.mvc.QueryStringBindable[String]].unbind("maxValue", maxValue)), Some(implicitly[play.api.mvc.QueryStringBindable[Integer]].unbind("core", core)), Some(implicitly[play.api.mvc.QueryStringBindable[Integer]].unbind("bisect", bisect)), Some(implicitly[play.api.mvc.QueryStringBindable[Integer]].unbind("outer", outer)), Some(implicitly[play.api.mvc.QueryStringBindable[Integer]].unbind("hybrid", hybrid)), Some(implicitly[play.api.mvc.QueryStringBindable[Integer]].unbind("mannose", mannose)))))
    }
  
    // @LINE:33
    def searchTaxon(taxon:String): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "searchTaxon/" + implicitly[play.api.mvc.PathBindable[String]].unbind("taxon", play.core.routing.dynamicString(taxon)))
    }
  
    // @LINE:34
    def searchProfile(profile:String): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "searchProfile/" + implicitly[play.api.mvc.PathBindable[String]].unbind("profile", play.core.routing.dynamicString(profile)))
    }
  
    // @LINE:22
    def searchGuMotif(expType:String, minValue:String, maxValue:String, core:Integer, bisect:Integer, outer:Integer, hybrid:Integer, mannose:Integer): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "searchGuMotif/" + implicitly[play.api.mvc.PathBindable[String]].unbind("expType", play.core.routing.dynamicString(expType)) + play.core.routing.queryString(List(Some(implicitly[play.api.mvc.QueryStringBindable[String]].unbind("minValue", minValue)), Some(implicitly[play.api.mvc.QueryStringBindable[String]].unbind("maxValue", maxValue)), Some(implicitly[play.api.mvc.QueryStringBindable[Integer]].unbind("core", core)), Some(implicitly[play.api.mvc.QueryStringBindable[Integer]].unbind("bisect", bisect)), Some(implicitly[play.api.mvc.QueryStringBindable[Integer]].unbind("outer", outer)), Some(implicitly[play.api.mvc.QueryStringBindable[Integer]].unbind("hybrid", hybrid)), Some(implicitly[play.api.mvc.QueryStringBindable[Integer]].unbind("mannose", mannose)))))
    }
  
    // @LINE:6
    def index(): Call = {
      
      Call("GET", _prefix)
    }
  
    // @LINE:45
    def queryoptions(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "search")
    }
  
    // @LINE:37
    def listProfie4Type(_pf_escape_type:String): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "profile/" + implicitly[play.api.mvc.PathBindable[String]].unbind("type", play.core.routing.dynamicString(_pf_escape_type)))
    }
  
    // @LINE:31
    def searchTissue(tissue:String): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "searchTissue/" + implicitly[play.api.mvc.PathBindable[String]].unbind("tissue", play.core.routing.dynamicString(tissue)))
    }
  
    // @LINE:43
    def format(s:String): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "format/" + implicitly[play.api.mvc.PathBindable[String]].unbind("s", play.core.routing.dynamicString(s)))
    }
  
  }

  // @LINE:16
  class ReverseStructureSearch(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:72
    def SubstructureSPARQLQueryResult(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "substructureSPARQLQuery")
    }
  
    // @LINE:16
    def builder(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "builder")
    }
  
    // @LINE:17
    def searchGlycoct(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "searchGlycoCT")
    }
  
    // @LINE:71
    def glys3(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "glys3")
    }
  
  }

  // @LINE:14
  class ReverseBuildCollectionsJson(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:14
    def buildCollectionsJson(report:String): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "displayCollection/" + implicitly[play.api.mvc.PathBindable[String]].unbind("report", play.core.routing.dynamicString(report)))
    }
  
  }

  // @LINE:41
  class ReverseImage(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:62
    def showImagePgcObjectStore(glycostoreId:String, notation:String): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "showImagePgcObjectStore" + play.core.routing.queryString(List(Some(implicitly[play.api.mvc.QueryStringBindable[String]].unbind("glycostoreId", glycostoreId)), Some(implicitly[play.api.mvc.QueryStringBindable[String]].unbind("notation", notation)))))
    }
  
    // @LINE:61
    def showImagePgc(glycostoreId:String, notation:String, extended:String): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "showImagePgc" + play.core.routing.queryString(List(Some(implicitly[play.api.mvc.QueryStringBindable[String]].unbind("glycostoreId", glycostoreId)), Some(implicitly[play.api.mvc.QueryStringBindable[String]].unbind("notation", notation)), Some(implicitly[play.api.mvc.QueryStringBindable[String]].unbind("extended", extended)))))
    }
  
    // @LINE:42
    def showImageReaction(glycobaseId:String, notation:String, extended:String): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "showImageReaction" + play.core.routing.queryString(List(Some(implicitly[play.api.mvc.QueryStringBindable[String]].unbind("glycobaseId", glycobaseId)), Some(implicitly[play.api.mvc.QueryStringBindable[String]].unbind("notation", notation)), Some(implicitly[play.api.mvc.QueryStringBindable[String]].unbind("extended", extended)))))
    }
  
    // @LINE:41
    def showImage(structure:String, notation:String, extended:String): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "showImage" + play.core.routing.queryString(List(Some(implicitly[play.api.mvc.QueryStringBindable[String]].unbind("structure", structure)), Some(implicitly[play.api.mvc.QueryStringBindable[String]].unbind("notation", notation)), Some(implicitly[play.api.mvc.QueryStringBindable[String]].unbind("extended", extended)))))
    }
  
  }


}
