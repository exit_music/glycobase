
// @GENERATOR:play-routes-compiler
// @SOURCE:/Users/s2980369/IdeaProjects/glycobase/glycobase/conf/routes
// @DATE:Mon Feb 26 14:39:19 AEST 2018

import play.api.routing.JavaScriptReverseRoute


import _root_.controllers.Assets.Asset
import _root_.play.libs.F

// @LINE:6
package controllers.javascript {

  // @LINE:75
  class ReverseAssets(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:75
    def at: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Assets.at",
      """
        function(file1) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "images/" + (""" + implicitly[play.api.mvc.PathBindable[String]].javascriptUnbind + """)("file", file1)})
        }
      """
    )
  
    // @LINE:76
    def versioned: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Assets.versioned",
      """
        function(file1) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "assets/" + (""" + implicitly[play.api.mvc.PathBindable[Asset]].javascriptUnbind + """)("file", file1)})
        }
      """
    )
  
  }

  // @LINE:12
  class ReverseCollections(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:12
    def listAllCollections: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Collections.listAllCollections",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "collections"})
        }
      """
    )
  
    // @LINE:13
    def getCollectionsRef: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Collections.getCollectionsRef",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "collectionsRef"})
        }
      """
    )
  
  }

  // @LINE:6
  class ReverseApplication(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:50
    def getID: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Application.getID",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "searchID"})
        }
      """
    )
  
    // @LINE:32
    def searchSample: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Application.searchSample",
      """
        function(sample0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "searchSample/" + (""" + implicitly[play.api.mvc.PathBindable[String]].javascriptUnbind + """)("sample", encodeURIComponent(sample0))})
        }
      """
    )
  
    // @LINE:53
    def getProfileID: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Application.getProfileID",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "searchProfile"})
        }
      """
    )
  
    // @LINE:51
    def getUoxf: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Application.getUoxf",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "searchUoxf"})
        }
      """
    )
  
    // @LINE:35
    def searchID: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Application.searchID",
      """
        function(id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "searchID/" + (""" + implicitly[play.api.mvc.PathBindable[String]].javascriptUnbind + """)("id", encodeURIComponent(id0))})
        }
      """
    )
  
    // @LINE:54
    def getReportName: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Application.getReportName",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "searchReport"})
        }
      """
    )
  
    // @LINE:36
    def searchUoxf: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Application.searchUoxf",
      """
        function(name0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "searchUoxf/" + (""" + implicitly[play.api.mvc.PathBindable[String]].javascriptUnbind + """)("name", encodeURIComponent(name0))})
        }
      """
    )
  
    // @LINE:59
    def showPgc: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Application.showPgc",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "showPgc"})
        }
      """
    )
  
    // @LINE:38
    def searchReference: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Application.searchReference",
      """
        function(title0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "reference/" + (""" + implicitly[play.api.mvc.PathBindable[String]].javascriptUnbind + """)("title", encodeURIComponent(title0))})
        }
      """
    )
  
    // @LINE:52
    def getTissueName: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Application.getTissueName",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "searchTissue"})
        }
      """
    )
  
    // @LINE:24
    def searchGuMotifOlinked: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Application.searchGuMotifOlinked",
      """
        function(expType0,minValue1,maxValue2,core13,core24,bloodgrouph5,linearbloodgroupb6,bloodgroupi7,bloodgrouplewisa8,bloodgrouplewisx9,bloodgroupslewisx10) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "searchGuMotifOlinked/" + (""" + implicitly[play.api.mvc.PathBindable[String]].javascriptUnbind + """)("expType", encodeURIComponent(expType0)) + _qS([(""" + implicitly[play.api.mvc.QueryStringBindable[String]].javascriptUnbind + """)("minValue", minValue1), (""" + implicitly[play.api.mvc.QueryStringBindable[String]].javascriptUnbind + """)("maxValue", maxValue2), (""" + implicitly[play.api.mvc.QueryStringBindable[Integer]].javascriptUnbind + """)("core1", core13), (""" + implicitly[play.api.mvc.QueryStringBindable[Integer]].javascriptUnbind + """)("core2", core24), (""" + implicitly[play.api.mvc.QueryStringBindable[Integer]].javascriptUnbind + """)("bloodgrouph", bloodgrouph5), (""" + implicitly[play.api.mvc.QueryStringBindable[Integer]].javascriptUnbind + """)("linearbloodgroupb", linearbloodgroupb6), (""" + implicitly[play.api.mvc.QueryStringBindable[Integer]].javascriptUnbind + """)("bloodgroupi", bloodgroupi7), (""" + implicitly[play.api.mvc.QueryStringBindable[Integer]].javascriptUnbind + """)("bloodgrouplewisa", bloodgrouplewisa8), (""" + implicitly[play.api.mvc.QueryStringBindable[Integer]].javascriptUnbind + """)("bloodgrouplewisx", bloodgrouplewisx9), (""" + implicitly[play.api.mvc.QueryStringBindable[Integer]].javascriptUnbind + """)("bloodgroupslewisx", bloodgroupslewisx10)])})
        }
      """
    )
  
    // @LINE:39
    def searchProtein: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Application.searchProtein",
      """
        function(name0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "proteins/" + (""" + implicitly[play.api.mvc.PathBindable[String]].javascriptUnbind + """)("name", encodeURIComponent(name0))})
        }
      """
    )
  
    // @LINE:66
    def startShowPgcData: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Application.startShowPgcData",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "showPgcData"})
        }
      """
    )
  
    // @LINE:29
    def showGlycan: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Application.showGlycan",
      """
        function(uoxf0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "glycan/" + (""" + implicitly[play.api.mvc.PathBindable[String]].javascriptUnbind + """)("uoxf", encodeURIComponent(uoxf0))})
        }
      """
    )
  
    // @LINE:9
    def listReferences: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Application.listReferences",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "references"})
        }
      """
    )
  
    // @LINE:56
    def searchFilter: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Application.searchFilter",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "searchFilter"})
        }
      """
    )
  
    // @LINE:49
    def getTaxonName: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Application.getTaxonName",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "searchTaxon"})
        }
      """
    )
  
    // @LINE:60
    def showPgcGlycan: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Application.showPgcGlycan",
      """
        function(id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "showPgcGlycan/" + (""" + implicitly[play.api.mvc.PathBindable[String]].javascriptUnbind + """)("id", encodeURIComponent(id0))})
        }
      """
    )
  
    // @LINE:55
    def getProtein: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Application.getProtein",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "proteins"})
        }
      """
    )
  
    // @LINE:67
    def showPgcDataTemp: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Application.showPgcDataTemp",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "showPgcDataTemp"})
        }
      """
    )
  
    // @LINE:8
    def about: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Application.about",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "about"})
        }
      """
    )
  
    // @LINE:11
    def listAllStructures: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Application.listAllStructures",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "structures"})
        }
      """
    )
  
    // @LINE:30
    def searchReport: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Application.searchReport",
      """
        function(report0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "searchReport/" + (""" + implicitly[play.api.mvc.PathBindable[String]].javascriptUnbind + """)("report", encodeURIComponent(report0))})
        }
      """
    )
  
    // @LINE:48
    def getSampleName: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Application.getSampleName",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "searchSample"})
        }
      """
    )
  
    // @LINE:47
    def getGuRange: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Application.getGuRange",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "searchGU"})
        }
      """
    )
  
    // @LINE:21
    def searchGu: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Application.searchGu",
      """
        function(expType0,minValue1,maxValue2,core3,bisect4,outer5,hybrid6,mannose7) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "searchGu/" + (""" + implicitly[play.api.mvc.PathBindable[String]].javascriptUnbind + """)("expType", encodeURIComponent(expType0)) + _qS([(""" + implicitly[play.api.mvc.QueryStringBindable[String]].javascriptUnbind + """)("minValue", minValue1), (""" + implicitly[play.api.mvc.QueryStringBindable[String]].javascriptUnbind + """)("maxValue", maxValue2), (""" + implicitly[play.api.mvc.QueryStringBindable[Integer]].javascriptUnbind + """)("core", core3), (""" + implicitly[play.api.mvc.QueryStringBindable[Integer]].javascriptUnbind + """)("bisect", bisect4), (""" + implicitly[play.api.mvc.QueryStringBindable[Integer]].javascriptUnbind + """)("outer", outer5), (""" + implicitly[play.api.mvc.QueryStringBindable[Integer]].javascriptUnbind + """)("hybrid", hybrid6), (""" + implicitly[play.api.mvc.QueryStringBindable[Integer]].javascriptUnbind + """)("mannose", mannose7)])})
        }
      """
    )
  
    // @LINE:33
    def searchTaxon: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Application.searchTaxon",
      """
        function(taxon0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "searchTaxon/" + (""" + implicitly[play.api.mvc.PathBindable[String]].javascriptUnbind + """)("taxon", encodeURIComponent(taxon0))})
        }
      """
    )
  
    // @LINE:34
    def searchProfile: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Application.searchProfile",
      """
        function(profile0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "searchProfile/" + (""" + implicitly[play.api.mvc.PathBindable[String]].javascriptUnbind + """)("profile", encodeURIComponent(profile0))})
        }
      """
    )
  
    // @LINE:22
    def searchGuMotif: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Application.searchGuMotif",
      """
        function(expType0,minValue1,maxValue2,core3,bisect4,outer5,hybrid6,mannose7) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "searchGuMotif/" + (""" + implicitly[play.api.mvc.PathBindable[String]].javascriptUnbind + """)("expType", encodeURIComponent(expType0)) + _qS([(""" + implicitly[play.api.mvc.QueryStringBindable[String]].javascriptUnbind + """)("minValue", minValue1), (""" + implicitly[play.api.mvc.QueryStringBindable[String]].javascriptUnbind + """)("maxValue", maxValue2), (""" + implicitly[play.api.mvc.QueryStringBindable[Integer]].javascriptUnbind + """)("core", core3), (""" + implicitly[play.api.mvc.QueryStringBindable[Integer]].javascriptUnbind + """)("bisect", bisect4), (""" + implicitly[play.api.mvc.QueryStringBindable[Integer]].javascriptUnbind + """)("outer", outer5), (""" + implicitly[play.api.mvc.QueryStringBindable[Integer]].javascriptUnbind + """)("hybrid", hybrid6), (""" + implicitly[play.api.mvc.QueryStringBindable[Integer]].javascriptUnbind + """)("mannose", mannose7)])})
        }
      """
    )
  
    // @LINE:6
    def index: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Application.index",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + """"})
        }
      """
    )
  
    // @LINE:45
    def queryoptions: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Application.queryoptions",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "search"})
        }
      """
    )
  
    // @LINE:37
    def listProfie4Type: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Application.listProfie4Type",
      """
        function(type0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "profile/" + (""" + implicitly[play.api.mvc.PathBindable[String]].javascriptUnbind + """)("type", encodeURIComponent(type0))})
        }
      """
    )
  
    // @LINE:31
    def searchTissue: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Application.searchTissue",
      """
        function(tissue0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "searchTissue/" + (""" + implicitly[play.api.mvc.PathBindable[String]].javascriptUnbind + """)("tissue", encodeURIComponent(tissue0))})
        }
      """
    )
  
    // @LINE:43
    def format: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Application.format",
      """
        function(s0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "format/" + (""" + implicitly[play.api.mvc.PathBindable[String]].javascriptUnbind + """)("s", encodeURIComponent(s0))})
        }
      """
    )
  
  }

  // @LINE:16
  class ReverseStructureSearch(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:72
    def SubstructureSPARQLQueryResult: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.StructureSearch.SubstructureSPARQLQueryResult",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "substructureSPARQLQuery"})
        }
      """
    )
  
    // @LINE:16
    def builder: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.StructureSearch.builder",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "builder"})
        }
      """
    )
  
    // @LINE:17
    def searchGlycoct: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.StructureSearch.searchGlycoct",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "searchGlycoCT"})
        }
      """
    )
  
    // @LINE:71
    def glys3: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.StructureSearch.glys3",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "glys3"})
        }
      """
    )
  
  }

  // @LINE:14
  class ReverseBuildCollectionsJson(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:14
    def buildCollectionsJson: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.BuildCollectionsJson.buildCollectionsJson",
      """
        function(report0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "displayCollection/" + (""" + implicitly[play.api.mvc.PathBindable[String]].javascriptUnbind + """)("report", encodeURIComponent(report0))})
        }
      """
    )
  
  }

  // @LINE:41
  class ReverseImage(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:62
    def showImagePgcObjectStore: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Image.showImagePgcObjectStore",
      """
        function(glycostoreId0,notation1) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "showImagePgcObjectStore" + _qS([(""" + implicitly[play.api.mvc.QueryStringBindable[String]].javascriptUnbind + """)("glycostoreId", glycostoreId0), (""" + implicitly[play.api.mvc.QueryStringBindable[String]].javascriptUnbind + """)("notation", notation1)])})
        }
      """
    )
  
    // @LINE:61
    def showImagePgc: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Image.showImagePgc",
      """
        function(glycostoreId0,notation1,extended2) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "showImagePgc" + _qS([(""" + implicitly[play.api.mvc.QueryStringBindable[String]].javascriptUnbind + """)("glycostoreId", glycostoreId0), (""" + implicitly[play.api.mvc.QueryStringBindable[String]].javascriptUnbind + """)("notation", notation1), (""" + implicitly[play.api.mvc.QueryStringBindable[String]].javascriptUnbind + """)("extended", extended2)])})
        }
      """
    )
  
    // @LINE:42
    def showImageReaction: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Image.showImageReaction",
      """
        function(glycobaseId0,notation1,extended2) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "showImageReaction" + _qS([(""" + implicitly[play.api.mvc.QueryStringBindable[String]].javascriptUnbind + """)("glycobaseId", glycobaseId0), (""" + implicitly[play.api.mvc.QueryStringBindable[String]].javascriptUnbind + """)("notation", notation1), (""" + implicitly[play.api.mvc.QueryStringBindable[String]].javascriptUnbind + """)("extended", extended2)])})
        }
      """
    )
  
    // @LINE:41
    def showImage: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Image.showImage",
      """
        function(structure0,notation1,extended2) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "showImage" + _qS([(""" + implicitly[play.api.mvc.QueryStringBindable[String]].javascriptUnbind + """)("structure", structure0), (""" + implicitly[play.api.mvc.QueryStringBindable[String]].javascriptUnbind + """)("notation", notation1), (""" + implicitly[play.api.mvc.QueryStringBindable[String]].javascriptUnbind + """)("extended", extended2)])})
        }
      """
    )
  
  }


}
