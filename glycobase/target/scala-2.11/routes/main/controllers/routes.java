
// @GENERATOR:play-routes-compiler
// @SOURCE:/Users/s2980369/IdeaProjects/glycobase/glycobase/conf/routes
// @DATE:Mon Feb 26 14:39:19 AEST 2018

package controllers;

import router.RoutesPrefix;

public class routes {
  
  public static final controllers.ReverseAssets Assets = new controllers.ReverseAssets(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseCollections Collections = new controllers.ReverseCollections(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseApplication Application = new controllers.ReverseApplication(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseStructureSearch StructureSearch = new controllers.ReverseStructureSearch(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseBuildCollectionsJson BuildCollectionsJson = new controllers.ReverseBuildCollectionsJson(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseImage Image = new controllers.ReverseImage(RoutesPrefix.byNamePrefix());

  public static class javascript {
    
    public static final controllers.javascript.ReverseAssets Assets = new controllers.javascript.ReverseAssets(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseCollections Collections = new controllers.javascript.ReverseCollections(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseApplication Application = new controllers.javascript.ReverseApplication(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseStructureSearch StructureSearch = new controllers.javascript.ReverseStructureSearch(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseBuildCollectionsJson BuildCollectionsJson = new controllers.javascript.ReverseBuildCollectionsJson(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseImage Image = new controllers.javascript.ReverseImage(RoutesPrefix.byNamePrefix());
  }

}
